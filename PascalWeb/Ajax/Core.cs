﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Routing;

namespace PascalWeb.Ajax.Core
{
    public static class HtmlHelpers
    {
        /// <summary>
        /// Создает информационные блоки:
        ///     1) "pageTitle" - с названием страницы
        ///     2) "pageUrl" - с адресом страницы
        /// </summary>
        public static MvcHtmlString PageInfo(this HtmlHelper helper, String title, String url)
        {
            // Create title
            TagBuilder pageTitle = new TagBuilder("div");
            pageTitle.SetInnerText("Pascal. " + title);
            pageTitle.MergeAttribute("id", "pageTitle");
            pageTitle.MergeAttribute("style", "display: none;");
            // Create url
            TagBuilder pageUrl = new TagBuilder("div");
            pageUrl.SetInnerText(url);
            pageUrl.MergeAttribute("id", "pageUrl");
            pageUrl.MergeAttribute("style", "display: none;");

     //       System.Web.Mvc.Html.RenderPartialExtensions.RenderPartial(helper, "_HeadNavigation");
   

            return new MvcHtmlString(pageTitle.ToString(TagRenderMode.Normal) + pageUrl.ToString(TagRenderMode.Normal));
        }
    }

    public static class AjaxHelpers
    {
        /// <summary>
        /// Метод создает ссылку, которая с помощью Ajax запроса загружает в контейнер с именем main 
        ///     требуемый PartialView
        /// </summary>
        public static MvcHtmlString ActionLinkTo(this AjaxHelper ajaxHelper, String linkText, String actionName, String controllerName = null, String areaName = null, String loadMessage = null, Object routeValues = null, Object htmlAttributes = null, string imageUrl = null)
        {
            // Создаем маршрут
            RouteValueDictionary routeValueDictionary = new RouteValueDictionary(routeValues);

            if (!String.IsNullOrEmpty(actionName) && !routeValueDictionary.ContainsKey("action"))
            {
                routeValueDictionary.Add("action", actionName);
            }
            if (!String.IsNullOrEmpty(controllerName) && !routeValueDictionary.ContainsKey("controller"))
            {
                routeValueDictionary.Add("controller", controllerName);
            }
            if (!routeValueDictionary.ContainsKey("area"))
            {
                if (!String.IsNullOrEmpty(areaName))
                    routeValueDictionary.Add("area", areaName);
                else
                    routeValueDictionary.Add("area", "");
            }

            // Создаем параметры Ajax            
            AjaxOptions ajaxOptions = new AjaxOptions()
            {
                UpdateTargetId = "main",
                InsertionMode = InsertionMode.Replace,
                HttpMethod = "GET",
                LoadingElementId = "loadLayout",
                OnBegin = "changeLoadMesage('" + loadMessage + "')",
                OnSuccess = "onPageLoaded()"
            };

            // Возвращаем строку
            //String ajaxActionName = "Ajax" + actionName;
            String ajaxActionName = actionName;


            return ajaxHelper.ActionLink(linkText, ajaxActionName, null,
                routeValueDictionary, ajaxOptions, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }

        public static AjaxOptions generateOptionsGet(string HttpMethod, string loadMessage = "Загрузка")
        {
            return new AjaxOptions()
            {
                UpdateTargetId = "main",
                InsertionMode = InsertionMode.Replace,
                HttpMethod = HttpMethod,
                LoadingElementId = "loadLayout",
                OnBegin = "changeLoadMesage('" + loadMessage + "')",
                OnSuccess = "onPageLoaded()"
            };
        }

        public static AjaxOptions generateOptionsGet(string loadMessage = "Загрузка")
        {
            return generateOptionsGet("GET", loadMessage);
        }

        public static AjaxOptions generateOptionsPost(string loadMessage = "Загрузка")
        {
            return generateOptionsGet("POST", loadMessage);
        }




        /// <summary>
        /// Метод создает ссылку, которая с помощью Ajax запроса загружает в контейнер с именем main 
        ///     требуемый PartialView
        /// </summary>
        public static MvcHtmlString AudioActionLinkTo(this AjaxHelper ajaxHelper, String linkText, String actionName, String controllerName = null, String areaName = null, String loadMessage = null, Object routeValues = null, Object htmlAttributes = null, string imageUrl = null)
        {
            // Создаем маршрут
            RouteValueDictionary routeValueDictionary = new RouteValueDictionary(routeValues);

            if (!String.IsNullOrEmpty(actionName) && !routeValueDictionary.ContainsKey("action"))
            {
                routeValueDictionary.Add("action", actionName);
            }
            if (!String.IsNullOrEmpty(controllerName) && !routeValueDictionary.ContainsKey("controller"))
            {
                routeValueDictionary.Add("controller", controllerName);
            }
            if (!routeValueDictionary.ContainsKey("area"))
            {
                if (!String.IsNullOrEmpty(areaName))
                    routeValueDictionary.Add("area", areaName);
                else
                    routeValueDictionary.Add("area", "");
            }

            // Создаем параметры Ajax            
            AjaxOptions ajaxOptions = new AjaxOptions()
            {
                UpdateTargetId = "mainAudio",
                InsertionMode = InsertionMode.Replace,
                HttpMethod = "GET"
            };

            // Возвращаем строку
            //String ajaxActionName = "Ajax" + actionName;
            String ajaxActionName = actionName;
            return ajaxHelper.ActionLink(linkText, ajaxActionName, null,
                routeValueDictionary, ajaxOptions, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }

        /// <summary>
        /// Метод создает ссылку, которая с помощью Ajax запроса загружает в контейнер с именем main 
        ///     требуемый PartialView
        /// </summary>
        public static MvcHtmlString AudioActionButton(this AjaxHelper ajaxHelper,
                                                   String actionName, 
                                                   String controllerName,
                                                   String areaName,
                                                   string buttonText,
                                                   string iconSrc,
                                                   object routeValues = null,
                                                   object htmlAttributes = null,
                                                   object iconAttributes = null)
        {
            RouteValueDictionary routeValueDictionary = new RouteValueDictionary(routeValues);

            if (!String.IsNullOrEmpty(actionName) && !routeValueDictionary.ContainsKey("action"))
            {
                routeValueDictionary.Add("action", actionName);
            }
            if (!String.IsNullOrEmpty(controllerName) && !routeValueDictionary.ContainsKey("controller"))
            {
                routeValueDictionary.Add("controller", controllerName);
            }
            if (!routeValueDictionary.ContainsKey("area"))
            {
                if (!String.IsNullOrEmpty(areaName))
                    routeValueDictionary.Add("area", areaName);
                else
                    routeValueDictionary.Add("area", "");
            }


            AjaxOptions ajaxOptions = new AjaxOptions()
            {
                UpdateTargetId = "mainAudio",
                InsertionMode = InsertionMode.Replace,
                HttpMethod = "GET"
            };

            return ActionButton(ajaxHelper, buttonText, iconSrc, ajaxOptions, routeValueDictionary,
                                routeValues, htmlAttributes, iconAttributes);
    /*        // Создаем маршрут
            RouteValueDictionary routeValueDictionary = new RouteValueDictionary(routeValues);

            if (!String.IsNullOrEmpty(actionName) && !routeValueDictionary.ContainsKey("action"))
            {
                routeValueDictionary.Add("action", actionName);
            }
            if (!String.IsNullOrEmpty(controllerName) && !routeValueDictionary.ContainsKey("controller"))
            {
                routeValueDictionary.Add("controller", controllerName);
            }
            if (!routeValueDictionary.ContainsKey("area"))
            {
                if (!String.IsNullOrEmpty(areaName))
                    routeValueDictionary.Add("area", areaName);
                else
                    routeValueDictionary.Add("area", "");
            }

            // Создаем параметры Ajax            
            AjaxOptions ajaxOptions = new AjaxOptions()
            {
                UpdateTargetId = "mainAudio",
                InsertionMode = InsertionMode.Replace,
                HttpMethod = "GET"
            };

            // Возвращаем строку
            String ajaxActionName = actionName;
            return ajaxHelper.ActionLink(linkText, ajaxActionName, null,
                routeValueDictionary, ajaxOptions, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));*/
        }

        /*
         * http://stackoverflow.com/questions/341649/asp-net-mvc-ajax-actionlink-with-image
         * @Ajax.ImageActionLink("../../Content/images/play64.png", "Delete", "Delete", new { id = 1}, new AjaxOptions { Confirm = "Delete contact?", HttpMethod = "Delete", UpdateTargetId = "divContactList" })
        
        public static IHtmlString ImageActionLink(this AjaxHelper helper, string imageUrl, string altText, string actionName, object routeValues, AjaxOptions ajaxOptions, object htmlAttributes = null)
        {
            var builder = new TagBuilder("img");
            builder.MergeAttribute("src", imageUrl);
            builder.MergeAttribute("alt", altText);
            builder.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            var link = helper.ActionLink("[replaceme]", actionName, routeValues, ajaxOptions).ToHtmlString();
            return MvcHtmlString.Create(link.Replace("[replaceme]", builder.ToString(TagRenderMode.SelfClosing)));
        }*/

        /*
         *             @Ajax.ActionImageLink("../../Content/images/play64.png", "Delete", "Delete", "AudioCon",null, null)
         */
        public static MvcHtmlString ActionImageLink(this AjaxHelper helper, string imageUrl, string altText, string actionName, string controller, object routeValues, AjaxOptions ajaxOptions)
        {
            var builder = new TagBuilder("img");
            builder.MergeAttribute("src", imageUrl);
            builder.MergeAttribute("alt", altText);
            var link = helper.ActionLink("[replaceme]", actionName, controller, routeValues, ajaxOptions);
            return new MvcHtmlString(link.ToHtmlString().Replace("[replaceme]", builder.ToString(TagRenderMode.SelfClosing)));
        }

        public static IHtmlString ImageActionLink(this AjaxHelper helper, string actionName, string iconSrc, AjaxOptions ajaxOptions)
        {
            var builder = new TagBuilder("img");
            builder.MergeAttribute("src", iconSrc);
            builder.MergeAttribute("title", "Добавить");

            var link = helper.ActionLink("[replaceme]", actionName, "Home", null, ajaxOptions).ToHtmlString();
            return new MvcHtmlString(link.Replace("[replaceme]", builder.ToString(TagRenderMode.SelfClosing)));
        }


        /*
         * Source
         * http://www.zombiehugs.com/2013/10/asp-net-mvc-ajax-action-button/
         */
        public static MvcHtmlString ActionButton(this AjaxHelper ajaxHelper,
                                                   string buttonText,
                                                   string iconSrc,
                                                   AjaxOptions ajaxOptions,
                                                   RouteValueDictionary htmlAttributeDictionary,
                                                   object routeValues = null,
                                                   object htmlAttributes = null,
                                                   object iconAttributes = null)
        {
            UrlHelper urlHelper = new UrlHelper(ajaxHelper.ViewContext.RequestContext);
          //  RouteValueDictionary htmlAttributeDictionary =
           //     HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            
            string imgUrl = urlHelper.Content(iconSrc);

            //Build icon for use in button
            TagBuilder buttonIcon = new TagBuilder("img");
            buttonIcon.MergeAttribute("src", imgUrl);
            buttonIcon.MergeAttributes(new RouteValueDictionary(iconAttributes));

            //Text
            TagBuilder text = new TagBuilder("span") { InnerHtml = buttonText };
            //Build Button and place text and icon inside
            TagBuilder button = new TagBuilder("button");
             
            button.MergeAttributes(ajaxOptions.ToUnobtrusiveHtmlAttributes());
            button.MergeAttributes(htmlAttributeDictionary);
            button.InnerHtml = buttonIcon.ToString(TagRenderMode.SelfClosing) + text.ToString();
            return MvcHtmlString.Create(button.ToString());
        }
    }
}