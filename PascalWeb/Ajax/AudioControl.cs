﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace PascalWeb.Ajax
{
    public class AudioControlAttributes
    {
        public bool Required { get; set; }
        public string Message { get; set; }
    }

    public static class AudioControlHelper
    {
        public static MvcHtmlString LegacyTextboxFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, AudioControlAttributes audioAttributes)
        {
            var fieldName = ExpressionHelper.GetExpressionText(expression);
            var fullBindingName = html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(fieldName);
            var fieldId = TagBuilder.CreateSanitizedId(fullBindingName);

            var metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            var value = metadata.Model;

            TagBuilder tag = new TagBuilder("audio");
            tag.Attributes.Add("name", fullBindingName);
            tag.Attributes.Add("id", fieldId);
            tag.Attributes.Add("type", "text");
            tag.Attributes.Add("autoplay", "autoplay");
            tag.Attributes.Add("value", value == null ? "" : value.ToString());

            if (audioAttributes != null)
            {
                if (audioAttributes.Required) tag.Attributes.Add("foo:required", "true");
                if (!String.IsNullOrEmpty(audioAttributes.Message)) tag.Attributes.Add("foo:message", audioAttributes.Message);
            }

            return new MvcHtmlString(tag.ToString(TagRenderMode.SelfClosing));
        }
    }
}