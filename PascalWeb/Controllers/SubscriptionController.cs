﻿using PascalWeb.Additional.Auth;
using PascalWeb.DB;
using PascalWeb.DB.PageModels;
using PascalWeb.DB.PageModels.Specific;
using PascalWeb.DB.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PascalWeb.Controllers
{
    [DynamicAuthorizeAttribute]
    public class SubscriptionController : ControllerBase
    {
        UnitOfWork unitOfWork = new UnitOfWork();

        public ActionResult ShowSubscription()
        {
            var userID = getCurrentUserID();

            var programSubscriptionList = unitOfWork.SubscriptionProgramRepository.Get(filter: obj => obj.UserID == userID && obj.Existed, orderBy: q => q.OrderByDescending(x => x.CreationDate),
                includeProperties: "Program");
            var blogSubscriptionList = unitOfWork.SubscriptionBlogRepository.Get(filter: obj => obj.UserID == userID && obj.Existed, orderBy: q => q.OrderByDescending(x => x.CreationDate),
                includeProperties: "Blog");

            SubscriptionProfileModel model = new SubscriptionProfileModel(programSubscriptionList, blogSubscriptionList);

            // add remaining programs for subscription
            var subscribedProgramIDList = programSubscriptionList.Select(x => x.ProgramID);
            var remainProgramList = unitOfWork.ProgramRepository.Get(filter: obj => !subscribedProgramIDList.Contains(obj.ID), orderBy: q => q.OrderByDescending(x => x.Name));
            for (int i = 0; i < remainProgramList.Count(); i++)
                model.SubscriptionProgramList.Add(new SubscriptionProfileModel.SubscriptionModel(remainProgramList.ElementAt(i)));
            
            // add remaining blogs for subscription
            var subscribedBlogIDList = blogSubscriptionList.Select(x => x.BlogID);
            var remainBlogList = unitOfWork.BlogRepository.Get(filter: obj => !subscribedBlogIDList.Contains(obj.ID), orderBy: q => q.OrderByDescending(x => x.Name));
            for (int i = 0; i < remainBlogList.Count(); i++)
                model.SubscriptionBlogList.Add(new SubscriptionProfileModel.SubscriptionModel(remainBlogList.ElementAt(i)));

                return PartialOrFullView(model: model);
        }

        /// <summary>
        /// subscribe to blog
        /// </summary>
        [HttpGet]
        public ActionResult BlogSubscribe(int blogID, bool on, bool changeToSubscibtionIndication = false)
        {
            long? userID = getCurrentUserID();
            if (!userID.HasValue)
                PartialOrFullView("_updateBlogSubscribeButton", model: on);

            SubscriptionController.blogSubscribe(unitOfWork, userID, blogID, on);
            SubscriptionButtonModel model = new SubscriptionButtonModel(blogID, on, changeToSubscibtionIndication);
            return PartialOrFullView("_updateBlogSubscribeButton", model: model);
        }

        /// <summary>
        /// subscribe to program
        /// </summary>
        [HttpGet]
        public ActionResult ProgramSubscribe(int programID, bool on, bool changeToSubscibtionIndication = false)
        {
            long? userID = getCurrentUserID();
            if (!userID.HasValue)
                PartialOrFullView("_updateProgramSubscribeButton", model: on);

            SubscriptionController.programSubscribe(unitOfWork, userID, programID, on);
            SubscriptionButtonModel model = new SubscriptionButtonModel(programID, on, changeToSubscibtionIndication);
            return PartialOrFullView("_updateProgramSubscribeButton", model: model);
        }

        public static bool? blogSubscribe(UnitOfWork unitOfWork, long? userID,int blogID, bool on)
        {
            SubscriptionBlog subcription = unitOfWork.SubscriptionBlogRepository.Get(filter: obj => obj.BlogID == blogID && obj.UserID == userID).FirstOrDefault();

            if (subcription == null)
            {
                subcription = new SubscriptionBlog();
                subcription.CreationDate = DateTime.Now;
                subcription.BlogID = blogID;
                subcription.UserID = userID.Value;
                subcription.Existed = on;
                unitOfWork.SubscriptionBlogRepository.Insert(subcription);
            }
            subcription.Existed = on;

            unitOfWork.Save();
            return !on;
        }

        public static bool? programSubscribe(UnitOfWork unitOfWork, long? userID, int programID, bool on)
        {
            SubscriptionProgram subcription = unitOfWork.SubscriptionProgramRepository.Get(filter: obj => obj.ProgramID == programID && obj.UserID == userID).FirstOrDefault();

            if (subcription == null)
            {
                subcription = new SubscriptionProgram();
                subcription.CreationDate = DateTime.Now;
                subcription.ProgramID = programID;
                subcription.UserID = userID.Value;
                subcription.Existed = on;
                unitOfWork.SubscriptionProgramRepository.Insert(subcription);
            }
            subcription.Existed = on;

            unitOfWork.Save();
            return !on;
        }

        /// <summary>
        /// check whether user is subscribed to blog
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="userID"></param>
        /// <param name="blogID"></param>
        /// <returns></returns>
        public static bool? checkBlogSuscription(UnitOfWork unitOfWork, long? userID, int blogID)
        {
            if (!userID.HasValue)
                return null;

            // #TODO
            SubscriptionBlog subcription = unitOfWork.SubscriptionBlogRepository.Get(filter: obj => obj.BlogID == blogID && obj.UserID == userID).FirstOrDefault();
            if (subcription == null || !subcription.Existed)
            {
                return false;
            }
            else
                return true;
        }

        /// <summary>
        /// check whether user is subscribed to program
        /// </summary>
        /// <param name="programID"></param>
        /// <returns></returns>
        public static bool? checkProgramSuscription(UnitOfWork unitOfWork, long? userID, int programID)
        {
            if (!userID.HasValue)
                return null;

            // #TODO
            SubscriptionProgram subcription = unitOfWork.SubscriptionProgramRepository.Get(filter: obj => obj.ProgramID == programID && obj.UserID == userID).FirstOrDefault();
            if (subcription == null || !subcription.Existed)
            {
                return false;
            }
            else
                return true;
        }
    }
}
