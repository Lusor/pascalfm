﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PascalWeb.DB;
using PascalWeb.DB.AuxiliaryModels;
using PascalWeb.DB.PageModels;
using PascalWeb.DB.Repositories;
using PascalWeb.Additional.Audio;
using PascalWeb.Additional;
using System.Threading;

namespace PascalWeb.Controllers
{
    public class ProfileController : ControllerBase
    {
        UnitOfWork unitOfWork = new UnitOfWork();

        public const string SESSION_PROGRAM_AUTHORS = "MANAGMENT_AUTHORS";

        //
        // GET: /Profile/

        public ActionResult Index()
        {
            // profile model
            ProfileModel model = new ProfileModel();

            // user info
            var currentUserID = getCurrentUserID().Value;
            var blogID = unitOfWork.BlogRepository.Get(filter: obj => obj.AuthorID == currentUserID).Select(obj => obj.ID).FirstOrDefault();
            var userExternal = unitOfWork.UserExternalRepository.GetByID(currentUserID);

            model.BlogID = blogID;
            model.UserID = currentUserID;
            model.RoleName = getRoleName();
            model.IsExternalAccount = (userExternal == null) ? true : false;

            // content of subscriptions
            var subscribedProgramEditionsData = getLastSubscriptionProgramEditions(currentUserID, 0, ConstantSettings.FP_MaxNumberOfLastProgramEditions);
            model.SubscribedProgramEditions = new ExpandingContainerModel<List<LastProgramEditionPageModel.LastProgramEdition>>(subscribedProgramEditionsData, "Profile", "GetMoreLastSubscriptionProgramEditionsAction");
            var subscribedBlogRecordsData = getLastSubscriptionBlogRecords(currentUserID, 0, ConstantSettings.FP_MaxNumberOfLastBlogRecords);
            model.SubscribedBlogRecords = new ExpandingContainerModel<List<BlogRecord>>(subscribedBlogRecordsData, "Profile", "GetMoreLastSubscriptionBlogRecordsAction");

            return base.PartialOrFullView(model: model);
        }


        [HttpPost]
        public ActionResult GetMoreLastSubscriptionProgramEditionsAction(int? pageNumber)
        {
            var page = pageNumber.HasValue ? pageNumber.Value : 0;
            int skipNumber = (page) * ConstantSettings.FP_MaxNumberOfLastProgramEditions;
            var currentUserID = getCurrentUserID().Value;
            var model = getLastSubscriptionProgramEditions(currentUserID, skipNumber, ConstantSettings.FP_MaxNumberOfLastProgramEditions);
            return base.PartialView("..//ProgramEdition//_TileList", model);
        }

        private List<LastProgramEditionPageModel.LastProgramEdition> getLastSubscriptionProgramEditions(long userID, int skip, int batch)
        {
            // get list of programIDs
            var subscribedProgramIDList = unitOfWork.SubscriptionProgramRepository.Get(filter: obj => obj.UserID == userID && obj.Existed).Select(s => s.ProgramID);

            // get from DB
            var dbEditions = unitOfWork.ProgramEditionRepository.Get(filter: obj => subscribedProgramIDList.Contains(obj.ProgramID) && obj.Published == true,
                orderBy: q => q.OrderByDescending(x => x.AirDate), includeProperties: "Program, Content", skip: skip, take: batch);

            // convert to model structure and add to the list
            var lastEditions = new List<LastProgramEditionPageModel.LastProgramEdition>();
            for (int i = 0; i < dbEditions.Count(); i++)
            {
                lastEditions.Add(LastProgramEditionPageModel.getLastProgramEditionPageModel(dbEditions.ElementAt(i)));
            }

            // prepare data for audio player
            List<AudioTransferData> audioData = AudioManager.setPlayList(lastEditions);
            if (skip == 0)
            {
                Session[AudioManager.PLAYLIST_SESSION_KEY] = audioData;
            }
            else
            {
                List<AudioTransferData> lastAudioData = (List<AudioTransferData>)Session[AudioManager.PLAYLIST_SESSION_KEY];
                if (lastAudioData != null)
                {
                    lastAudioData.AddRange(audioData);
                    Session[AudioManager.PLAYLIST_SESSION_KEY] = lastAudioData;
                } 
                else
                    Session[AudioManager.PLAYLIST_SESSION_KEY] = audioData;
            }

            return lastEditions;
        }

        [HttpPost]
        public ActionResult GetMoreLastSubscriptionBlogRecordsAction(int? pageNumber)
        {
            var page = pageNumber.HasValue ? pageNumber.Value : 0;
            int skipNumber = (page) * ConstantSettings.FP_MaxNumberOfLastBlogRecords;
            var currentUserID = getCurrentUserID().Value;
            var model = getLastSubscriptionBlogRecords(currentUserID, skipNumber, ConstantSettings.FP_MaxNumberOfLastBlogRecords);
            return base.PartialView("..//BlogRecord//_blogRecordList", model);
        }

        private List<BlogRecord> getLastSubscriptionBlogRecords(long userID, int skip, int batch)
        {
            // get list of programIDs
            var subscribedBlogIDList = unitOfWork.SubscriptionBlogRepository.Get(filter: obj => obj.UserID == userID && obj.Existed).Select(s => s.BlogID);

            // get from DB
            var dbBlogRecords = unitOfWork.BlogRecordRepository.Get(filter: obj => subscribedBlogIDList.Contains(obj.BlogID) && obj.Published == true,
                orderBy: q => q.OrderByDescending(x => x.CreationDate), includeProperties: "Blog", skip: skip, take: batch).ToList();

            return dbBlogRecords;
        }

        //
      

        /*
        * AUTHORS MANAGMENT
        */

        [HttpGet]
        public ActionResult UserSearchByLoginAction(string userData)
        {
            var users = new List<UserAuth>();
            if (!string.IsNullOrWhiteSpace(userData))
            {
                if (!string.IsNullOrWhiteSpace(userData))
                {
                    users = unitOfWork.UserManagment.searchUsersByNameAndEmail(userData).ToList();
                    users = users != null ? users : new List<UserAuth>();
                }
            }
            AuthorItemListCommonModel model = new AuthorItemListCommonModel(UserInfoModel.convertFromToUserInfoList(users),
                new ManageViewCommonModel("Profile", "AddUserAction", "programAuthors", "", ManageViewCommonModel.eActionType.Add));
            return base.PartialView("_authorsRepresentationForAttachingView", model);
        }

        [HttpGet]
        public ActionResult AddUserAction(long userID)
        {
            List<UserAuth> authors = new List<UserAuth>();

            UserAuth newUser = unitOfWork.UserAuthRepository.GetOneItem(obj => obj.ID == userID, "UserRole");
            if (newUser != null)
            {
                // check if list is existed in session
                if (Session[SESSION_PROGRAM_AUTHORS] != null)
                {
                    authors = (List<UserAuth>)Session[SESSION_PROGRAM_AUTHORS];

                    bool isUserFound = false;
                    for (int i = 0; i < authors.Count; i++)
                    {
                        if (authors[i].ID == userID)
                        {
                            isUserFound = true;
                            break;
                        }
                    }
                    if (!isUserFound)
                        authors.Add(newUser);
                    Session[SESSION_PROGRAM_AUTHORS] = authors;
                }
                else
                {
                    authors.Add(newUser);
                    Session.Add(SESSION_PROGRAM_AUTHORS, authors);
                }
            }

            AuthorItemListCommonModel model = new AuthorItemListCommonModel(UserInfoModel.convertFromToUserInfoList(authors),
                new ManageViewCommonModel("Profile", "DeleteUserAction", "programAuthors", "", ManageViewCommonModel.eActionType.Delete));
            return PartialView("_authorsRepresentationForAttachingView", model);
        }

        [HttpGet]
        public ActionResult DeleteUserAction(long userID)
        {
            var authors = new List<UserAuth>();

            if (Session[SESSION_PROGRAM_AUTHORS] != null)
            {
                authors = (List<UserAuth>)Session[SESSION_PROGRAM_AUTHORS];
                authors.RemoveAll(t => t.ID == userID);
                Session[SESSION_PROGRAM_AUTHORS] = authors;
            }

            AuthorItemListCommonModel model = new AuthorItemListCommonModel(UserInfoModel.convertFromToUserInfoList(authors),
                new ManageViewCommonModel("Profile", "DeleteUserAction", "programAuthors", "", ManageViewCommonModel.eActionType.Delete));
            return PartialView("_authorsRepresentationForAttachingView", model);
        }


        /*
        * AUTHORS MANAGMENT
        */


        /*
         for program - authors
         */

        [HttpGet]
        public ActionResult UserSearchByNameAndLogin(string userData)
        {
            var userInfoList = new List<UserInfoModel>();
            if (!string.IsNullOrWhiteSpace(userData))
            {            
                var userAuths = new List<UserAuth>();
                userAuths = unitOfWork.UserManagment.searchUsersByNameAndEmail(userData).ToList();
                //users = users != null ? users : new List<UserAuth>();

                userInfoList = UserInfoModel.convertFromToUserInfoList(userAuths);
            }

            AuthorItemListCommonModel model = new AuthorItemListCommonModel(userInfoList,
                new ManageViewCommonModel("Profile", "ShowUserData", "authorInfo", "", ManageViewCommonModel.eActionType.Add));
            return base.PartialView("_authorSearchResultForChoosingView", model);
        }


        const int itemsOnPage = 6;

        [HttpGet]
        public ActionResult ShowUserData(long id)
        {
            UserAuth userAuth = unitOfWork.UserAuthRepository.GetByID(id);
            UserInfo userInfo = unitOfWork.UserInfoRepository.GetByID(id);

            UserUnitedModel model = new UserUnitedModel(userAuth, userInfo);

            // get programs
            //var programs = unitOfWork.ProgramAuthorsRepository.Get(filter: obj => obj.UserID == id, 
            //    orderBy: q => q.OrderBy(x => x.Program.Name)).Select(s => s.Program);
            //model.setUserPrograms(programs.ToList());

            // get last editions
            var lastProgramEditionPageModel = getLastUserProgramEditionsWithPagination(id, 0);
            model.setUserProgramEditions(lastProgramEditionPageModel);

            return base.PartialView("ShowAuthorInfo", model);
        }

        private PageWithPagination<LastProgramEditionPageModel> getLastUserProgramEditionsWithPagination(long userID, int paginationNumber)
        {
            long fullItemsNumber = unitOfWork.ProgramEditionAuthorsRepository.Count(filter: obj => obj.UserID == userID);

            var lastEditions = unitOfWork.ProgramEditionAuthorsRepository.Get(filter: obj => obj.UserID == userID,
                orderBy: q => q.OrderByDescending(x => x.ProgramEdition.AirDate),
                skip: paginationNumber, take: itemsOnPage).Select(s => s.ProgramEdition);
            LastProgramEditionPageModel lastProgramEditionPageModel = LastProgramEditionPageModel.getLastProgramEditionModel(lastEditions);

            // form model
            int numberOfPages = (int)Math.Ceiling((double)fullItemsNumber / (double)itemsOnPage);
            PaginationModel paginationModel = new PaginationModel(numberOfPages, paginationNumber, new ManageViewCommonModel("ProfileController", "Index"));

            var model = new PageWithPagination<LastProgramEditionPageModel>(lastProgramEditionPageModel, paginationModel);
            return model;
        }


    }
}
