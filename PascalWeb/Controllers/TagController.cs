﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PascalWeb.DB.PageModels;
using PascalWeb.DB.Repositories;
using PascalWeb.DB;
using PascalWeb.DB.AuxiliaryModels;

namespace PascalWeb.Controllers
{
    public class TagController : ControllerBase
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        public const string SESSION_MANAGMENT_TAGS = "MANAGMENT_TAGS";

        //
        // GET: /Tag/

        public ActionResult Index(string query = "")
        {
            IEnumerable<Tag> dbTags;
            if(string.IsNullOrEmpty(query))
                dbTags = unitOfWork.TagRepository.Get(orderBy: q => q.OrderBy(x => x.Level).ThenBy(x => x.Name));
            else
                dbTags = unitOfWork.TagRepository.Get(filter: obj => obj.Name.Contains(query), orderBy: q => q.OrderBy(x => x.Level).ThenBy(x => x.Name));
            List<TagShowModel> model = new List<TagShowModel>();
            for (int i = 0; i < dbTags.Count(); i++)
            {
                var tagID = dbTags.ElementAt(i).ID;
                List<string> subTags = unitOfWork.TagRepository.Get(filter: obj => obj.ParentTagID == tagID,
                    orderBy: q => q.OrderBy(x => x.Name)).Select(s => s.Name).ToList();

                TagShowModel tagForShow = new TagShowModel(dbTags.ElementAt(i).ID, i + 1, 
                    dbTags.ElementAt(i).Name, 
                    dbTags.ElementAt(i).Level, 
                    dbTags.ElementAt(i).ParentTag !=  null ? dbTags.ElementAt(i).ParentTag.Name : null,
                    subTags);
                model.Add(tagForShow);
            }

            return View(model:model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Tag tag)
        {
            ModelState.Clear();
            if (TryUpdateModel(tag))
            {
                try
                {                    
                    tag.ParentTag = null;
                    if (tag.ID == 0)
                    {
                        unitOfWork.TagRepository.Insert(tag);
                    }
                    else
                    {
                        unitOfWork.TagRepository.Update(tag);
                    }
                    unitOfWork.Save();
                    return Json(new { success = true });
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }
            getLevelList();
            return PartialView("Create", tag);

        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var tag = unitOfWork.TagRepository.GetByID(id);
            tag.ParentTag = unitOfWork.TagRepository.GetByID(tag.ParentTagID);
            if (tag.ParentTag == null)
            {
                tag.ParentTag = new Tag();
                tag.ParentTag.Name = "[не задается]";
            }
            getLevelList();
            return base.PartialOrFullView("Create", tag);
        }

        [HttpGet]
        public ActionResult CreateChildTag(int? parentTagID, bool isFirstLevelTag)
        {
            var childTag = new Tag();

            if (parentTagID.HasValue)
            {
                childTag.ParentTag = unitOfWork.TagRepository.GetByID(parentTagID.Value);
                childTag.ParentTagID = parentTagID.Value;
                childTag.Level = childTag.ParentTag.Level + 1;
            }
            else
            {
                if (isFirstLevelTag)
                    childTag.Level = 1;
                childTag.ParentTag = new Tag();
                childTag.ParentTag.Name = "[не задается]";
            }
            getLevelList();
            return base.PartialOrFullView("Create", childTag);
        }

        private void getLevelList()
        {
            var userRoleList = (from item in Enumerable.Range(1, 4)
                select new
                {
                    ID = item,
                    Value = item
                }).ToList();

            ViewBag.LevelList = new SelectList(userRoleList, "ID", "Value");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var tag = unitOfWork.TagRepository.GetByID(id);
            var model = new DeleteDialogModel(id, tag.Name, "тэг", new ManageViewCommonModel("Tag", "Delete"));
            return PartialView("..//Shared//CommonViews//DeleteDialog", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int? id)
        {
            try
            {
                unitOfWork.TagRepository.Delete(id.Value);
                unitOfWork.Save();
                return Json(new { success = true, message = "123" });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.InnerException.InnerException.Message);

                var tag = unitOfWork.TagRepository.GetByID(id);
                var model = new DeleteDialogModel(id, tag.Name, "тэг", new ManageViewCommonModel("Tag", "Delete"));
                return PartialView("..//Shared//CommonViews//DeleteDialog", model);
            }
        }


        /*
         TAG MANAGMENT
         */

        [HttpGet]
        public ActionResult TagSearch(string tagFilter)
        {
            var tagList = new List<Tag>();
            if (!string.IsNullOrWhiteSpace(tagFilter))
            {
                tagList = unitOfWork.TagRepository.Get(filter: q => q.Name.Contains(tagFilter), orderBy: q => q.OrderBy(x => x.Name)).ToList();
                tagList = tagList != null ? tagList : new List<Tag>();
            }

            TagViewCommonModel model = new TagViewCommonModel(tagList,
                new ManageViewCommonModel("Tag", "AddTag", "tagProgramView", " (добавить)", ManageViewCommonModel.eActionType.Add));

            return PartialView("..//Tag//_tagCommonView", model);
        }


        [HttpGet]
        public ActionResult AddTag(int tagID)
        {
            var tagList = new List<Tag>();

            var newTag = unitOfWork.TagRepository.GetByID(tagID);
            if (newTag != null)
            {
                // check if list is existed in session
                if (Session[SESSION_MANAGMENT_TAGS] != null)
                    tagList = (List<Tag>)Session[SESSION_MANAGMENT_TAGS];

                bool isUserFound = false;
                for (int i = 0; i < tagList.Count; i++)
                {
                    if (tagList[i].Name.ToLower() == newTag.Name.ToLower())
                    {
                        isUserFound = true;
                        break;
                    }
                }
                if (!isUserFound)
                    tagList.Add(newTag);

                Session[SESSION_MANAGMENT_TAGS] = tagList;
            }

            TagViewCommonModel model = new TagViewCommonModel(tagList,
                new ManageViewCommonModel("Tag", "DeleteTag", "tagProgramView", " (удалить)", ManageViewCommonModel.eActionType.Delete));
            return PartialView("..//Tag//_tagCommonView", model);
        }


        [HttpGet]
        public ActionResult DeleteTag(int tagID)
        {
            var tagList = new List<Tag>();

            if (Session[SESSION_MANAGMENT_TAGS] != null)
            {
                tagList = (List<Tag>)Session[SESSION_MANAGMENT_TAGS];
                tagList.RemoveAll(t => t.ID == tagID);
                Session[SESSION_MANAGMENT_TAGS] = tagList;
            }

            TagViewCommonModel model = new TagViewCommonModel(tagList,
                new ManageViewCommonModel("Tag", "DeleteTag", "tagProgramView", " (удалить)", ManageViewCommonModel.eActionType.Delete));
            return PartialView("..//Tag//_tagCommonView", model);
        }
        /*
         TAG MANAGMENT
         */


        /*
         * addition methods for work with tags
         */

        private static string tagSplitter = "||";
        public static string tagListToString(List<Tag> tagList)
        {
            string tagString = "";
            if (tagList == null)
                return tagString;

            for (int i = 0; i < tagList.Count; i++)
            {
                tagString = string.Concat(tagString, tagList[i].Name);
                tagString = string.Concat(tagString, tagSplitter);
            }
            return tagString;
        }

        public static List<string> stringToTagList(string tagString)
        {
            if (string.IsNullOrWhiteSpace(tagString))
                return new List<string>();

            var tagList = tagString.Split(new string[] { tagSplitter }, StringSplitOptions.RemoveEmptyEntries).ToList();
            return tagList;
        }

    }
}
