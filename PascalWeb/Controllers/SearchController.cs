﻿using PascalWeb.DB.PageModels;
using PascalWeb.DB.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PascalWeb.Controllers
{
    public class SearchController : ControllerBase
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        //
        // GET: /Search/
        [HttpGet]
        public ActionResult Index(SearchModel modelData)
        {
            searchIt(modelData);
            return PartialOrFullView(model: modelData);
        }

        [HttpGet]
        public ActionResult PartialSearch(SearchModel modelData)
        {
            searchIt(modelData);
            return base.PartialView("_resultItems", model: modelData);
        }

        [HttpGet]
        public ActionResult Search(string query, SearchResult.SearchResultType searchType = SearchResult.SearchResultType.All)
        {
            SearchModel model = new SearchModel();
            model.Query = query;
            model.IsHashTag = true;
            model.IsSearchInPrograms = true;
            model.SearchType = searchType;
            return Index(model);
        }

        private List<string> getTagNameList(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return new List<string>();
            var array = query.Split(new char[] { ' ' });
            var result = (from word in array
                         where string.IsNullOrWhiteSpace(word) == false
                         select word.Replace('#', ' ').Trim().ToLower()).ToList<string>();

            return result;
        }


        private void searchIt(SearchModel model)
        {
            model.SearchResultList = new List<SearchResult>();
            //if (model.IsHashTag)
            {
                List<string> tagIDList_lowerCase = getTagNameList(model.Query);
                if (string.IsNullOrEmpty(model.Query))
                    return;

                searchInProgramEditions(ref model, tagIDList_lowerCase);
                searchInBlogRecords(ref model, tagIDList_lowerCase);

                model.SearchResultList = model.SearchResultList.OrderByDescending(q => q.PublishTime).ToList();
            }
        }

        private void searchInProgramEditions(ref SearchModel model, List<string> tagIDList_lowerCase)
        {
            var programEditionResults = unitOfWork.ProgramEditionRepository.Get(filter: obj => tagIDList_lowerCase.All(word => obj.TagString.ToLower().Contains(word))
                && obj.Published == true, includeProperties: "Program").Take(100);

            for (int i = 0; i < programEditionResults.Count(); i++)
            {
                var programEdition = programEditionResults.ElementAt(i);
                SearchResult item = new SearchResult();
                item.Description = programEdition.ShortDescription;
                item.ImagePath = programEdition.Program.SmallLogoPath;
                item.PublishTime = programEdition.AirDate;
                item.ResultType = SearchResult.SearchResultType.ProgramEdition;

                item.RecordName = programEdition.EditionTittle;
                item.RecordID = programEdition.ID;

                // parent info
                item.ParentName = programEdition.Program.Name;
                item.ParentID = programEdition.Program.ID;

                // add to result
                model.SearchResultList.Add(item);
                model.NumberOfResults = model.SearchResultList.Count();
            }
        }


        private void searchInBlogRecords(ref SearchModel model, List<string> tagIDList_lowerCase)
        {
            var blogRecordResults = unitOfWork.BlogRecordRepository.Get(filter: obj => tagIDList_lowerCase.All(word => obj.TagString.ToLower().Contains(word))
                    && obj.Published == true, includeProperties: "Blog").Take(100);

            for (int i = 0; i < blogRecordResults.Count(); i++)
            {
                var blogRecord = blogRecordResults.ElementAt(i);
                SearchResult item = new SearchResult();
                item.Description = blogRecord.NoteFragment;
                item.ImagePath = blogRecord.Blog.SmallLogoPath;
                item.PublishTime = blogRecord.CreationDate;
                item.ResultType = SearchResult.SearchResultType.Blog;

                item.RecordName = blogRecord.Title;
                item.RecordID = blogRecord.ID;

                // parent info
                item.ParentName = blogRecord.Blog.Name;
                item.ParentID = blogRecord.Blog.ID;

                // add to result
                model.SearchResultList.Add(item);
                model.NumberOfResults = model.SearchResultList.Count();
            }
        }
    }
}
