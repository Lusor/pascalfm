﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PascalWeb.DB;
using PascalWeb.DB.Enumerators;
using PascalWeb.DB.Repositories;
using PascalWeb.DB.PageModels;
using PascalWeb.DB.AuxiliaryModels;
using PascalWeb.Additional;
using PascalWeb.DB.PageModels.Specific;

namespace PascalWeb.Controllers
{
    //[DynamicAuthorizeAttribute]
    public class AdminController : ControllerBase
    {
        UnitOfWork unitOfWork = new UnitOfWork();

        /// <summary>
        /// get programs
        /// </summary>
        /// <returns></returns>
        public ActionResult AdminProgramView()
        {
            var currentUserID = getCurrentUserID(); //Membership.GetUser().ProviderUserKey
            string role = unitOfWork.UserManagment.getUserRoleNameByUserID(currentUserID.Value);

            var programList = new List<Program>();
            if (GeneralMethods.compareStrings(role, eUserRoles.superadmin.ToString()) || GeneralMethods.compareStrings(role, eUserRoles.admin.ToString()))
            {
                programList = unitOfWork.ProgramRepository.Get(orderBy: q => q.OrderBy(x => x.Name), includeProperties: "ProgramAndTag, ProgramAndTag.Tag").ToList();
            }
            else
            {
                programList = unitOfWork.ProgramAuthorsRepository.Get(filter: obj => obj.UserID == currentUserID,
                    orderBy: q => q.OrderBy(x => x.Program.Name), includeProperties: "Program.ProgramAndTag, Program.ProgramAndTag.Tag").Select(s => s.Program).ToList();
            }

            return PartialOrFullView(model: programList);
        }

        /// <summary>
        /// get program editions
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AdminProgramEditionView(int programID)
        {
            var currentUserID = getCurrentUserID().Value;
            string role = unitOfWork.UserManagment.getUserRoleNameByUserID(currentUserID);

            var programEditionList = new List<ProgramEdition>();
            if (GeneralMethods.compareStrings(role, eUserRoles.superadmin.ToString()) ||
                GeneralMethods.compareStrings(role, eUserRoles.admin.ToString()))
            {
                programEditionList = unitOfWork.ProgramEditionRepository.Get(filter: obj => obj.ProgramID == programID, orderBy: q => q.OrderByDescending(x => x.AirDate)).ToList();
            }
            else
            {
                // additional - check whether current user is author of program
                var checkAuthor = unitOfWork.ProgramAuthorsRepository.Count(filter: obj => obj.ProgramID == programID && obj.UserID == currentUserID);
                if (checkAuthor > 0)
                {
                    // it is necessary to relate user with program's author (not with program edition's author)
                    programEditionList = unitOfWork.ProgramEditionRepository.Get(filter: obj => obj.ProgramID == programID,
                        orderBy: q => q.OrderByDescending(x => x.AirDate)).ToList();
                }
                //programEditionList = unitOfWork.ProgramEditionAuthorsRepository.Get(filter: obj => obj.UserID == currentUserID && obj.ProgramEdition.Program.ID == programID,
                //    orderBy: q => q.OrderByDescending(x => x.ProgramEdition.AirDate)).Select(s => s.ProgramEdition).ToList();
            }

            ViewData.Add("programID", programID);
            return PartialOrFullView(model: programEditionList);
        }

        /// <summary>
        /// get users
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AdminUserView(string userData, int? roleID)
        {
            getUserRoleList();

            var userInfoList = new List<UserInfoModel>();
            var userAuths = new List<UserAuth>();
            if (!string.IsNullOrWhiteSpace(userData) || roleID.HasValue)
            {
                userAuths = unitOfWork.UserManagment.searchUsersByDataAndRole(userData, roleID, 100).ToList();
                userInfoList = UserInfoModel.convertFromToUserInfoList(userAuths);
            }
            else
            {
                userAuths = unitOfWork.UserManagment.searchAllUsersExceptRole(eUserRoles.generaluser, 100).ToList();
                userInfoList = UserInfoModel.convertFromToUserInfoList(userAuths);
            }

            UserSearchModel model = new UserSearchModel(userData, roleID);
            model.UserList = userInfoList;
            return PartialOrFullView(model: model);
        }

        ///// <summary>
        ///// search users
        ///// </summary>
        ///// <returns></returns>
        //[HttpGet]
        //public ActionResult UserSearch(string userData)
        //{
        //    var userInfoList = new List<UserInfoModel>();
        //    if (!string.IsNullOrWhiteSpace(userData))
        //    {
        //        var userAuths = new List<UserAuth>();
        //        userAuths = unitOfWork.UserManagment.searchUsersByNameAndEmail(userData, 100).ToList();
        //        userInfoList = UserInfoModel.convertFromToUserInfoList(userAuths);
        //    }
        //    else
        //    {
        //        var userAuths = new List<UserAuth>();
        //        userAuths = unitOfWork.UserManagment.searchUsersByRole(eUserRoles.journalist, 100).ToList();
        //        userInfoList = UserInfoModel.convertFromToUserInfoList(userAuths);
        //    }

        //    return PartialOrFullView("_userTable", userInfoList);
        //}

        [HttpGet]
        public ActionResult AdminUserRights()
        {
            var controllerList = SiteAnalysis.GetControllerNames().OrderBy(x => x).ToList();

            List<SiteActionModel> controllersWithAtions = new List<SiteActionModel>();

            for (int i = 0; i < controllerList.Count; i++)
            {
                var actionList = SiteAnalysis.ActionNames(controllerList[i]);
                var modifiedActionList = actionList.OrderBy(x => x).Distinct().ToList();

                var actionRuleList = new SortedList<string, ActionRule>();

                for (int j = 0; j < modifiedActionList.Count; j++)
                {
                    ActionRule actionRule = new ActionRule(modifiedActionList[j], new RightFeatures());
                    actionRuleList.Add(modifiedActionList[j], actionRule);
                }
                SiteActionModel item = new SiteActionModel(controllerList[i], actionRuleList);
                controllersWithAtions.Add(item);
            }

            actualizationRightModel(ref controllersWithAtions);

            return PartialOrFullView(model: controllersWithAtions);

        }

        /// <summary>
        /// get information about availibility of pages for each role
        /// </summary>
        /// <param name="model"></param>
        void actualizationRightModel(ref List<SiteActionModel> model)
        {
            for (int i = 0; i < model.Count; i++)
            {
                var itemFromModel = model[i];

                var query = unitOfWork.UserRightRepository.Get(filter: obj => obj.ControllerName.ToLower().CompareTo(itemFromModel.ControllerName) == 0,
                    includeProperties: "UserRole");

                for (int j = 0; j < query.Count(); j++)
                {
                    var right = query.ElementAt(j);
                    if (itemFromModel.ActionRules.ContainsKey(right.ActionName))
                    {
                        var actionRuleIndex = itemFromModel.ActionRules.Keys.IndexOf(right.ActionName);
                        string userRoleName = right.UserRole.Name;
                        if (GeneralMethods.compareStrings(userRoleName, eUserRoles.admin.ToString()) && right.IsAvailable)
                            itemFromModel.ActionRules.Values[actionRuleIndex].Rights.ForAdministrator = true;

                        if (GeneralMethods.compareStrings(userRoleName, eUserRoles.editor.ToString()) && right.IsAvailable)
                            itemFromModel.ActionRules.Values[actionRuleIndex].Rights.ForEditor = true;

                        if (GeneralMethods.compareStrings(userRoleName, eUserRoles.generaluser.ToString()) && right.IsAvailable)
                            itemFromModel.ActionRules.Values[actionRuleIndex].Rights.ForGeneralUser = true;

                        if (GeneralMethods.compareStrings(userRoleName, eUserRoles.journalist.ToString()) && right.IsAvailable)
                            itemFromModel.ActionRules.Values[actionRuleIndex].Rights.ForJournalist = true;

                        if (GeneralMethods.compareStrings(userRoleName, eUserRoles.moderator.ToString()) && right.IsAvailable)
                            itemFromModel.ActionRules.Values[actionRuleIndex].Rights.ForModerator = true;

                        //if (GeneralMethods.compareStrings(userRoleName, eUserRoles.superadmin.ToString()) && right.IsAvailable)
                        //    itemFromModel.ActionRules.Values[j].Rights. = true;

                        if (GeneralMethods.compareStrings(userRoleName, eUserRoles.withoutauth.ToString()) && right.IsAvailable)
                            itemFromModel.ActionRules.Values[actionRuleIndex].Rights.ForNotAuth = true;
                    }
                }
            }
        }


        [HttpGet]
        public ActionResult SetRight(string controllerName, string actionName, bool allRoles, eUserRoles userRole, bool isAddtion)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(actionName))
                {
                    if (!allRoles)
                    {
                        int? userRoleID = unitOfWork.UserManagment.getUserRoleIDByName(userRole.ToString());
                        var actionNameList = SiteAnalysis.ActionNames(controllerName + "controller").OrderBy(x => x).Distinct().ToList();
                        for (int i = 0; i < actionNameList.Count; i++)
                        {
                            AddUserRight(controllerName, actionNameList[i], userRoleID.Value, isAddtion);
                        }
                    }
                }
                else
                {
                    if (allRoles)
                    {
                        var userRoleIDList = unitOfWork.UserRoleRepository.Get().Select(x => x.ID);
                        foreach (int id in userRoleIDList)
                        {
                            AddUserRight(controllerName, actionName, id, isAddtion);
                        }
                    }
                    else
                    {
                        int? userRoleID = unitOfWork.UserManagment.getUserRoleIDByName(userRole.ToString());
                        AddUserRight(controllerName, actionName, userRoleID.Value, isAddtion);
                    }

                }

                unitOfWork.Save();
                return RedirectToAction("AdminUserRights");
            }
            catch (Exception)
            {
                return Json(new { success = false, message = "problems" }, JsonRequestBehavior.AllowGet);
            }
        }

        private void AddUserRight(string controllerName, string actionName, int userRoleID, bool isAddtion)
        {
            var dbUserRight = unitOfWork.UserRightRepository.Get(filter: obj => obj.ControllerName.CompareTo(controllerName) == 0 && 
                                                                         obj.ActionName.CompareTo(actionName) == 0 &&
                                                                         obj.UserRoleID == userRoleID).FirstOrDefault();
            if (dbUserRight == null)
            {
                UserRight newUserRight = new UserRight();
                newUserRight.ControllerName = controllerName;
                newUserRight.ActionName = actionName;
                newUserRight.IsAvailable = isAddtion;
                newUserRight.UserRoleID = userRoleID;
                unitOfWork.UserRightRepository.Insert(newUserRight);
            }
            else
            {
                dbUserRight.IsAvailable = isAddtion;
                unitOfWork.UserRightRepository.Update(dbUserRight);
            }
        }

        /*
         * Blogs
         */
        [HttpGet]
        public ActionResult AdminBlogView(int paginationNumber = 0, int? blogID = null)
        {
            var model = new AdminBlogPageModel();
            bool blogCheck = false;

            var currentUserID = getCurrentUserID().Value;

            // load blog info
            model.Blog = unitOfWork.BlogRepository.GetByID(blogID);

            // #IMPORTANT
            // checking whether user can see this blog

            if (model.Blog != null)
            {
                if (model.Blog.AuthorID == currentUserID)
                    blogCheck = true;
                else
                {
                    // check role
                    var userRole = unitOfWork.UserManagment.getUserRoleNameByUserID(currentUserID);
                    // for superadmin, admin and editor
                    bool specialPrivileges = GeneralMethods.compareStrings(userRole, eUserRoles.superadmin.ToString()) ||
                        GeneralMethods.compareStrings(userRole, eUserRoles.admin.ToString()) ||
                        GeneralMethods.compareStrings(userRole, eUserRoles.editor.ToString());

                    if (specialPrivileges)
                        blogCheck = true;
                }
            }

            if (!blogCheck)
            {
                // search own blog
                blogID = unitOfWork.BlogRepository.Get(filter: obj => obj.AuthorID == currentUserID).Select(obj => obj.ID).FirstOrDefault();
                model.Blog = unitOfWork.BlogRepository.GetByID(blogID);
                if (model.Blog != null)
                    blogCheck = true;
            }

            if (blogCheck)
            {            
                // pagination for table
                const int itemsOnPage = 50;

                // get full number of items
                long fullItemsNumber = unitOfWork.BlogRecordRepository.Count(filter: obj => obj.BlogID == blogID);

                // get program editions
                var blogRecords = unitOfWork.BlogRecordRepository.Get(filter: obj => obj.BlogID == blogID, orderBy: q => q.OrderByDescending(x => x.CreationDate),
                    includeProperties: "BlogRecordAndTag, BlogRecordAndTag.Tag",
                    skip: paginationNumber * itemsOnPage, take: itemsOnPage);
                //LastProgramEditionPageModel lastProgramEditionPageModel = getLastProgramEditionModel(lastEditions);

                // form model
                int numberOfPages = (int)Math.Ceiling((double)fullItemsNumber / (double)itemsOnPage);
                PaginationModel paginationModel = new PaginationModel(numberOfPages, paginationNumber, new ManageViewCommonModel("Admin", "AdminBlogView"));
                var tableModel = new PageWithPagination<IEnumerable<BlogRecord>>(blogRecords, paginationModel);
                model.TableData = tableModel;
            }

            return PartialOrFullView(model: model);
        }

        [HttpGet]
        public ActionResult AdminAllBlogs(string query = "")
        {
            if (string.IsNullOrEmpty(query))
                query = "";
            var blogs = unitOfWork.BlogRepository.Get(filter: obj => obj.Name.Contains(query), orderBy: q => q.OrderByDescending(x => x.Name),
                includeProperties: "UserAuth").ToList();
            var model = new QueryAndResultModel<List<Blog>>(blogs, query);
            return PartialOrFullView("AdminAllBlogsView", model: model);
        }


        #region Lists
        private void getUserRoleList()
        {
            var userRoleList = unitOfWork.UserRoleRepository.Get(
                orderBy: q => q.OrderBy(x => x.Alias)).Select(obj => new
                {
                    ID = obj.ID,
                    Value = obj.Alias
                }).ToList();

            ViewBag.UserRoles = new SelectList(userRoleList, "ID", "Value");
        }
        #endregion
    }
}
