﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using PascalWeb.Additional.Audio;
using PascalWeb.Additional.Auth;
using PascalWeb.Additional.Storage;
using PascalWeb.DB;
using PascalWeb.DB.PageModels;
using PascalWeb.DB.Repositories;
using PascalWeb.Additional.Cache;
using PascalWeb.Additional;
using PascalWeb.DB.AuxiliaryModels;
using Pulzonic.Multipartial;

namespace PascalWeb.Controllers
{
    [DynamicAuthorizeAttribute]
    public class HomeController : ControllerBase
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        //[HttpGet]
        //public ViewResultBase Index()
        //{
        //    System.Diagnostics.Debug.WriteLine("time1 home: " + DateTime.Now.ToString("hh.mm.ss.ffffff"));

        //    // get top edition of each program
        //    var topProgramEditions = CasheManager.getData<LastProgramEditionPageModel>(eCasheEnumeration.FP_TOP_PROGRAM_EDITIONS_BY_PROGRAM, unitOfWork);
        //    // get last edition of each program
        //    var lastProgramEditions = CasheManager.getData<LastProgramEditionPageModel>(eCasheEnumeration.FP_LAST_PROGRAM_EDITIONS_BY_PROGRAM, unitOfWork);

        //    // build model
        //    FirstPageModel model = new FirstPageModel(topProgramEditions.LastProgramEditions, lastProgramEditions.LastProgramEditions);

        //    model.ThemeList = unitOfWork.ProgramThemeRepository.Get(orderBy: q => q.OrderBy(x => x.Name)).ToList();

        //    System.Diagnostics.Debug.WriteLine("time2 home: " + DateTime.Now.ToString("hh.mm.ss.ffffff"));

        //    var list = model.TopProgramEditions.Concat(model.LastProgramEditions).ToList();
        //    // prepare data for audio player
        //    List<AudioTransferData> audioData = AudioManager.setPlayList(list);
        //    Session[AudioManager.PLAYLIST_SESSION_KEY] = audioData;

        //    System.Diagnostics.Debug.WriteLine("time3 home: " + DateTime.Now.ToString("hh.mm.ss.ffffff"));
        //    return base.PartialOrFullView("Index", model);
        //}

        // ViewResultBase
        [HttpGet]
        public ActionResult Index(int? themeID = null, long? authorID = null)
        {
            System.Diagnostics.Debug.WriteLine("time1 home: " + DateTime.Now.ToString("hh.mm.ss.ffffff"));

            // data for edition's tiles
            LastProgramEditionPageModel lastProgramEditions = null;
            List<BlogRecord> lastBlogRecords = null;

            if (themeID.HasValue)
            {
                lastProgramEditions = CacheQueries.lastProgramEditionsForFPQuery(unitOfWork, themeID: themeID);
                lastBlogRecords = CacheQueries.lastBlogRecordsForFPQuery(unitOfWork, themeID: themeID);

            }
            else if (authorID.HasValue)
            {
                lastProgramEditions = CacheQueries.lastProgramEditionsForFPQuery(unitOfWork, authorID: authorID);
                lastBlogRecords = CacheQueries.lastBlogRecordsForFPQuery(unitOfWork, authorID: authorID);
            }
            else
            {
                // get top editions of each program
                //topProgramEditions = CasheManager.getData<LastProgramEditionPageModel>(eCasheEnumeration.FP_TOP_PROGRAM_EDITIONS_BY_PROGRAM, unitOfWork);

                // get last records
                lastBlogRecords = CasheManager.getData<List<BlogRecord>>(eCasheEnumeration.FP_BLOG_RECORDS_LAST, unitOfWork);

                // get last editions
                lastProgramEditions = CasheManager.getData<LastProgramEditionPageModel>(eCasheEnumeration.FP_LAST_PROGRAM_EDITIONS, unitOfWork);
            }

            // data for popular author list
            var authorQuery = CasheManager.getData<List<UserAuth>>(eCasheEnumeration.FP_PROGRAM_TOP_AUTHOR_LIST, unitOfWork);
            List<UserInfoModel> authorList = getAuthorList(unitOfWork, true, true);

            Session["themeID"] = themeID;
            Session["authorID"] = authorID;

            var lastProgramEditionsModel = new ExpandingContainerModel<List<LastProgramEditionPageModel.LastProgramEdition>>(lastProgramEditions.LastProgramEditions, "Home", "GetMoreLastProgramEditionsAction");
            var lastBlogRecordsModel = new ExpandingContainerModel<List<BlogRecord>>(lastBlogRecords, "Home", "GetMoreLastBlogRecordsAction");

            // build model
            FirstPageModel model = new FirstPageModel(lastProgramEditions: lastProgramEditionsModel, lastBlogRecords: lastBlogRecordsModel);

            model.ChosenAuthorID = authorID;
            model.ChosenThemeID = themeID;

            model.ThemeList = getThemes(unitOfWork, true, true);
            model.PopularAuthorList = authorList;

            System.Diagnostics.Debug.WriteLine("time2 home: " + DateTime.Now.ToString("hh.mm.ss.ffffff"));

            var list = model.LastProgramEditions.Data;
            // prepare data for audio player
            List<AudioTransferData> audioData = AudioManager.setPlayList(list);
            Session[AudioManager.PLAYLIST_SESSION_KEY] = audioData;
            //AudioElement(audioData[0].EditionID);

            System.Diagnostics.Debug.WriteLine("time3 home: " + DateTime.Now.ToString("hh.mm.ss.ffffff"));
/*
            // multi partial result
            MultipartialResult result = new MultipartialResult(this);
            result.AddView("Index", null, model);



            AudioPlayerModel playerModel = new AudioPlayerModel();

            var item = new AudioPlayerModel.AudioElement();
            item.ProgramEditionID = audioData[0].EditionID;
            item.ProgramName = audioData[0].ProgramName;
            item.ProgramEditionName = audioData[0].ProgramEditionName;
            item.ResourceLink = audioData[0].ResourceLink;
            playerModel.Elements.Add(item);


            result.AddView("AudioElement", "mainAudio", playerModel);
            result.AddScript("alert ('ActionLink clicked');");
//            result.AddScript("MultipartialUpdate");
*/
            return base.PartialOrFullView("Index", model);
        }

        [HttpPost]
        public ActionResult GetMoreLastProgramEditionsAction(int? pageNumber)
        {
            int? themeID = Session["themeID"] != null ? (int?)Session["themeID"] : null;
            long? authorID = Session["authorID"] != null ? (long?)Session["authorID"] : null;

            var page = pageNumber.HasValue ? pageNumber.Value : 0;
            int skipNumber = (page) * ConstantSettings.FP_MaxNumberOfLastProgramEditions;
            var model = CacheQueries.lastProgramEditionsForFPQuery(unitOfWork, skipNumber, ConstantSettings.FP_MaxNumberOfLastProgramEditions, themeID, authorID);

            return base.PartialView("..//ProgramEdition//_TileList", model.LastProgramEditions);
        }

        [HttpPost]
        public ActionResult GetMoreLastBlogRecordsAction(int? pageNumber)
        {
            int? themeID = Session["themeID"] != null ? (int?)Session["themeID"] : null;
            long? authorID = Session["authorID"] != null ? (long?)Session["authorID"] : null;

            var page = pageNumber.HasValue ? pageNumber.Value : 0;
            int skipNumber = (page) * ConstantSettings.FP_MaxNumberOfLastBlogRecords;
            var model = CacheQueries.lastBlogRecordsForFPQuery(unitOfWork, skipNumber, ConstantSettings.FP_MaxNumberOfLastBlogRecords, themeID, authorID); 
            return base.PartialView("..//BlogRecord//_blogRecordList", model);
        }


        // #TODO - в отдельный класс

        /// <summary>
        /// get themes
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="forPrograms"></param>
        /// <param name="forBlogs"></param>
        /// <returns></returns>
        public static List<ProgramTheme> getThemes(UnitOfWork unitOfWork, bool forPrograms, bool forBlogs)
        {
            List<ProgramTheme> themes = new List<ProgramTheme>();

            // for programs
            if (forPrograms)
            {
                var _tempThemes = unitOfWork.ProgramRepository.Get(filter: obj => obj.ProgramTheme != null, orderBy: q => q.OrderBy(x => x.ProgramTheme.Order), includeProperties: "ProgramTheme").Select(s => s.ProgramTheme).Distinct().ToList();
                themes.AddRange(_tempThemes);
            }

            var previousIDs = themes.Select(x => x.ID);

            // for blogs
            if (forBlogs)
            {
                var _tempThemes = unitOfWork.BlogRepository.Get(filter: obj => obj.ProgramTheme != null && !previousIDs.Contains(obj.ProgramTheme.ID), orderBy: q => q.OrderBy(x => x.ProgramTheme.Order), includeProperties: "ProgramTheme").Select(s => s.ProgramTheme).Distinct().ToList();
                themes.AddRange(_tempThemes);
            }

            return themes;
        }


        public static List<UserInfoModel> getAuthorList(UnitOfWork unitOfWork, bool forPrograms, bool forBlogs)
        {
            List<UserAuth> authorQuery = new List<UserAuth>();
            List<UserInfoModel> authorList = new List<UserInfoModel>();

            // for programs
            if (forPrograms)
            {
                var _tempList = unitOfWork.ProgramAuthorsRepository.Get(includeProperties: "UserAuth").Select(s => s.UserAuth).Distinct().ToList();
                authorQuery.AddRange(_tempList);
            }

            var previousIDs = authorQuery.Select(x => x.ID).Distinct();

            // for blogs
            if (forBlogs)
            {
                var _tempList = unitOfWork.BlogRepository.Get(filter: obj => obj.AuthorID != null && !previousIDs.Contains(obj.AuthorID), includeProperties: "UserAuth").Select(s => s.UserAuth).ToList();
                authorQuery.AddRange(_tempList);
            }

            for (int i = 0; i < authorQuery.Count(); i++)
            {
                UserAuth _user = authorQuery[i];
                UserInfoModel userModel = new UserInfoModel(_user.ID, _user.Email, _user.Name, _user.Surname, _user.SmallLogoPath);
                userModel.ProgramList = unitOfWork.ProgramAuthorsRepository.Get(filter: obj => obj.UserID == _user.ID).Select(s => s.Program).ToList();
                userModel.Blog = _user.Blog != null && _user.Blog.Count > 0 ? _user.Blog.ElementAt(0) : null;
                authorList.Add(userModel);
            }
            return authorList;
        }


        [HttpGet]
        public ViewResultBase PageNotFound()
        {
            return base.PartialOrFullView();
        }
        

        /*public ActionResult MyAudio()
        {
            bool storageConnected = cStorageManager.getConnected();
            cStorageManager.createContainer("audio");

           // var file = @"http://cdn.echo.msk.ru/snd/2014-06-30-beseda-1635.mp3";
            var file = @"https://portalvhds8dzdrm51bz8fc.blob.core.windows.net/audio/2014/7/thunder";
            WebClient client = new WebClient();
            Stream stream = client.OpenRead(file);

            return File(stream, "123");

         //   return null;
        }


        public ViewResultBase SetAudio(string filePath)
        {
            bool storageConnected = cStorageManager.getConnected();
            cStorageManager.createContainer("audio");

            WebClient client = new WebClient();
            Stream stream = client.OpenRead(filePath);

            ViewBag.AudioPath = File(stream, "123");

            if (!ViewData.ContainsKey("AudioPath"))
                ViewData.Add("AudioPath", File(stream, "123"));
            else
                ViewData["AudioPath"] = File(stream, "123");

            return Index();
        }*/

        /// <summary>
        /// запуск только одного выпуска
        /// </summary>
        /// <param name="editionID"></param>
        /// <returns></returns>
        //[HttpGet]
        //public ActionResult AudioElementForOneEdition(long programEditionID)
        //{
        //    List<ProgramEdition> lastEditions = new List<ProgramEdition>();
        //    var edition = unitOfWork.ProgramEditionRepository.GetOneItem(obj => obj.ID == programEditionID, "Program, Content");
        //    if (edition != null /*&& edition.Published == true*/)
        //        lastEditions.Add(edition);

        //    List<AudioTransferData> audioData = AudioManager.setPlayList(lastEditions);
        //    Session[AudioManager.PLAYLIST_SESSION_KEY] = audioData;

        //    // add 1 to listened counter
        //    //if (startImmediatelyPlaying)
        //    //    ProgramEditionManagment.addToProgramEditionCounters(unitOfWork, Request, programEditionID, null, null, true, null);

        //    return AudioElement(programEditionID);
        //}

        /// <summary>
        /// загрузка списка выпусков
        /// </summary>
        /// <param name="editionID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AudioElement(long programEditionID)
        {
            List<AudioTransferData> model = (List<AudioTransferData>)Session[AudioManager.PLAYLIST_SESSION_KEY];

            if (model == null || model.Count == 0)
                return null;

            AudioPlayerModel playerModel = new AudioPlayerModel();
            //playerModel.StartImmediatelyPlaying = startImmediatelyPlaying;

            bool beginPlaylist = false;
            for (int i = 0; i < model.Count(); i++)
            {
                // search for the requested item
                if (programEditionID == model[i].EditionID)
                    beginPlaylist = true;

                // and form playlist
                if (beginPlaylist)
                {
                    var item = new AudioPlayerModel.AudioElement();
                    item.ProgramEditionID = model[i].EditionID;
                    item.ProgramName = model[i].ProgramName;
                    item.ProgramEditionName = model[i].ProgramEditionName;
                    item.ResourceLink = model[i].ResourceLink;
                    playerModel.Elements.Add(item);
                    if (playerModel.Elements.Count == 1)
                        playerModel.FirstElement = item;
                }
            }

            //if (startImmediatelyPlaying)
            //{
            //    AddToListenCounterForEdition(programEditionID);
            //}

            return PartialOrFullView("AudioElement", playerModel);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="programEditionID"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddToListenCounterForEdition(long programEditionID) //string programEditionID
        {
            // add 1 to listened counter
            ProgramEditionManagment.addToProgramEditionCounters(unitOfWork, Request, programEditionID, null, null, true, null);
            return Json(true);
        }

        public ActionResult Contact()
        {
            return PartialOrFullView();
        }

        public ActionResult About()
        {
            //cStorageManager.setSpecialSettings();
            return PartialOrFullView();
        }

        public ActionResult Message(string message)
        {
            return PartialOrFullView("..//Shared//CommonViews//Message", message);
        }    
    }
}
