﻿using PascalWeb.Additional;
using PascalWeb.Additional.Auth;
using PascalWeb.Additional.RSS;
using PascalWeb.DB.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace PascalWeb.Controllers
{
    [DynamicAuthorizeAttribute]
    public class RSSController : ControllerBase
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        public RSSFeedResult ITunes()
        {
            try
            {
                // get program list
                var programIDList = unitOfWork.RSSFeedAndProgramRepository.Get(filter: obj => obj.RSSFeedID == 1).Select(s => s.ProgramID).ToList();

                // get editions
                var programEditionList = unitOfWork.ProgramEditionRepository.Get(filter: obj => programIDList.Contains(obj.ProgramID) == true && obj.Published == true, includeProperties: "Program, Content", orderBy: q => q.OrderByDescending(x => x.ID)).ToList();
                var feedObject = RSSFeed.createRSSDoc(programEditionList);
                return new RSSFeedResult(feedObject);
            }
            catch (Exception exc)
            {
                PascalLogging.WriteErrorMessage("RSSController", "RSS", exc.Message);
                return new RSSFeedResult(new SyndicationFeed());
            }
        }

        public FileStreamResult SoundStream()
        {
            try
            {
                var programIDList = unitOfWork.RSSFeedAndProgramRepository.Get(filter: obj => obj.RSSFeedID == 1).Select(s => s.ProgramID).ToList();

                // get editions
                var programEditionList = unitOfWork.ProgramEditionRepository.Get(filter: obj => programIDList.Contains(obj.ProgramID) == true && obj.Published == true, includeProperties: "Program, Content", orderBy: q => q.OrderByDescending(x => x.ID)).ToList();
                var feedObject = RSSFeed.createRSSDoc(programEditionList);

                var feedText = new RSSFeedResult(feedObject);

                // write data to file
                var byteArray = Encoding.UTF8.GetBytes(feedText.Content);
                var stream = new MemoryStream(byteArray);
                return File(stream, "text/plain", "rss.xml");   
            }
            catch (Exception exc)
            {
                PascalLogging.WriteErrorMessage("RSSController", "RSS", exc.Message);
                return null;
            }
        }




        public RSSFeedResult Test()
        {
            try
            {
                // get program list
                var programIDList = unitOfWork.RSSFeedAndProgramRepository.Get(filter: obj => obj.RSSFeedID == 1).Select(s => s.ProgramID).ToList();

                // get editions
                var programEditionList = unitOfWork.ProgramEditionRepository.Get(filter: obj => programIDList.Contains(obj.ProgramID) == true && obj.Published == true, includeProperties: "Program, Content", orderBy: q => q.OrderByDescending(x => x.ID)).ToList();
                var feedObject = RSSFeed.createRSSDocTEST(programEditionList);
                return new RSSFeedResult(feedObject);
            }
            catch (Exception exc)
            {
                PascalLogging.WriteErrorMessage("RSSController", "RSS", exc.Message);
                return new RSSFeedResult(new SyndicationFeed());
            }
        }
    }
}
