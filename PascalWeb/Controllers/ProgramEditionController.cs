﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.WindowsAzure.Storage.Blob;
using PascalWeb.Additional.Auth;
using PascalWeb.Additional.Storage;
using PascalWeb.DB;
using PascalWeb.DB.AuxiliaryModels;
using PascalWeb.DB.PageModels;
using PascalWeb.DB.Repositories;
using PascalWeb.Additional;
using PascalWeb.Additional.FileProcessing;
using PascalWeb.DB.Enumerators;
using PascalWeb.Additional.Audio;

namespace PascalWeb.Controllers
{
    [DynamicAuthorizeAttribute]
    public class ProgramEditionController : ControllerBase
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        private PascalDBEntities context = new PascalDBEntities();

        //
        // GET: /ProgramEdition/Details/5

        public ActionResult Details(long id = 0)
        {
            ProgramEdition programEdition = unitOfWork.ProgramEditionRepository.GetOneItem(obj => obj.ID == id, "Content, Program, ProgramEditionText");
            if (programEdition == null)
            {
                return HttpNotFound();
            }
            Session["SelectedProgramEditionID"] = id;

            ProgramEditionPageView model = new ProgramEditionPageView();
            model.ProgramEdition = programEdition;
            model.Comments = getProgramEditionComments(null, 0, ConstantSettings.CommentBatchNumber);
            model.Recommendation = getRecommendationList(unitOfWork, programEdition.Recommendations);

            List<AudioTransferData> audioData = AudioManager.setPlayList(model.Recommendation.LastProgramEditions);
            audioData.Insert(0, new AudioTransferData(programEdition));

            Session[AudioManager.PLAYLIST_SESSION_KEY] = audioData;
           // Session[AudioManager.PLAYLIST_SESSION_KEY] = audioData;

   
            addToProgramEditionCounters(false, true, false, false);

            return PartialOrFullView(model: model);
        }

        private LastProgramEditionPageModel getRecommendationList(UnitOfWork unitOfWork, string recommendationString)
        {
            var peList = new List<ProgramEdition>();
            List<long> editionIDList = Serialization.stringToLongList(recommendationString);
            for (int i = 0; i < 4 && i < editionIDList.Count; i++)
            {
                long editionID = editionIDList[i];
                ProgramEdition pe = unitOfWork.ProgramEditionRepository.GetOneItem(filter: obj => obj.ID == editionID, includeProperties: "Content, Program");
                peList.Add(pe);
            }
            var model = LastProgramEditionPageModel.getLastProgramEditionModel(peList);

            return model;
        }

        // ----------------------------------------------------------------------------------------------
        // COMMENTS
        // ----------------------------------------------------------------------------------------------



        [HttpPost]
        public ActionResult GetMoreProgramEditionComments(int? pageNumber)
        {
            var page = pageNumber.HasValue ? pageNumber.Value : 0;

            int skipNumber = (page) * ConstantSettings.CommentBatchNumber;
            var model = getProgramEditionComments(null, skipNumber, ConstantSettings.CommentBatchNumber).CommentsList;
            return base.PartialView("_ViewCommentList", model);
        }

        /// <summary>
        /// get n comments
        /// </summary>
        /// <param name="programEditionID"></param>
        /// <param name="lastCommentID"></param>
        /// <returns></returns>
        private CommentBoxModel getProgramEditionComments(int? lastCommentID, int skipSize, int batchSize)
        {
            long? currentUserID = getCurrentUserID();

            string roleName = getRoleName();
            bool alwaysCanEdit = ((roleName == eUserRoles.admin.ToString()) || (roleName == eUserRoles.superadmin.ToString()) ||
                            (roleName == eUserRoles.moderator.ToString()) || (roleName == eUserRoles.editor.ToString()));
            long? programEditionID = null;
            if (Session["SelectedProgramEditionID"] != null)
                programEditionID = (long)Session["SelectedProgramEditionID"];

            var programEditionComments = unitOfWork.CommentProgramEditionRepository.Get(filter: obj => obj.ProgramEditionID == programEditionID && !obj.Deleted, 
                includeProperties: "UserAuth").Skip(skipSize).Take(batchSize);
            List<CommonCommentViewModel> comments = new List<CommonCommentViewModel>();
            for (int i = 0; i < programEditionComments.Count(); i++)
            {
                var comment = programEditionComments.ElementAt(i);
                CommonCommentViewModel _comment = new CommonCommentViewModel(comment.ID,
                                                                    comment.UserAuth,
                                                                    comment.CreationDate,
                                                                    comment.Comment,
                                                                    (alwaysCanEdit || (currentUserID == comment.UserAuth.ID)), 
                                                                    "ProgramEdition");
                comments.Add(_comment);
            }

            //int wholeCommentNumber = unitOfWork.CommentProgramEditionRepository.Get(obj => obj.ProgramEditionID == programEditionID && !obj.Deleted).Count();
            //User.Identity.
      
            CommentBoxModel model = new CommentBoxModel(comments, new int?(), new KeyValuePair<string, string>("GetMoreProgramEditionComments", "ProgramEdition"), unitOfWork.UserManagment.getUserLogoByID(getCurrentUserID()));
            return model;
        }

        /// <summary>
        /// Add comment
        /// </summary>
        /// <param name="commentText"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AddComment(string commentText)
        {
            var programEditionID = (long)Session["SelectedProgramEditionID"];
            if (!string.IsNullOrWhiteSpace(commentText))
            {
                CommentProgramEdition comment = new CommentProgramEdition();
                comment.Comment = commentText;
                comment.CreationDate = DateTime.Now;
                comment.ProgramEditionID = programEditionID;
                comment.UserID = getCurrentUserID().Value;

                unitOfWork.CommentProgramEditionRepository.Insert(comment);
                unitOfWork.Save();
                addToProgramEditionCounters(true, false, false, false);
            }

            CommentBoxModel model = getProgramEditionComments(null, 0, ConstantSettings.CommentBatchNumber);

            return base.PartialView("_ViewCommentBox", model);
        }

        /// <summary>
        /// Delete comment
        /// </summary>
        /// <param name="commentID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DeleteComment(long commentID)
        {
            var comment = unitOfWork.CommentProgramEditionRepository.GetByID(commentID);
            unitOfWork.CommentProgramEditionRepository.Delete(comment);
            unitOfWork.Save();
            addToProgramEditionCounters(true, false, false, false);

           // long ProgramEditionID = (long)Session["SelectedProgramEditionID"];
            CommentBoxModel model = getProgramEditionComments(null, 0, ConstantSettings.CommentBatchNumber);
            return base.PartialView("_ViewCommentBox", model);
        }

        /// <summary>
        /// Delete comment
        /// </summary>
        /// <param name="commentID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult EditComment(long commentID)
        {
            var comment = unitOfWork.CommentProgramEditionRepository.GetByID(commentID);
           // unitOfWork.CommentProgramEditionRepository.Delete(comment);
            unitOfWork.Save();

            //long ProgramEditionID = (long)Session["SelectedProgramEditionID"];
            CommentBoxModel model = getProgramEditionComments(null, 0, ConstantSettings.CommentBatchNumber);
            return base.PartialView("_ViewCommentBox", model);
        }


        // ----------------------------------------------------------------------------------------------
        //  Counters
        // ----------------------------------------------------------------------------------------------
        private bool addToProgramEditionCounters(bool? updateCommentCounter, bool? addToViewedCounter, bool? addToListenedCounter, bool? addToDownloadCounter)
        {
            try
            {
                var programEditionID = (long)Session["SelectedProgramEditionID"];
                return ProgramEditionManagment.addToProgramEditionCounters(unitOfWork, Request, programEditionID, updateCommentCounter, addToViewedCounter, addToListenedCounter, addToDownloadCounter);
            }
            catch (Exception)
            {
                // logging
                return false;
            }
        }


        // ----------------------------------------------------------------------------------------------
        // 
        // ----------------------------------------------------------------------------------------------


        //
        // GET: /ProgramEdition/Create

        public ActionResult Create(int programID)
        {
            ViewBag.ProgramID = new SelectList(unitOfWork.getContext().Program, "ID", "Name");

            // clear session data
            Session[TagController.SESSION_MANAGMENT_TAGS] = null;
            Session[ProfileController.SESSION_PROGRAM_AUTHORS] = null;

            // for dropdownList
            var programsList = unitOfWork.ProgramRepository.Get(
                orderBy: q => q.OrderBy(x => x.Name)).Select(obj => new
                {
                    ID = obj.ID,
                    Value = obj.Name
                }).ToList();

            ViewBag.Programs = new SelectList(programsList, "ID", "Value");

            var model = new ProgramEditionEditPageModel();
            model.ProgramEdition.CreationDate = DateTime.Now;
            model.ProgramEdition.AirDate = DateTime.Now;
            model.ProgramEdition.ProgramID = programID;
            model.ProgramEdition.Duration = 0;

            return base.PartialOrFullView("Edit", model: model);
        }

        public ActionResult Edit(long? id)
        {
            ProgramEdition programEdition = unitOfWork.ProgramEditionRepository.GetOneItem(obj => obj.ID == id, "ProgramEditionAuthors, ProgramEditionAuthors.UserAuth, ProgramEditionAuthors.UserAuth.UserRole");
            if (programEdition == null)
            {
                return HttpNotFound();
            }

            ProgramEditionText programEditionText = unitOfWork.ProgramEditionTextRepository.GetByID(programEdition.ID);
            // programedition text

            // for dropdownList
            var programsList = unitOfWork.ProgramRepository.Get(
                orderBy: q => q.OrderBy(x => x.Name)).Select(obj => new
                {
                    ID = obj.ID,
                    Value = obj.Name
                }).ToList();

            ViewBag.Programs = new SelectList(programsList, "ID", "Value");

            // get authors from db
            List<UserAuth> authors = (from obj in programEdition.ProgramEditionAuthors
                                        select obj.UserAuth).ToList();

            // get tags from db
            var tagsList = unitOfWork.ProgramEditionAndTagRepository.Get(filter: obj => obj.ProgramEditionID == id,
                orderBy: q => q.OrderBy(x => x.Tag.Name), includeProperties: "Tag").Select(obj => obj.Tag).ToList();
            Session[TagController.SESSION_MANAGMENT_TAGS] = tagsList;

            Session[ProfileController.SESSION_PROGRAM_AUTHORS] = authors;
            var programModel = new ProgramEditionEditPageModel(programEdition, programEditionText, authors, tagsList);

            return PartialOrFullView("Edit", model: programModel);
        }

        //
        // POST: /ProgramEdition/Edit/5

        [HttpPost]
        public ActionResult Edit(ProgramEditionEditPageModel programModel, HttpPostedFileBase file)
        {
            long programEditionID = programModel.ProgramEdition.ID;

            // checking whether file was attached or will be attached
            bool fileIsExisted = (programModel.ProgramEdition.ContentID != null) || (file != null);

            // for program dropdownList
            var programsList = unitOfWork.ProgramRepository.Get(
                orderBy: q => q.OrderBy(x => x.Name)).Select(obj => new
                {
                    ID = obj.ID,
                    Value = obj.Name
                }).ToList();

            ViewBag.Programs = new SelectList(programsList, "ID", "Value");

            if (!fileIsExisted)
                ModelState.AddModelError(string.Empty, "Аудио файл не прикреплен");
            if (ModelState.IsValid && fileIsExisted)
            {
                unitOfWork.getContext().Entry(programModel.ProgramEdition).State = EntityState.Modified;

                if (file != null)
                {
                    // delete previous

                    if (programModel.ProgramEdition.ContentID.HasValue)
                    {
                        cContentManager.deleteAudioContent(unitOfWork, programModel.ProgramEdition.ContentID.Value);
                        programModel.ProgramEdition.Content = null;
                    }

                    DB.Content content = null;
                    bool saved = cContentManager.saveAudioContent(unitOfWork, file, getCurrentUserID().Value, programModel.ProgramEdition.CreationDate, ref content);

                    // get duration of audio file
                    int duration = cFileWork.getMp3FileDuration(file);
                    programModel.ProgramEdition.Duration = duration;
                    if (!saved)
                    {
                        return HttpNotFound();
                    }
                    programModel.ProgramEdition.Content = content;
                }
                else
                {
                    unitOfWork.getContext().Entry(programModel.ProgramEdition).Property(obj => obj.ContentID).IsModified = false;
                }

                // if new
                if (programModel.ProgramEdition.ID == 0)
                {
                    unitOfWork.ProgramEditionRepository.Insert(programModel.ProgramEdition);
                    unitOfWork.Save();
                }
                else
                {
                    unitOfWork.getContext().Entry(programModel.ProgramEdition).Property(obj => obj.CommentsCounter).IsModified = false;
                    unitOfWork.getContext().Entry(programModel.ProgramEdition).Property(obj => obj.ListenedCounter).IsModified = false;
                    unitOfWork.getContext().Entry(programModel.ProgramEdition).Property(obj => obj.ViewedCounter).IsModified = false;
                    unitOfWork.getContext().Entry(programModel.ProgramEdition).Property(obj => obj.DownloadCounter).IsModified = false;
                }

                // save authors
                saveRelationWithAuthors(programModel.ProgramEdition.ID);
                // save tags
                saveRelationWithTags(programModel.ProgramEdition.ID);
                // addition save for tags
                programModel.ProgramEdition.TagString = TagController.tagListToString((List<Tag>)Session[TagController.SESSION_MANAGMENT_TAGS]);


                var programEditionTextTest = unitOfWork.ProgramEditionTextRepository.GetByID(programModel.ProgramEdition.ID);
                if (programEditionTextTest == null || programEditionTextTest.ProgramEditionID == 0)
                {
                    programModel.ProgramEditionText.ProgramEditionID = programModel.ProgramEdition.ID;
                    if (string.IsNullOrEmpty(programModel.ProgramEditionText.Note))
                        programModel.ProgramEditionText.Note = "";
                    unitOfWork.ProgramEditionTextRepository.Insert(programModel.ProgramEditionText);
                    //unitOfWork.Save();
                }
                else
                {
                    if (programModel.ProgramEditionText.Note != null)
                        programModel.ProgramEdition.ProgramEditionText.Note = programModel.ProgramEditionText.Note;
                    else
                        programModel.ProgramEdition.ProgramEditionText.Note = "";
                }


                //unitOfWork.ProgramEditionRepository.Update(programModel.ProgramEdition);
                unitOfWork.Save();
                return RedirectToAction("AdminProgramEditionView", "Admin", routeValues: new { programID = programModel.ProgramEdition.ProgramID });
            }

            return PartialOrFullView("Edit", model: programModel);
        }


        private void saveRelationWithAuthors(long programEditionID)
        {
            if (Session[ProfileController.SESSION_PROGRAM_AUTHORS] != null)
            {
                var newAuthors = (from user in (List<UserAuth>)Session[ProfileController.SESSION_PROGRAM_AUTHORS]
                                  select user.ID).ToList();

                var programAuthors = unitOfWork.ProgramEditionAuthorsRepository.Get(filter:
                    relation => relation.ProgramEditionID == programEditionID);
                var previousAuthors = (from obj in programAuthors
                                       select obj.UserID).ToList();

                var forDeleting = previousAuthors.Except(newAuthors);
                var forAdding = newAuthors.Except(previousAuthors);

                for (int i = 0; i < forDeleting.Count(); i++)
                {
                    long userID = forDeleting.ElementAt(i);

                    ProgramEditionAuthors pa = unitOfWork.ProgramEditionAuthorsRepository.Get(filter:
                            relation => relation.ProgramEditionID == programEditionID &&
                                        relation.UserID == userID).FirstOrDefault();

                    if (pa != null)
                        unitOfWork.ProgramEditionAuthorsRepository.Delete(pa);
                }

                for (int i = 0; i < forAdding.Count(); i++)
                {
                    ProgramEditionAuthors pa = new ProgramEditionAuthors();
                    pa.ProgramEditionID = programEditionID;
                    pa.UserID = forAdding.ElementAt(i);
                    unitOfWork.ProgramEditionAuthorsRepository.Insert(pa);
                }
            }
        }


        private void saveRelationWithTags(long programEditionID)
        {
            if (Session[TagController.SESSION_MANAGMENT_TAGS] != null)
            {
                var newTagIDs = (from tag in (List<Tag>)Session[TagController.SESSION_MANAGMENT_TAGS]
                                 select tag.ID).ToList();

                var previousTagIDs = unitOfWork.ProgramEditionAndTagRepository.Get(filter:
                    relation => relation.ProgramEditionID == programEditionID).Select(obj => obj.TagID);

                var forDeleting = previousTagIDs.Except(newTagIDs);
                var forAdding = newTagIDs.Except(previousTagIDs);

                for (int i = 0; i < forDeleting.Count(); i++)
                {
                    long tagID = forDeleting.ElementAt(i);

                    var pa = unitOfWork.ProgramEditionAndTagRepository.Get(filter:
                            relation => relation.ProgramEditionID == programEditionID &&
                                        relation.TagID == tagID).FirstOrDefault();

                    if (pa != null)
                        unitOfWork.ProgramEditionAndTagRepository.Delete(pa);
                }

                for (int i = 0; i < forAdding.Count(); i++)
                {
                    var pa = new ProgramEditionAndTag();
                    pa.ProgramEditionID = programEditionID;
                    pa.TagID = forAdding.ElementAt(i);
                    unitOfWork.ProgramEditionAndTagRepository.Insert(pa);
                }
            }
        }

        //
        // GET: /ProgramEdition/Delete/5

        public ActionResult Delete(long id = 0)
        {
            ProgramEdition programedition = unitOfWork.ProgramEditionRepository.GetByID(id);
            var model = new DeleteDialogModel(id, programedition.EditionTittle, "выпуск", new ManageViewCommonModel("ProgramEdition", "Delete"));
            return PartialView("..//Shared//CommonViews//DeleteDialog", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int? id)
        {
            if (id.HasValue)
            {
                var programEdition = unitOfWork.ProgramEditionRepository.GetByID(id.Value);
                deleteRelationWithAuthors(id.Value);
                deleteRelationWithTags(id.Value);
                deleteProgramEditionText(id.Value);
                deleteRelatedContent(programEdition);
                unitOfWork.ProgramEditionRepository.Delete(id.Value);
                unitOfWork.Save();
                return Json(new { success = true, message = "success" });
            }
            else
                return Json(new { success = false, message = "problems" });
        }

        /*
         *  Authors
         */
        private void deleteRelationWithAuthors(long programEditionID)
        {
            var programEditionAndAuthorRelations = unitOfWork.ProgramEditionAuthorsRepository.Get(filter:
                relation => relation.ProgramEditionID == programEditionID);
            for (int i = 0; i < programEditionAndAuthorRelations.Count(); i++)
            {
                unitOfWork.ProgramEditionAuthorsRepository.Delete(programEditionAndAuthorRelations.ElementAt(i));
            }
        }

        /*
          Tags
        */
        private void deleteRelationWithTags(long programEditionID)
        {
            var programEditionAndTagRelations = unitOfWork.ProgramEditionAndTagRepository.Get(filter:
                relation => relation.ProgramEditionID == programEditionID);
            for (int i = 0; i < programEditionAndTagRelations.Count(); i++)
            {
                unitOfWork.ProgramEditionAndTagRepository.Delete(programEditionAndTagRelations.ElementAt(i));
            }
        }

        /*
         * Content
         */ 
        private void deleteRelatedContent(ProgramEdition programEdition)
        {
            if (programEdition.ContentID.HasValue)
            {
                cContentManager.deleteAudioContent(unitOfWork, programEdition.ContentID.Value);
            }
        }

        /*
          ProgramEditionText
        */
        private void deleteProgramEditionText(long programEditionID)
        {
            var programEditionText = unitOfWork.ProgramEditionTextRepository.GetByID(programEditionID);
            if (programEditionText != null)
                unitOfWork.ProgramEditionTextRepository.Delete(programEditionText);
        }


        [HttpGet]
        public ActionResult Publish(long id, bool publish)
        {
            try
            {
                var programEdition = unitOfWork.ProgramEditionRepository.GetByID(id);
                programEdition.Published = publish;
                unitOfWork.ProgramEditionRepository.Update(programEdition);
                unitOfWork.Save();
                return RedirectToAction("AdminProgramEditionView", "Admin", new { programID = programEdition .ProgramID});
                //return Json(new { success = true, message = "success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false, message = "problems" }, JsonRequestBehavior.AllowGet);
            }
        }
      

        protected override void Dispose(bool disposing)
        {
            unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}