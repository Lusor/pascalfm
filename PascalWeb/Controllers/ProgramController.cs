﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using PascalWeb.Additional;
using PascalWeb.Additional.Audio;
using PascalWeb.Additional.Auth;
using PascalWeb.DB;
using PascalWeb.DB.AuxiliaryModels;
using PascalWeb.DB.Enumerators;
using PascalWeb.DB.PageModels;
using PascalWeb.DB.Repositories;
using PascalWeb.Additional.ImageProcessing;
using System.Drawing;
using PascalWeb.Additional.Storage;
using System.Data.Entity.Infrastructure;
using PascalWeb.Additional.Cache;

namespace PascalWeb.Controllers
{
    [DynamicAuthorizeAttribute]
    public class ProgramController : ControllerBase
    {
        UnitOfWork unitOfWork = new UnitOfWork();
       
        //
        // GET: /Program/

        //public ViewResultBase Index(int paginationNumber = 0)
        //{
        //    var lastProgramEditionPageModel = CasheManager.getData<LastProgramEditionPageModel>(eCasheEnumeration.FP_LAST_PROGRAM_EDITIONS, unitOfWork);
                
        //    // prepare data for audio player
        //    List<AudioTransferData> audioData = AudioManager.setPlayList(lastProgramEditionPageModel.LastProgramEditions);
        //    Session[AudioManager.PLAYLIST_SESSION_KEY] = audioData;

        //    // prepare model
        //    PaginationModel paginationmodel = new PaginationModel(1, paginationNumber, new ManageViewCommonModel("Program", "Index"));

        //    var model = new PageWithPagination<LastProgramEditionPageModel>(lastProgramEditionPageModel, paginationmodel);
        //    return PartialOrFullView("Index", model);
        //}



        public ViewResultBase Index(int? programID = null)
        {
            // get theme's list
            var programThemeList = HomeController.getThemes(unitOfWork, true, false);
            var themeWithProgramsList = new List<ListElementWithChildren<Program>>();

            for (int i = 0; i < programThemeList.Count(); i++)
            {
                var themeWithPrograms = new ListElementWithChildren<Program>(programThemeList.ElementAt(i).Name, programThemeList.ElementAt(i).Program);
                themeWithProgramsList.Add(themeWithPrograms);
            }

            List<UserInfoModel> authorList = HomeController.getAuthorList(unitOfWork, true, false);

            ProgramThemePageModel model = null;
            // if particlural program was chosen
            if (programID.HasValue)
            {
                ProgramPageModel programInfo = ((ViewResultBase)Details(programID.Value, 0)).Model as ProgramPageModel;
                model = new ProgramThemePageModel(themeWithProgramsList, authorList, programInfo);
            }
            else
            {
                // else show popular programs
                var popularProgramList = getPopularProgramList();
                model = new ProgramThemePageModel(themeWithProgramsList, authorList, popularProgramList);
            }

            return PartialOrFullView(model: model);
        }

        
        IEnumerable<Program> getPopularProgramList()
        {
            var popularProgram = unitOfWork.ProgramRepository.Get(orderBy: q => q.OrderByDescending(x => x.ProgramEdition.Sum(s => s.ViewedCounter)));
            return popularProgram;
        }

        //public ViewResultBase ShowProgramsByTheme(int? programID = null)
        //{
        //    System.Diagnostics.Debug.WriteLine("time00 home: " + DateTime.Now.ToString("hh.mm.ss.ffffff"));

        //    // get theme's list
        //    var programThemeList = unitOfWork.ProgramThemeRepository.Get(orderBy: q => q.OrderBy(x => x.Name), includeProperties:"Program");
        //    var themeWithProgramsList = new List<ThemeWithChildren<Program>>();

        //    for (int i = 0; i < programThemeList.Count(); i++)
        //    {
        //        var themeWithPrograms = new ThemeWithChildren<Program>(programThemeList.ElementAt(i).Name, programThemeList.ElementAt(i).Program);
        //        themeWithProgramsList.Add(themeWithPrograms);
        //    }
        //    System.Diagnostics.Debug.WriteLine("time01 home: " + DateTime.Now.ToString("hh.mm.ss.ffffff"));

        //    ProgramThemePageModel model = null;
        //    // if particlural program was chosen
        //    if (programID.HasValue)
        //    {
        //        ProgramPageModel programInfo = ((ViewResultBase)Details(programID.Value, 0)).Model as ProgramPageModel;
        //        model = new ProgramThemePageModel(themeWithProgramsList, null, programInfo);
        //    }
        //    else
        //    {
        //        System.Diagnostics.Debug.WriteLine("time1 home: " + DateTime.Now.ToString("hh.mm.ss.ffffff"));
        //        // else show popular programs
        //        var popularProgramList = getPopularProgramList();
        //        model = new ProgramThemePageModel(themeWithProgramsList, null, popularProgramList);
        //        System.Diagnostics.Debug.WriteLine("time2 home: " + DateTime.Now.ToString("hh.mm.ss.ffffff"));
        //    }

        //    return PartialOrFullView(model:model);
        //}

       // public ViewResultBase ShowProgramsByAuthors(long? userID = null)
       // {
       //     System.Diagnostics.Debug.WriteLine("time00 home: " + DateTime.Now.ToString("hh.mm.ss.ffffff"));
       //     // prepare left author's list

       //     var authorQuery = CasheManager.getData<List<UserAuth>>(eCasheEnumeration.FP_PROGRAM_TOP_AUTHOR_LIST, unitOfWork);
       ////     var authorQuery = unitOfWork.ProgramAuthorsRepository.Get(includeProperties: "UserAuth, Program").Select(obj => obj.UserAuth).Distinct();
       //     List<UserInfoModel> authorList = new List<UserInfoModel>();
       //     for (int i = 0; i < authorQuery.Count(); i++)
       //     {
       //         UserAuth _user = authorQuery[i];
       //         UserInfoModel userModel = new UserInfoModel(_user.ID, _user.Email, _user.Name, _user.Surname, _user.SmallLogoPath);
       //         authorList.Add(userModel);
       //     }
       //     System.Diagnostics.Debug.WriteLine("time01 home: " + DateTime.Now.ToString("hh.mm.ss.ffffff"));
         
       //     ProgramAuthorPageModel model = new ProgramAuthorPageModel(authorList);

       //     if (userID.HasValue) // get specific information about author
       //     {
       //         ProgramAuthorPageModel.AuthorInfoModel authorInfo = new ProgramAuthorPageModel.AuthorInfoModel();
       //         authorInfo.UserData = new UserUnitedModel();
       //         authorInfo.UserData.UserAuth = unitOfWork.UserAuthRepository.GetOneItem(obj => obj.ID == userID, "ProgramAuthors, ProgramAuthors.Program");
       //         authorInfo.UserData.UserInfo = unitOfWork.UserInfoRepository.GetByID(userID);

       //         var currentUserID = getCurrentUserID();
       //         var authorProgramList = new List<KeyValuePair<Program, bool?>>();
       //         for (int i = 0; i < authorInfo.UserData.UserAuth.ProgramAuthors.Count; i++)
       //         {
       //             var pa = authorInfo.UserData.UserAuth.ProgramAuthors.ElementAt(i);
       //             // check whether user is subscribed to program
       //             bool? isSubscribedToProgram = currentUserID.HasValue ? SubscriptionController.checkProgramSuscription(unitOfWork, currentUserID, pa.ProgramID) : null;
       //             authorProgramList.Add(new KeyValuePair<Program, bool?>(pa.Program, isSubscribedToProgram));
       //         }
       //         authorInfo.UserData.UserPrograms = authorProgramList;

       //         // check blog subscription
       //         var blogID = unitOfWork.BlogRepository.Get(filter: obj => obj.AuthorID == userID.Value).Select(obj => obj.ID).FirstOrDefault();
       //         bool? isSubscribedToBlog = currentUserID.HasValue ? SubscriptionController.checkBlogSuscription(unitOfWork, currentUserID, blogID) : null;
       //         authorInfo.BlogSubscription = new KeyValuePair<int?, bool?>(blogID, isSubscribedToBlog);


       //         // get last editions
       //         //var authorProgramIDList = unitOfWork.ProgramAuthorsRepository.Get(filter: obj => obj.UserID == userID).Select(obj => obj.ProgramID);
       //         var authorProgramIDList = authorInfo.UserData.UserAuth.ProgramAuthors.Select(obj => obj.ProgramID);

       //         // get last program editions
       //         var lastEditions =  unitOfWork.ProgramEditionRepository.Get(filter: obj => obj.Published == true && authorProgramIDList.Contains(obj.ProgramID), 
       //             orderBy: q => q.OrderByDescending(x => x.AirDate), includeProperties: "Content, Program");
       //         LastProgramEditionPageModel lastProgramEditionPageModel = LastProgramEditionPageModel.getLastProgramEditionModel(lastEditions);
       //         authorInfo.lastEditions = lastProgramEditionPageModel.LastProgramEditions;

       //         // prepare data for audio player
       //         List<AudioTransferData> audioData = AudioManager.setPlayList(lastProgramEditionPageModel.LastProgramEditions);
       //         Session[AudioManager.PLAYLIST_SESSION_KEY] = audioData;

       //         // get last blog records
       //         var blogRecords = unitOfWork.BlogRecordRepository.Get(filter: obj => obj.Published == true && obj.Blog.AuthorID == userID.Value, orderBy: q => q.OrderByDescending(x => x.CreationDate)).ToList();
       //         authorInfo.lastBlogRecords = blogRecords;

       //         model.AuthorInfo = authorInfo;
       //     }
       //     else // get list of best authors
       //     {
       //         model.PopularAuthors = authorQuery;
       //     }

       //     return PartialOrFullView(model: model);
       // }


        //
        // GET: /Program/Details/5

        public ActionResult Details(int id = 0, int paginationNumber = 0) 
        {
            const int itemsInTable = ConstantSettings.ProgramEditionNumberOnProgramPage;
            Program program = unitOfWork.ProgramRepository.GetOneItem(obj => obj.ID == id, "ProgramAuthors, ProgramAuthors.UserAuth");
            if (program == null)
            {
                return HttpNotFound();
            }

            var lastEditions = unitOfWork.ProgramEditionRepository.Get(filter: obj => obj.ProgramID == id && obj.Published == true,
                orderBy: q => q.OrderByDescending(x => x.AirDate), includeProperties: "Content",
                skip: paginationNumber * itemsInTable, take: itemsInTable);

                        // get full number of items
            long fullItemsNumber = unitOfWork.ProgramEditionRepository.Count(filter: obj => obj.ProgramID == id && obj.Published == true);

            int numberOfPages = (int)Math.Ceiling((double)fullItemsNumber / (double)itemsInTable);
            PageWithPagination<LastProgramEditionPageModel> lastProgramEditionPageModel = new PageWithPagination<LastProgramEditionPageModel>(LastProgramEditionPageModel.getLastProgramEditionModel(lastEditions), 
                new PaginationModel(numberOfPages, paginationNumber, new ManageViewCommonModel("Program", "Details")));

            ProgramPageModel programPageModel = new ProgramPageModel(program, lastProgramEditionPageModel);

            // prepare data for audio player
            List<AudioTransferData> audioData = AudioManager.setPlayList(lastEditions.ToList());
            Session[AudioManager.PLAYLIST_SESSION_KEY] = audioData;

            long? userID = getCurrentUserID();
            programPageModel.UserIsSubscribed = SubscriptionController.checkProgramSuscription(unitOfWork, userID, id);

            return PartialOrFullView(model: programPageModel);
        }

        public ActionResult Create()
        {
            // clear session data
            Session[TagController.SESSION_MANAGMENT_TAGS] = null;
            Session[ProfileController.SESSION_PROGRAM_AUTHORS] = null;

            // for dropdownList #NECESSARY
            var programThemes = unitOfWork.ProgramThemeRepository.Get(
                orderBy: q => q.OrderBy(x => x.Name)).Select(obj => new
                {
                    ID = obj.ID,
                    Value = obj.Name
                }).ToList();

            ViewBag.ProgramThemes = new SelectList(programThemes, "ID", "Value");

            ProgramEditPageModel model = new ProgramEditPageModel();
            return base.PartialOrFullView("Edit", model: model);
        }


        public ActionResult Edit(int id = 0)
        {
            Program program = unitOfWork.ProgramRepository.GetOneItem(obj => obj.ID == id, "ProgramAuthors, ProgramAuthors.UserAuth, ProgramAuthors.UserAuth.UserRole");
            if (program == null)
            {
                return HttpNotFound();
            }

            // get authors from db
            List<UserAuth> authors = (from obj in program.ProgramAuthors
                                      select obj.UserAuth).ToList();
            // get tags from db
            var tagsList = unitOfWork.ProgramAndTagRepository.Get(filter: obj => obj.ProgramID == id,
                orderBy: q => q.OrderBy(x => x.Tag.Name), includeProperties: "Tag").Select(obj => obj.Tag).ToList();
            Session[TagController.SESSION_MANAGMENT_TAGS] = tagsList;

            Session[ProfileController.SESSION_PROGRAM_AUTHORS] = authors;
            var programModel = new ProgramEditPageModel(program, authors, tagsList);

            // for dropdownList #NECESSARY
            var programThemes = unitOfWork.ProgramThemeRepository.Get(
                orderBy: q => q.OrderBy(x => x.Name)).Select(obj => new
                {
                    ID = obj.ID,
                    Value = obj.Name
                }).ToList();

            ViewBag.ProgramThemes = new SelectList(programThemes, "ID", "Value");

            return PartialOrFullView(model: programModel);
        }        

        /// <summary>
        /// create or edit record
        /// </summary>
        /// <param name="programModel"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateOrEdit(ProgramEditPageModel programModel, HttpPostedFileBase file)
        {
            bool newRecord = (programModel.Program.ID == 0);
            string previousSmallImageAbsoluteUri = null, previousMediumImageAbsoluteUri = null, previousSourceImageAbsoluteUri = null;
            if (!newRecord)
            {
                // save paths for possible deletion
                Program previousProgram = unitOfWork.ProgramRepository.GetByID(programModel.Program.ID);
                previousSmallImageAbsoluteUri = previousProgram.SmallLogoPath;
                previousMediumImageAbsoluteUri = previousProgram.MediumLogoPath;
                previousSourceImageAbsoluteUri = previousProgram.SourceLogoPath;
                // detach record from context (because of presence of two entity with the same key)
                unitOfWork.getContext().Entry(previousProgram).State = EntityState.Detached; 
            }

            if (ModelState.IsValid)
            {
                if (newRecord)
                {
                    programModel.Program.CreationDate = DateTime.Now;

                    ModelState.Clear();
                    bool val = TryValidateModel(programModel.Program);
                }
                else
                    unitOfWork.getContext().Entry(programModel.Program).State = EntityState.Modified;

                // save logo images
                if (file != null)
                {
                    byte[] imageBytes = cFileWork.ConvertToBytes(file);

                    Image smallImage = null, mediumImage = null;
                    cFileWork.convertIconImage(imageBytes, out smallImage, out mediumImage);

                    var smallImageBytes = ImageProcessing.ImageToByte(smallImage);
                    programModel.Program.SmallLogo = null;
                    programModel.Program.LogoContentType = file.ContentType;

                    string smallImageAbsoluteUri = null;
                    bool res = GeneralImageStorage.saveProgramImages(smallImage, DateTime.Now, out smallImageAbsoluteUri);
                    string mediumImageAbsoluteUri = null;
                    res = GeneralImageStorage.saveProgramImages(mediumImage, DateTime.Now, out mediumImageAbsoluteUri);
                    string sourceImageAbsoluteUri = null;
                    res = GeneralImageStorage.saveProgramImages(file, DateTime.Now, out sourceImageAbsoluteUri);

                    programModel.Program.SmallLogoPath = smallImageAbsoluteUri;
                    programModel.Program.MediumLogoPath = mediumImageAbsoluteUri;
                    programModel.Program.SourceLogoPath = sourceImageAbsoluteUri;
                }                
                else
                {
                    // объект должен быть чтобы его менять
                    if (programModel.Program.ID > 0)
                    {
                        unitOfWork.getContext().Entry(programModel.Program).Property(obj => obj.SmallLogo).IsModified = false;
                        unitOfWork.getContext().Entry(programModel.Program).Property(obj => obj.LogoContentType).IsModified = false;

                        // for two additional images
                        unitOfWork.getContext().Entry(programModel.Program).Property(obj => obj.SmallLogoPath).IsModified = false;
                        unitOfWork.getContext().Entry(programModel.Program).Property(obj => obj.MediumLogoPath).IsModified = false;
                        unitOfWork.getContext().Entry(programModel.Program).Property(obj => obj.SourceLogoPath).IsModified = false;
                    }
                    // it is unnecessary to delete previous images
                    previousSmallImageAbsoluteUri = null;
                    previousMediumImageAbsoluteUri = null;
                    previousSourceImageAbsoluteUri = null;
                }

                if (newRecord)
                    unitOfWork.ProgramRepository.Insert(programModel.Program);
                // without Update!!!
                unitOfWork.Save();

                // save authors
                saveRelationWithAuthors(programModel.Program.ID);
                // save tags
                saveRelationWithTags(programModel.Program.ID);

                unitOfWork.Save();

                // delete previous images
                GeneralImageStorage.deleteFile(previousSmallImageAbsoluteUri);
                GeneralImageStorage.deleteFile(previousMediumImageAbsoluteUri);
                GeneralImageStorage.deleteFile(previousSourceImageAbsoluteUri);

                return RedirectToAction("..//Admin//AdminProgramView");
            }

            return PartialOrFullView(model: programModel);

        }

        /*
         * author managment
         */

        private void saveRelationWithAuthors(int programID)
        {
            if (Session[ProfileController.SESSION_PROGRAM_AUTHORS] != null)
            {
                var newAuthors = (from user in (List<UserAuth>)Session[ProfileController.SESSION_PROGRAM_AUTHORS]
                                  select user.ID).ToList();

                var programAuthors = unitOfWork.ProgramAuthorsRepository.Get(filter:
                    relation => relation.ProgramID == programID);
                var previousAuthors = (from obj in programAuthors
                                       select obj.UserID).ToList();

                var forDeleting = previousAuthors.Except(newAuthors);
                var forAdding = newAuthors.Except(previousAuthors);

                for (int i = 0; i < forDeleting.Count(); i++)
                {
                    long userID = forDeleting.ElementAt(i);

                    ProgramAuthors pa = unitOfWork.ProgramAuthorsRepository.Get(filter:
                            relation => relation.ProgramID == programID &&
                                        relation.UserID == userID).FirstOrDefault();

                    if (pa != null)
                        unitOfWork.ProgramAuthorsRepository.Delete(pa);
                }

                for (int i = 0; i < forAdding.Count(); i++)
                {
                    ProgramAuthors pa = new ProgramAuthors();
                    pa.ProgramID = programID;
                    pa.UserID = forAdding.ElementAt(i);
                    unitOfWork.ProgramAuthorsRepository.Insert(pa);
                }
            }
        }


        /*
         * Tag managment
         */
        private void saveRelationWithTags(int programID)
        {
            if (Session[TagController.SESSION_MANAGMENT_TAGS] != null)
            {
                var newTagIDs = (from tag in (List<Tag>)Session[TagController.SESSION_MANAGMENT_TAGS]
                                 select tag.ID).ToList();

                var previousTagIDs = unitOfWork.ProgramAndTagRepository.Get(filter:
                    relation => relation.ProgramID == programID).Select(obj => obj.TagID);

                var forDeleting = previousTagIDs.Except(newTagIDs);
                var forAdding = newTagIDs.Except(previousTagIDs);

                for (int i = 0; i < forDeleting.Count(); i++)
                {
                    long tagID = forDeleting.ElementAt(i);

                    var pa = unitOfWork.ProgramAndTagRepository.Get(filter:
                            relation => relation.ProgramID == programID &&
                                        relation.TagID == tagID).FirstOrDefault();

                    if (pa != null)
                        unitOfWork.ProgramAndTagRepository.Delete(pa);
                }

                for (int i = 0; i < forAdding.Count(); i++)
                {
                    var pa = new ProgramAndTag();
                    pa.ProgramID = programID;
                    pa.TagID = forAdding.ElementAt(i);
                    unitOfWork.ProgramAndTagRepository.Insert(pa);
                }
            }
        }

        public FileContentResult DisplayLogo(int id)
        {
            Program program = unitOfWork.ProgramRepository.GetByID(id);
            if (program.SmallLogo == null)
                return null;
            return new FileContentResult(program.SmallLogo, program.LogoContentType);
        }

        //
        // GET: /Program/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Program program = unitOfWork.ProgramRepository.GetByID(id);
            if (program == null)
            {
                return HttpNotFound();
            }

            var model = new DeleteDialogModel(id, program.Name, "подкаст", new ManageViewCommonModel("Program", "Delete"));
            return PartialView("..//Shared//CommonViews//DeleteDialog", model);
        }

        //
        // POST: /Program/Delete/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int? id)
        {
            if (id.HasValue)
            {
                deleteRelationWithAuthors(id.Value);
                deleteRelationWithTags(id.Value);
                deleteRelationWithSubscription(id.Value);
                unitOfWork.ProgramRepository.Delete(id.Value);
                unitOfWork.Save();
                return Json(new { success = true, message = "success" });
            }
            else
                return Json(new { success = false, message = "problems" });
        }

        private void deleteRelationWithAuthors(int programID)
        {
            var programAndAuthorRelations = unitOfWork.ProgramAuthorsRepository.Get(filter:
                relation => relation.ProgramID == programID);
            for (int i = 0; i < programAndAuthorRelations.Count(); i++)
            {
                unitOfWork.ProgramAuthorsRepository.Delete(programAndAuthorRelations.ElementAt(i));
            }
        }

        private void deleteRelationWithTags(int programID)
        {
            var programAndTagRelations = unitOfWork.ProgramAndTagRepository.Get(filter:
                relation => relation.ProgramID == programID);
            for (int i = 0; i < programAndTagRelations.Count(); i++)
            {
                unitOfWork.ProgramAndTagRepository.Delete(programAndTagRelations.ElementAt(i));
            }
        }

        private void deleteRelationWithSubscription(int programID)
        {
            var programAndSubRelations = unitOfWork.SubscriptionProgramRepository.Get(filter:
                relation => relation.ProgramID == programID);
            for (int i = 0; i < programAndSubRelations.Count(); i++)
            {
                unitOfWork.SubscriptionProgramRepository.Delete(programAndSubRelations.ElementAt(i));
            }
        }

        protected override void Dispose(bool disposing)
        {
            //Session["PROGRAM_AUTHORS"] = null;
            unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}