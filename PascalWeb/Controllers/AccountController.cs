﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using PascalWeb.Filters;
using PascalWeb.Models;
using System.Web.Routing;
using PascalWeb.Additional.Auth;
using System.Web.Script.Serialization;
using PascalWeb.DB;
using PascalWeb.DB.Repositories;
using PascalWeb.DB.PageModels;
using PascalWeb.DB.Enumerators;
using PascalWeb.DB.AuxiliaryModels;
using PascalWeb.DB.PageModels.Registration;
using PascalWeb.Additional;
using ForecastMVC.Other_classes;
using PascalWeb.Additional.ImageProcessing;
using PascalWeb.Additional.Email;
using PascalWeb.DB.PageModels.Specific;
using System.IO;
using DotNetOpenAuth.AspNet.Clients;
using System.Net;
using System.Drawing;
using PascalWeb.Additional.Storage;

namespace PascalWeb.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    [DynamicAuthorizeAttribute]
    public class AccountController : ControllerBase
    {

        UnitOfWork unitOfWork = new UnitOfWork();
        //
        // GET: /Account/Login
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login(string url)
        {
            Session["ReturnUrl"] = url;
            ViewBag.ReturnUrl = url;
            return base.PartialOrFullView();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid && WebSecurity.Login(model.Email, model.Password, persistCookie: model.RememberMe))
            {
                long userID = unitOfWork.UserManagment.getUserIDByEmail(model.Email).Value;
                CreateAuthenticationTicket(userID);

                returnUrl = (string) Session["ReturnUrl"];
                if (!string.IsNullOrEmpty(returnUrl))
                    return Redirect(returnUrl);
                else
                    return RedirectToAction("..//Home//Index");
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError(string.Empty, "Неверный email пользователя или пароль");

 //           System.Web.Mvc.Html.RenderPartialExtensions.RenderPartial
            return PartialOrFullView("Login");
        }

        //
        // POST: /Account/LogOff

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff(string url)
        {
            try
            {
                WebSecurity.Logout();

                // more complicated case when for login
                // if it is not acceptable for unathentificated user - go to home page
                string controllerName = "", actionName = "";
                StringProcessing.getUrlParts(url, out controllerName, out actionName);
                if (!string.IsNullOrWhiteSpace(controllerName))
                    actionName = "index";

                bool toLoginPage;
                bool permissionCheck = false;
                if (!string.IsNullOrWhiteSpace(controllerName) && !string.IsNullOrWhiteSpace(actionName))
                    permissionCheck = unitOfWork.UserManagment.getUserRight(controllerName, actionName, eUserRoles.withoutauth.ToString(), out toLoginPage);
                if (!string.IsNullOrEmpty(url) && permissionCheck)
                    return Redirect(url);
                else
                    return RedirectToAction("..//Home//Index");
            } catch(Exception exc)
            {
                PascalLogging.WriteErrorMessage("AccountController", "getUrlParts", exc.Message);
                return RedirectToAction("..//Home//Index");
            }
        }


        //
        // POST: /Account/ExternalLogin

        //[HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            return new ExternalLoginResult(provider, Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
        }

        
        // GET: /Account/ExternalLoginCallback

        [AllowAnonymous]
        public ActionResult ExternalLoginCallback(string returnUrl)
        {
            AuthenticationResult authenticationResult = OAuthWebSecurity.VerifyAuthentication(Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
            if (!authenticationResult.IsSuccessful)
            {
                return RedirectToAction("ExternalLoginFailure");
            }

            if (externalAuthentification(authenticationResult))
                return RedirectToLocal(returnUrl);
            else
            {
                // facebook
                var id = authenticationResult.ExtraData["id"];
                //var name = authenticationResult.ExtraData["name"];
                var link = authenticationResult.ExtraData["link"];
                var gender = authenticationResult.ExtraData["gender"];
                var accesstoken = authenticationResult.ExtraData["accesstoken"];

                
                UserAuth newUserAuth = new UserAuth();
                UserExternal newUserExternal = new UserExternal();

                Image smallLogo = null;

                if (authenticationResult.Provider.Equals("facebook"))
                {
                    // get more info from facebook
                    getDataFromFacebook(accesstoken, ref newUserAuth, ref newUserExternal, out smallLogo);
                }
                else if (authenticationResult.Provider.Equals("vkontakte"))
                {
                    getDataFromVK(authenticationResult.ExtraData, ref newUserAuth, ref newUserExternal, out smallLogo);
                }

                getDefaultUserAuth(unitOfWork, newUserAuth);
                //newUserAuth.Name = name;

                string smallImageAbsoluteUri = null;
                bool res = GeneralImageStorage.saveUserImages(smallLogo, DateTime.Now, out smallImageAbsoluteUri);
                newUserAuth.SmallLogoPath = smallImageAbsoluteUri;

                unitOfWork.UserAuthRepository.Insert(newUserAuth);
                unitOfWork.getContext().Configuration.ValidateOnSaveEnabled = false;
                unitOfWork.Save();
                unitOfWork.getContext().Configuration.ValidateOnSaveEnabled = true;

 
                newUserExternal.Identifier = id;
                newUserExternal.Token = accesstoken;
                newUserExternal.UserAuthID = newUserAuth.ID;
                newUserExternal.AccountLink = link;
                newUserExternal.WebsiteTypeID = unitOfWork.WebsiteTypeRepository.Get(filter: obj => obj.Name == authenticationResult.Provider).Select(s => s.ID).FirstOrDefault();

                
                unitOfWork.UserExternalRepository.Insert(newUserExternal);
                unitOfWork.Save();

                if (externalAuthentification(authenticationResult))
                    return RedirectToLocal(returnUrl);
            }
            return null;

        }


        private static bool getDataFromVK(IDictionary<string, string> extraData, ref UserAuth newUserAuth, ref UserExternal newUserExternal, out Image smallLogo)
        {
            string name = checkAndCutStringLength(extraData["first_name"], 50);
            string last_name = checkAndCutStringLength(extraData["last_name"], 50);
            string email = null; //checkAndCutStringLength(extraData[], 50);

            string id = extraData["id"];
            string country = extraData.ContainsKey("country") ? checkAndCutStringLength(extraData["country"], 100) : null;
            string hometown = extraData.ContainsKey("city") ? checkAndCutStringLength(extraData["city"], 100) : null;
            string birthday = checkAndCutStringLength(extraData["birthday"], 30);
            string about = null;     //checkAndCutStringLength(extraData[], 200);
            string education = null; //checkAndCutStringLength(extraData[], 100);
            string locale = null;    //checkAndCutStringLength(extraData[], 10);
            string gender = checkAndCutStringLength(extraData["gender"], 10);

            smallLogo = downloadImage(extraData["picture"]);

            // fill the fields
            newUserAuth.Name = name;
            newUserAuth.Surname = last_name;
            newUserAuth.Email = email;

            newUserExternal.Country = country;
            newUserExternal.Hometown = hometown;
            newUserExternal.Birthday = birthday;
            newUserExternal.About = about;
            newUserExternal.Education = education;
            newUserExternal.Locale = locale;
            newUserExternal.Gender = gender;

            return true;
        }

        private static bool getDataFromFacebook(string accesstoken, ref UserAuth newUserAuth, ref UserExternal newUserExternal, out Image smallLogo)
        {
            try
            {
                var client = new Facebook.FacebookClient(accesstoken);
                dynamic resultData = client.Get("me", new { fields = "id, first_name, last_name, about, birthday, education, email, hometown, locale, gender, picture" });
                string name = checkAndCutStringLength(resultData.first_name, 50);
                string last_name = checkAndCutStringLength(resultData.last_name, 50);
                string email = checkAndCutStringLength(resultData.email, 50);

                string id = resultData.id;
                string hometown = checkAndCutStringLength(resultData.hometown, 100);
                string birthday = checkAndCutStringLength(resultData.birthday, 30);
                string about = checkAndCutStringLength(resultData.about, 200);
                string education = checkAndCutStringLength(resultData.education, 100);
                string locale = checkAndCutStringLength(resultData.locale, 10);
                string gender = checkAndCutStringLength(resultData.gender, 10);

                // fill the fields
                newUserAuth.Name = name;
                newUserAuth.Surname = last_name;
                newUserAuth.Email = email;

                newUserExternal.Hometown = hometown;
                newUserExternal.Birthday = birthday;
                newUserExternal.About = about;
                newUserExternal.Education = education;
                newUserExternal.Locale = locale;
                newUserExternal.Gender = gender;

                var pirctureURL = resultData.picture.data.url;
                smallLogo = downloadImage(pirctureURL);
                return true;
            }
            catch (Facebook.FacebookOAuthException exc)
            {
                smallLogo = null;
                return false;
            }
        }

        /// <summary>
        /// download an image using its url
        /// </summary>
        /// <param name="pirctureURL"></param>
        /// <returns></returns>
        private static Image downloadImage(string pirctureURL)
        {
            if (string.IsNullOrEmpty(pirctureURL))
            {
                return null;
            }
            Image smallLogo;
            var request = WebRequest.Create(pirctureURL);

            using (var response = request.GetResponse())
            using (var stream = response.GetResponseStream())
            {
                Image image = Bitmap.FromStream(stream);
                smallLogo = image;
            }
            return smallLogo;
        }

        public static string checkAndCutStringLength(string originalString, int lengthMaximum)
        {
            if (originalString == null)
                return originalString;
            else
            {
                if (originalString.Length > lengthMaximum)
                    return originalString.Substring(0, lengthMaximum);
                else
                    return originalString;
            }
        }

        private bool externalAuthentification(AuthenticationResult result)
        {
            var userExt = unitOfWork.UserManagment.checkUserExternal(result.Provider, result.ProviderUserId);
            if (userExt != null)
            {
                var uniqueID = result.Provider + "/" + result.ProviderUserId;
                FormsAuthentication.SetAuthCookie(uniqueID, false);
                CreateAuthenticationTicket(userExt.UserAuthID);
                return true;
            }
            else
                return false;
        }

        static void getDefaultUserAuth(UnitOfWork _unitOfWork, UserAuth newUserAuth)
        {
            newUserAuth.CreationDate = DateTime.Now;
            newUserAuth.IsActivated = true;
            newUserAuth.IsLocked = false;

            newUserAuth.UserRoleID = _unitOfWork.UserManagment.getUserRoleIDByName(eUserRoles.generaluser.ToString()).Value;
            newUserAuth.IsEmailConfirmed = false;

            return;
        }

        
        // POST: /Account/ExternalLoginConfirmation

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLoginConfirmation(RegisterExternalLoginModel model, string returnUrl)
        {
            string provider = null;
            string providerUserId = null;

            if (User.Identity.IsAuthenticated || !OAuthWebSecurity.TryDeserializeProviderUserId(model.ExternalLoginData, out provider, out providerUserId))
            {
                return RedirectToAction("Manage");
            }

            if (ModelState.IsValid)
            {
                // Insert a new user into the database
                using (UsersContext db = new UsersContext())
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == model.UserName.ToLower());
                    // Check if user already exists
                    if (user == null)
                    {
                        // Insert name into the profile table
                        db.UserProfiles.Add(new UserProfile { UserName = model.UserName });
                        db.SaveChanges();

                        OAuthWebSecurity.CreateOrUpdateAccount(provider, providerUserId, model.UserName);
                        OAuthWebSecurity.Login(provider, providerUserId, createPersistentCookie: false);
                       // CreateAuthenticationTicket(model.UserName);
                        return RedirectToLocal(returnUrl);
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "User name already exists. Please enter a different user name.");
                    }
                }
            }

            ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(provider).DisplayName;
            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        
        // GET: /Account/ExternalLoginFailure

        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult ExternalLoginsList(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return PartialView("_ExternalLoginsListPartial", OAuthWebSecurity.RegisteredClientData);
        }

        [ChildActionOnly]
        public ActionResult RemoveExternalLogins()
        {
            ICollection<OAuthAccount> accounts = OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name);
            List<ExternalLogin> externalLogins = new List<ExternalLogin>();
            foreach (OAuthAccount account in accounts)
            {
                AuthenticationClientData clientData = OAuthWebSecurity.GetOAuthClientData(account.Provider);

                externalLogins.Add(new ExternalLogin
                {
                    Provider = account.Provider,
                    ProviderDisplayName = clientData.DisplayName,
                    ProviderUserId = account.ProviderUserId,
                });
            }

            ViewBag.ShowRemoveButton = externalLogins.Count > 1 || OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            return PartialView("_RemoveExternalLoginsPartial", externalLogins);
        }




        // http://www.codeproject.com/Tips/574576/How-to-implement-a-custom-IPrincipal-in-ASP-NET-MV

        void CreateAuthenticationTicket(long id)
        {
            var authUser = unitOfWork.UserManagment.getUserByID(id);
       //     CustomPrincipalSerializedModel serializeModel = new CustomPrincipalSerializedModel();

            CustomPrincipalSerializedModel serializeModel = new CustomPrincipalSerializedModel();
            serializeModel.UserID = authUser.ID;
            //serializeModel.Login = authUser.Login;
            serializeModel.RoleName = authUser.UserRole.Name;

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string userData = serializer.Serialize(serializeModel);

            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                1, authUser.Name, DateTime.Now, DateTime.Now.AddHours(336), false, userData);
            string encTicket = FormsAuthentication.Encrypt(authTicket);
            HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
            Response.Cookies.Add(faCookie);
        }

        /// <summary>
        /// First step of registration
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult RegStepOne()
        {
            //RegistrationStepOneModel model = new RegistrationStepOneModel();

            RegistrationStepOneModel model = (RegistrationStepOneModel)Session["REG_STEP1"];
            if (model == null)
                model = new RegistrationStepOneModel();
           
            getCaptchaImageString(model);
            return PartialOrFullView(model: model);
        }

        /// <summary>
        /// Second step of registration
        /// </summary>
        /// <param name="step1"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public ActionResult RegStepTwo(RegistrationStepOneModel step1, HttpPostedFileBase file)
        {
            if (file != null)
            {
                var imageBytes = cFileWork.ConvertToBytes(file);
                step1.Image = imageBytes;     
                step1.ImageType = string.Copy(file.ContentType);
            }

            Session["REG_STEP1"] = step1;

            if (!ModelState.IsValid)
                return PartialOrFullView("RegStepOne", model: step1);

            var checkUserID = unitOfWork.UserManagment.getUserIDByEmail(step1.Email);
            if (checkUserID.HasValue)
            {
                ModelState.AddModelError(string.Empty, "Введенный Вами e-mail уже используется в системе");
               // return PartialOrFКullView("RegStepOne", model: step1);
                return PartialOrFullView("RegStepOne", model: step1);
            }

            if (step1.CaptchaObject.CaptchaEnterCode != (string)Session[CaptchaImage.CaptchaValueKey])
            {
                ModelState.AddModelError(string.Empty, "Проверочный код введен неверно");

                getCaptchaImageString(step1);
                return PartialOrFullView("RegStepOne", model: step1);
            }

            if (ModelState.IsValid)
            {

            }

            Session["REG_STEP1"] = step1;
            var model = new WizardRegistrationModel
            {
                Step1 = step1
            };

            // for dropdownList
            getMartialStatusList();
            getEducationStatusList();
            getUserOccupationList();
            getUserGenderList();


            return PartialOrFullView(model: model);
        }

        /// <summary>
        /// Finishing of registration
        /// </summary>
        /// <param name="wizard"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult FinishRegistration(WizardRegistrationModel wizard)
        {
            RegistrationStepOneModel step1 = (RegistrationStepOneModel)Session["REG_STEP1"];
            wizard.Step1 = step1;
            wizard.Step1.UserRoleID = unitOfWork.UserManagment.getUserRoleIDByName(eUserRoles.generaluser.ToString()).Value;
            wizard.Step1.IsActivated = true;
            wizard.Step1.IsLocked = false;

            // check login
            var checkUserID = unitOfWork.UserManagment.getUserIDByEmail(wizard.Step1.Email);
            if (checkUserID.HasValue)
            {
                ModelState.AddModelError(string.Empty, "Введенный Вами e-mail уже используется в системе");
                return PartialOrFullView("RegStepOne", model: wizard.Step1); 
            }

            long? userID = createUser(wizard);
            if (userID.HasValue)
            {
                string urlBase = Request.Url.GetLeftPart(UriPartial.Authority);
                EmailSending.putMessageToDB(unitOfWork, userID.Value, eEmailDispatchType.email_confirmation, urlBase);
                //EmailSending.sendConfirmationEmail(userAuth);
            }
            return RedirectToAction("Index", "Profile");
        }

        /// <summary>
        /// Show agreement
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Agreement()
        {
            return View();
        }

        private void getCaptchaImageString(RegistrationStepOneModel model)
        {
            Session[CaptchaImage.CaptchaValueKey] = new Random(DateTime.Now.Millisecond).Next(1111, 9999).ToString();
            var ci = new CaptchaImage(Session[CaptchaImage.CaptchaValueKey].ToString(), 100, 30, "Arial");
            string captchaImageString = GeneralMethods.getImageSourceFromBytes(ImageProcessing.ImageToByte(ci.Image), "image/jpeg");
            model.CaptchaObject.CaptchaImageString = captchaImageString;
        }

      

        private long? createUser(WizardRegistrationModel wizard)
        {
            var userIDstr = WebSecurity.CreateUserAndAccount(wizard.Step1.Email, wizard.Step1.PasswordObject.Password);
            if (userIDstr != null)
            {
                long userID = Convert.ToInt64(userIDstr);

                unitOfWork.UserManagment.updateUser(userID, wizard);

                WebSecurity.Login(userID.ToString(), wizard.Step1.PasswordObject.Password);
                CreateAuthenticationTicket(userID);
                return userID;
            }
            else
            {
                return null;
            }
        }

        [HttpGet]
        public ActionResult Edit(long id)
        {
            // for dropdownList
            getMartialStatusList();
            getEducationStatusList();
            getUserOccupationList();
            getUserGenderList();
            getUserRoleList();

            UserAuth userAuth = unitOfWork.UserAuthRepository.GetByID(id);
            UserInfo userInfo = unitOfWork.UserInfoRepository.GetByID(id);
            if (userInfo == null)
                userInfo = new UserInfo();

           // UserUnitedModel model = new UserUnitedModel(userAuth, userInfo);

            WizardRegistrationModel model = new WizardRegistrationModel();
            model.Step1.Email = userAuth.Email;
            model.Step1.ID = userAuth.ID;
            // image is missed
            model.Step1.IsActivated = userAuth.IsActivated;
            model.Step1.IsLocked = userAuth.IsLocked;
            //model.Step1.Login = userAuth.Login;
            model.Step1.SmallLogoPath = userAuth.SmallLogoPath;
            model.Step1.UserName = userAuth.Name;
            model.Step1.UserRoleID = userAuth.UserRoleID;
            model.Step1.UserSurname = userAuth.Surname;

            model.Step1.Birthdate = userInfo.Birthdate;
            model.Step2.EducationStatusID = userInfo.EducationStatusID;
            if (userInfo.GenderIsMale.HasValue)
                model.Step2.GenderIndex = userInfo.GenderIsMale.Value ? 2 : 1;
            else
                model.Step2.GenderIndex = null;
            model.Step2.MaritalStatusID = userInfo.MaritalStatusID;
            model.Step2.UserInterests = userInfo.Info;
            model.Step2.UserOccupationID = userInfo.UserOccuptaionID;

            return PartialOrFullView(model: model);
        }

        [HttpPost]
        public ActionResult Edit(WizardRegistrationModel model, HttpPostedFileBase file)
        {
            ModelState.Clear();
            if (TryUpdateModel(model))
            {
                // Attempt to register the user
                try
                {
                    if (file != null)
                    {
                        var imageBytes = cFileWork.ConvertToBytes(file);
                        model.Step1.Image = imageBytes;
                        model.Step1.ImageType = file.ContentType;
                    }
                    unitOfWork.UserManagment.updateUser(model.Step1.ID.Value, model);
                    return RedirectToAction("Index", "Profile");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError(string.Empty, ErrorCodeToString(e.StatusCode));

                }
            }

            // for dropdownList
            getMartialStatusList();
            getEducationStatusList();
            getUserOccupationList();
            getUserGenderList();
            getUserRoleList();

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        /// <summary>
        /// open delete window
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(int id = 0)
        {
            UserAuth user = unitOfWork.UserAuthRepository.GetByID(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            var model = new DeleteDialogModel(id, user.Name, "пользователя", new ManageViewCommonModel("Account", "Delete"));
            return PartialView("..//Shared//CommonViews//DeleteDialog", model);
        }

        /// <summary>
        /// delete user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int? id)
        {
            if (id.HasValue)
            {
                unitOfWork.UserManagment.DeleteUser(id.Value);
                return Json(new { success = true, message = "success" });
            }
            else
                return Json(new { success = false, message = "problems" });
        }

        /// <summary>
        /// Show agreement
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Verify(string key)
        {
            bool result = false;
            var userAuth = unitOfWork.UserAuthRepository.Get(filter : obj => obj.EmailConfirmationKey == key).SingleOrDefault();
            if (userAuth != null)
            {
                userAuth.IsEmailConfirmed = true;
                userAuth.EmailConfirmationKey = null;
                unitOfWork.UserAuthRepository.Update(userAuth);
                unitOfWork.Save();
                result = true;
            }
            if(result)
                return RedirectToAction("Message", "Home", new { message = "Email подтвержден" });
            else
                return RedirectToAction("Message", "Home", new { message = "Email не подтвержден" });
        }


        /// <summary>
        /// Open page for restoring password
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Restore()
        {
            return PartialOrFullView("Restore");
        }

        /// <summary>
        /// Open page for restoring password
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult SendRestoreMessage(RestorePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                // check login
                var userAuth = unitOfWork.UserManagment.getUserByEmail(model.Email);
                if (userAuth == null)
                {
                    ModelState.AddModelError(string.Empty, "Данного email не существует");
                    return PartialOrFullView("Restore", model);
                }
                else
                {
                    userAuth.RestorePasswordKey = cPassword.GenerateKey();
                    unitOfWork.Save();
                    string urlBase = Request.Url.GetLeftPart(UriPartial.Authority);
                    EmailSending.putMessageToDB(unitOfWork, userAuth.ID, eEmailDispatchType.restore_password, urlBase);
                    //EmailSending.sendRestorePassword(userAuth);
                }
                return RedirectToAction("Message", "Home", new { message = "Сообщение с инструкцией по восстановлению пароля на указанный email будет отправлено в ближайшие минуты" });
                          
            } 
            return PartialOrFullView("Restore", model: model);
        }


        /// <summary>
        /// Open page for changing
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult ChangePassword()
        {
            var userID = getCurrentUserID();
            UserAuth userAuth = unitOfWork.UserAuthRepository.GetByID(userID);
            if (userID.HasValue && userAuth != null)
            {
                ChangePasswordModel model = new ChangePasswordModel();
                model.UserID = userAuth.ID;
                model.Email = userAuth.Email;
                return PartialOrFullView("ChangePassword", model: model);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Open page for restoring password
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult RestorePassword(string key)
        {
            var userAuth = unitOfWork.UserAuthRepository.Get(filter: obj => obj.RestorePasswordKey == key).SingleOrDefault();
            if (userAuth != null)
            {
                userAuth.RestorePasswordKey = null;
                unitOfWork.UserAuthRepository.Update(userAuth);
                unitOfWork.Save();

                ChangePasswordModel model = new ChangePasswordModel();
                model.UserID = userAuth.ID;
                model.Email = userAuth.Email;
                return PartialOrFullView("ChangePassword", model: model);
            }
            else
                return RedirectToAction("Message", "Home", new { message = "Ссылка некорректна" });
        }

        /// <summary>
        /// Open page for restoring password
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult UpdatePassword(ChangePasswordModel model)
        {
            if (!ModelState.IsValid)
                return PartialOrFullView("ChangePassword");

            var userAuth = unitOfWork.UserAuthRepository.GetByID(model.UserID);
            if (userAuth != null)
            {
                userAuth.PasswordSalt = cPassword.CreateSalt();
                userAuth.PasswordHash = cPassword.CreatePasswordHash(model.PasswordObject.Password, userAuth.PasswordSalt);
                unitOfWork.UserAuthRepository.Update(userAuth);
                unitOfWork.Save();

                if (getCurrentUserID() == null)
                    return RedirectToAction("Login", "Account");
                else
                    return RedirectToAction("Index", "Profile");
            }
            else
                return RedirectToAction("Message", "Home", new { message = "Не удалось сменить пароль" });
        }


        #region Lists
        private void getUserRoleList()
        {
            var userRoleList = unitOfWork.UserRoleRepository.Get(
                orderBy: q => q.OrderBy(x => x.Alias)).Select(obj => new
                {
                    ID = obj.ID,
                    Value = obj.Alias
                }).ToList();

            ViewBag.UserRoles = new SelectList(userRoleList, "ID", "Value");
        }

        private void getMartialStatusList()
        {
            var martialStatusList = unitOfWork.MaritalStatusRepository.Get(
                orderBy: q => q.OrderBy(x => x.Name)).Select(obj => new
                {
                    ID = obj.ID,
                    Value = obj.Alias
                }).ToList();

            ViewBag.MaritalStatuses = new SelectList(martialStatusList, "ID", "Value");
        }

        private void getEducationStatusList()
        {
            var educationStatusList = unitOfWork.EducationStatusRepository.Get(
                orderBy: q => q.OrderBy(x => x.Name)).Select(obj => new
                {
                    ID = obj.ID,
                    Value = obj.Alias
                }).ToList();

            ViewBag.EducationStatuses = new SelectList(educationStatusList, "ID", "Value");
        }

        private void getUserOccupationList()
        {
            var userOccupationList = unitOfWork.UserOccupationRepository.Get(
                orderBy: q => q.OrderBy(x => x.Name)).Select(obj => new
                {
                    ID = obj.ID,
                    Value = obj.Alias
                }).ToList();

            ViewBag.UserOccupations = new SelectList(userOccupationList, "ID", "Value");
        }

        private void getUserGenderList()
        {
            var userGenderList = new List<object>();
            userGenderList.Add(new { ID = 1, Value = "Женский" });
            userGenderList.Add(new { ID = 2, Value = "Мужской" });

            ViewBag.UserGenders = new SelectList(userGenderList, "ID", "Value");
        }

        private void getCountryList()
        {
            var countryList = unitOfWork.CountryRepository.Get(
                orderBy: q => q.OrderBy(x => x.Name)).Select(obj => new
                {
                    ID = obj.ID,
                    Value = obj.Name
                }).ToList();

            ViewBag.Countries = new SelectList(countryList, "ID", "Value");
        }

        #endregion

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
