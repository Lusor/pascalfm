﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PascalWeb.DB;
using PascalWeb.DB.PageModels;
using PascalWeb.Additional.Auth;
using PascalWeb.Additional;
using PascalWeb.DB.Repositories;
using PascalWeb.Additional.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using PascalWeb.Additional.Algorithms;

namespace PascalWeb.Controllers
{
    [DynamicAuthorizeAttribute]
    public class AudioPlayerController : ControllerBase
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        // http://stackoverflow.com/questions/23807535/azure-download-blob-filestream-memorystream

        /// <summary>
        /// 
        /// </summary>
        /// <param name="programEditionID"></param>
        /// <returns></returns>
        public ActionResult  Download(long programEditionID)
        {
            try
            {
                //cStorageManager.setSpecialSettings();
                var programEdition = unitOfWork.ProgramEditionRepository.GetOneItem(obj => obj.ID == programEditionID, "Content");
                if (programEdition != null && programEdition.Content != null)
                {
                    string editionTitle = Transliteration.translite(programEdition.EditionTittle);
                    editionTitle = editionTitle.Replace(" ", "_");
                    string downloadLink = cStorageManager.getDownloadLink(programEdition.Content.AbsoluteFileUri, editionTitle);
                    ProgramEditionManagment.addToProgramEditionCounters(unitOfWork, Request, programEditionID, null, null, null, true);
                    return Redirect(downloadLink);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception exc)
            {
                PascalLogging.WriteErrorMessage("AudioPlayerController", "Download", exc.Message);
                return null;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="programEditionID"></param>
        /// <returns></returns>
        public string Listen(long id)
        {
            try
            {
                //cStorageManager.setSpecialSettings();
                var programEdition = unitOfWork.ProgramEditionRepository.GetOneItem(obj => obj.ID == id, "Content");
                if (programEdition != null && programEdition.Content != null)
                {
                    return programEdition.Content.AbsoluteFileUri;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception exc)
            {
                PascalLogging.WriteErrorMessage("AudioPlayerController", "Listen", exc.Message);
                return "";
            }
        }
    }
}