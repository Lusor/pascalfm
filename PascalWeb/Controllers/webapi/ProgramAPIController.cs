﻿using PascalWeb.Additional;
using PascalWeb.DB;
using PascalWeb.DB.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PascalWeb.Controllers
{
    // how to add web api to the existing mvc project
    // http://stackoverflow.com/questions/11990036/how-to-add-web-api-to-an-existing-asp-net-mvc-4-web-application-project

    public class ProgramAPIController : ApiController
    {
        UnitOfWork unitOfWork = new UnitOfWork();

        // GET api/<controller>   : url to use => api/vs
        [HttpGet]
        public string Get()
        {
            return "Hi from web api controller";
        }

        // GET api/<controller>/5   : url to use => api/vs/5
        [HttpGet]
        public string Get(int id)
        {
            return (id + 1).ToString();
        }

        [HttpGet]
        [ActionName("getsometing")]
        public int getsometing(int id)
        {
            var result = id + 1;
            return result;
        }

        [HttpGet]
        [ActionName("getprogramlist")]
        public List<JsonProgram> getProgramList()
        {
            var ProgramDList = unitOfWork.ProgramRepository.Get(orderBy: q => q.OrderBy(x => x.Name)).ToList();

            List<JsonProgram> programJSONList = new List<JsonProgram>();
            for (int i = 0; i < ProgramDList.Count; i++)
            {
                JsonProgram jsonProgram = new JsonProgram();
                jsonProgram.id = ProgramDList[i].ID;
                jsonProgram.programName = ProgramDList[i].Name;
                jsonProgram.description = ProgramDList[i].Description;

               // GeneralMethods.ImageToBase64
                programJSONList.Add(jsonProgram);
            }
            return programJSONList;
        }

        public class JsonProgram
        {
            public int id;
            public string programName;
            public string description;
            public string logo;
        }

    }
}
