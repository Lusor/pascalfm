﻿using PascalWeb.Additional.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PascalWeb.Controllers
{
    [DynamicAuthorizeAttribute]
    public abstract class ControllerBase : Controller
    {
        /*
        protected ViewResultBase PartialOrFullView(string name = null, object model = null)
        {
            if (this.Request.IsAjaxRequest())
                return PartialView(name, model);
            else
                return View(name, model);
        }*/

        //protected override void Execute(RequestContext requestContext)
        //{
        //    // Если браузер запрашивает метод с именем Ajax{Something}, значит он ожидает получить PartialView
        //    //  однако, если при этом не задан заголовок X-Requested-With, значит пользователь попытался отобразить ссылку в новом окне/вкладке
        //    //  и, следовательно, его нужно перенаправить на полную страницу.
        //    Boolean isAjaxRequest = requestContext.HttpContext.Request.QueryString["X-Requested-With"] != null;
        //    Boolean urlRequestPartialView = requestContext.HttpContext.Request.RawUrl.ToLower().Contains("ajax");
        //    if ((urlRequestPartialView) && (!isAjaxRequest))
        //    {
        //        String newUrl = requestContext.HttpContext.Request.RawUrl.ToLower().Replace("ajax", "");
        //        requestContext.HttpContext.Response.Redirect(newUrl);
        //    }
        //    base.Execute(requestContext);
        //}

        //protected ViewResultBase PartialOrFullView()
        //{
        //    if (this.Request.IsAjaxRequest())
        //        return PartialView(null, null);
        //    else
        //        return View(null, null);
        //}

        //protected ViewResultBase PartialOrFullView(string name = null)
        //{
        //    if (this.Request.IsAjaxRequest())
        //        return PartialView(viewName: name);
        //    else
        //        return View(viewName: name);
        //}

        protected ViewResultBase PartialOrFullView(string name = null, object model = null)
        {
            if (this.Request.IsAjaxRequest())
               return PartialView(name, model);
            else
                return View(name, model);
        }

        /// <summary>
        /// get current info from context
        /// </summary>
        /// <returns></returns>
        protected long? getCurrentUserID()
        {
            var user = HttpContext.User as cPascalPrincipal;
            long? userID = 0;
            if (user == null)
            {
                return null;
            }
            else
                userID = user.UserID;

            return userID;
        }

        /// <summary>
        /// get roleName of user from context
        /// </summary>
        /// <returns></returns>
        protected string getRoleName()
        {
            var user = HttpContext.User as cPascalPrincipal;
            string roleName = null;
            if (user == null)
            {
                return null;
            }
            else
                roleName = user.RoleName;

            return roleName;
        }


    }
}