﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AjaxFileUploadDemo;
using PascalWeb.Additional.Auth;
using PascalWeb.Additional.Storage;
using PascalWeb.DB;
using PascalWeb.DB.PageModels;
using PascalWeb.DB.Repositories;
using PascalWeb.DB.AuxiliaryModels;
using PascalWeb.Additional;
using System.Drawing;
using PascalWeb.Additional.ImageProcessing;
using PascalWeb.DB.Enumerators;

namespace PascalWeb.Controllers
{
    [DynamicAuthorizeAttribute]
    public class BlogRecordController : ControllerBase
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        public ActionResult CreateBlog(int? id = null)
        {
            Blog model = new Blog();
            if (id.HasValue)
                model = unitOfWork.BlogRepository.GetByID(id.Value);
            return base.PartialOrFullView(model: model);
        }

        /// <summary>
        /// to create or to edit blog
        /// </summary>
        /// <param name="blogrecord"></param>
        /// <returns></returns>
        public ActionResult CreateOrEditBlog(Blog blog, HttpPostedFileBase file)
        {
            bool newRecord = (blog.ID == 0);
            string previousSmallImageAbsoluteUri = null, previousMediumImageAbsoluteUri = null;
            // if record do not exist
            if (!newRecord)
            {
                // save paths for possible deletion
                Blog previousBlog = unitOfWork.BlogRepository.GetByID(blog.ID);
                previousSmallImageAbsoluteUri = previousBlog.SmallLogoPath;
                previousMediumImageAbsoluteUri = previousBlog.MediumLogoPath;
                // detach record from context (because of presence of two entity with the same key)
                unitOfWork.getContext().Entry(previousBlog).State = EntityState.Detached;
            }

            if (ModelState.IsValid)
            {
                if (newRecord)
                {
                    blog.AuthorID = getCurrentUserID().Value;
                    blog.CreationDate = DateTime.Now;

                    ModelState.Clear();
                    bool val = TryValidateModel(blog);
                }
                else
                    unitOfWork.getContext().Entry(blog).State = EntityState.Modified;

                // save logo images
                if (file != null)
                {
                    byte[] imageBytes = cFileWork.ConvertToBytes(file);

                    Image smallImage = null, mediumImage = null;
                    cFileWork.convertIconImage(imageBytes, out smallImage, out mediumImage);

                    var smallImageBytes = ImageProcessing.ImageToByte(smallImage);
                    //programModel.Program.LogoContentType = file.ContentType;

                    string smallImageAbsoluteUri = null;
                    bool res = GeneralImageStorage.saveBlogLogos(smallImage, DateTime.Now, out smallImageAbsoluteUri);
                    string mediumImageAbsoluteUri = null;
                    res = GeneralImageStorage.saveBlogLogos(mediumImage, DateTime.Now, out mediumImageAbsoluteUri);

                    blog.SmallLogoPath = smallImageAbsoluteUri;
                    blog.MediumLogoPath = mediumImageAbsoluteUri;
                }
                else
                {
                    // объект должен быть чтобы его менять
                    if (blog.ID > 0)
                    {
                        // for two additional images
                        unitOfWork.getContext().Entry(blog).Property(obj => obj.SmallLogoPath).IsModified = false;
                        unitOfWork.getContext().Entry(blog).Property(obj => obj.MediumLogoPath).IsModified = false;
                    }
                    // it is unnecessary to delete previous images
                    previousSmallImageAbsoluteUri = null;
                    previousMediumImageAbsoluteUri = null;
                }


                if (newRecord)
                    unitOfWork.BlogRepository.Insert(blog);
                unitOfWork.Save();

                // delete previous images
                GeneralImageStorage.deleteFile(previousSmallImageAbsoluteUri);
                GeneralImageStorage.deleteFile(previousMediumImageAbsoluteUri);

                return RedirectToAction("AdminBlogView", "Admin", new { blogID = blog .ID});
            }

            return base.PartialOrFullView(model: blog);
        }

        public ActionResult Index(int blogID = 0, int paginationNumber = 0)
        {
            const int itemsOnPage = ConstantSettings.BlogRecordsOnPage;
            var model = new AdminBlogPageModel();


            var themeList = HomeController.getThemes(unitOfWork, false, true);
            var themeWithBlogsList = new List<ListElementWithChildren<Blog>>();

            for (int i = 0; i < themeList.Count(); i++)
            {
                var themeWithPrograms = new ListElementWithChildren<Blog>(themeList.ElementAt(i).Name, themeList.ElementAt(i).Blog);
                themeWithBlogsList.Add(themeWithPrograms);
            }

            model.ThemeList = themeWithBlogsList;
            model.PopularAuthorList = HomeController.getAuthorList(unitOfWork, false, true);

            // ??
            model.BlogRankingList = getBlogRanking(true);


            if (blogID > 0)  // show blog, if particular blog was chosen
            {
                model.Blog = unitOfWork.BlogRepository.GetByID(blogID);

                // pagination for table

                // get full number of items
                long fullItemsNumber = unitOfWork.BlogRecordRepository.Count(filter: obj => obj.BlogID == blogID);

                // get blog records
                var blogRecords = unitOfWork.BlogRecordRepository.Get(filter: obj => obj.BlogID == blogID && obj.Published == true, orderBy: q => q.OrderByDescending(x => x.CreationDate),
                    //             includeProperties: "BlogRecord.CommentBlogRecord", 
                    skip: paginationNumber * itemsOnPage, take: itemsOnPage);

                // form pagination model
                int numberOfPages = (int)Math.Ceiling((double)fullItemsNumber / (double)itemsOnPage);
                PaginationModel paginationModel = new PaginationModel(numberOfPages, paginationNumber, new ManageViewCommonModel("BlogRecord", "Index", new System.Web.Routing.RouteValueDictionary(){{"blogID", blogID}}));
                var tableModel = new PageWithPagination<IEnumerable<BlogRecord>>(blogRecords, paginationModel);
                model.TableData = tableModel;
                // for scubscription button
                long? userID = getCurrentUserID();
                model.UserIsSubscribed = SubscriptionController.checkBlogSuscription(unitOfWork, userID, blogID);
            }
            else // it is for last records
            {
                // show latest records
                model.Blog = null;

                // get full number of items
                long fullItemsNumber = unitOfWork.BlogRecordRepository.Count(filter: obj => obj.Published == true);

                if (fullItemsNumber > 100)
                    fullItemsNumber = 100;

                // get blog records
                var blogRecords = unitOfWork.BlogRecordRepository.Get(filter: obj => obj.Published == true, orderBy: q => q.OrderByDescending(x => x.CreationDate),
                    skip: paginationNumber * itemsOnPage, take: itemsOnPage);

                // form pagination model
                int numberOfPages = (int)Math.Ceiling((double)fullItemsNumber / (double)itemsOnPage);
                PaginationModel paginationModel = new PaginationModel(numberOfPages, paginationNumber, new ManageViewCommonModel("BlogRecord", "Index"));
                var tableModel = new PageWithPagination<IEnumerable<BlogRecord>>(blogRecords, paginationModel);
                model.TableData = tableModel;

            }

            return PartialOrFullView(model: model);
        }

        IEnumerable<Blog> getBlogRanking(bool forAllTime)
        {
            var blogEnum = unitOfWork.BlogRepository.Get(orderBy: q => q.OrderByDescending(x => x.BlogRecords.Sum(s => s.BlogID))).ToList();
            return blogEnum;
        }


        public ActionResult News(int paginationNumber = 0)
        {
            ViewResultBase result = (ViewResultBase)Index(1, paginationNumber);
            ((AdminBlogPageModel)result.Model).TableData.PaginationModel.ManageViewCommonModel.ActionName = "News";
            return result;
        }

        //
        // GET: /BlogRecord/Details/5
        public ActionResult Details(long id = 0)
        {
            Session["BlogRecordID"] = id;

            BlogRecord blogrecord = unitOfWork.BlogRecordRepository.GetOneItem(obj => obj.ID == id, "Blog");
            if (blogrecord == null)
            {
                return HttpNotFound();
            }

            var comments = getBlogRecordComments(null, 0, ConstantSettings.CommentBatchNumber);
            BlogRecordModel model = new BlogRecordModel(blogrecord, comments);

            addToBlogRecordCounters(null, true);

            return base.PartialOrFullView(model: model);
        }

        [HttpGet]
        public ActionResult Publish(long id, bool publish)
        {
            try
            {
                var blogRecord = unitOfWork.BlogRecordRepository.GetByID(id);
                int _blogID = blogRecord.BlogID;
                blogRecord.Published = publish;
                unitOfWork.BlogRecordRepository.Update(blogRecord);
                unitOfWork.Save();
                return RedirectToAction("AdminBlogView", "Admin", new { blogID = _blogID });
            }
            catch (Exception)
            {
                return Json(new { success = false, message = "problems" }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="blogrecord"></param>
        /// <returns></returns>
        public ActionResult CreateOrEdit(BlogRecord blogrecord)
        {
            bool newRecord = (blogrecord.ID == 0);

            int separationPosition = 0;
            string keyWord = "<!-- pagebreak -->";
            string noteFragment = HtmlProcessing.GetParagraphsBeforeKeyWord(blogrecord.Note, keyWord, out separationPosition);
            if (noteFragment != null)
                noteFragment = noteFragment.Replace(keyWord, "");
            blogrecord.NoteFragment = noteFragment;

            ModelState.Clear();
            bool val = TryValidateModel(blogrecord);

            if (ModelState.IsValid)
            {
                if (newRecord)
                    unitOfWork.BlogRecordRepository.Insert(blogrecord);
                else
                {

                    unitOfWork.BlogRecordRepository.Update(blogrecord);
                }
                unitOfWork.Save();

                // realtion with tags
                saveRelationWithTags(blogrecord.ID);
                // addition save for tags
                blogrecord.TagString = TagController.tagListToString((List<Tag>)Session[TagController.SESSION_MANAGMENT_TAGS]);
                unitOfWork.Save();

                // delete unnecessary images from Storage
                deleteUnnecessaryImages(blogrecord.Note, Session["BLOG_IMAGES"] as List<string>);

                return RedirectToAction("AdminBlogView", "Admin", new { blogID = blogrecord.BlogID });
            }

            return base.PartialOrFullView("Create", model: blogrecord);
        }

        //
        // GET: /BlogRecord/Edit/5
        public ActionResult LoadBlogRecord(long id = 0, int? blogID = null)
        {
            // clear buffers
            Session[TagController.SESSION_MANAGMENT_TAGS] = null;
            Session["BLOG_IMAGES"] = new List<string>();

            BlogRecord blogrecord = new BlogRecord();
            if (id != 0)
            {
                blogrecord = unitOfWork.BlogRecordRepository.GetByID(id);
                if (blogrecord == null)
                {
                    return HttpNotFound();
                }

                // get tags from db
                var tagsList = unitOfWork.BlogRecordAndTagRepository.Get(filter: obj => obj.BlogRecordID == id,
                    orderBy: q => q.OrderBy(x => x.Tag.Name), includeProperties:"Tag").Select(obj => obj.Tag).ToList();
                Session[TagController.SESSION_MANAGMENT_TAGS] = tagsList;
            }
            else
            {
                blogrecord.CreationDate = DateTime.Now;
                blogrecord.BlogID = blogID.Value;
                blogrecord.UserID = getCurrentUserID().Value;
            }
            return base.PartialOrFullView("Create", model:blogrecord);
        }

        /// <summary>
        /// open delete window
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Delete(long id = 0)
        {
            BlogRecord record = unitOfWork.BlogRecordRepository.GetByID(id);
            if (record == null)
            {
                return HttpNotFound();
            }

            var model = new DeleteDialogModel(id, record.Title, "запись", new ManageViewCommonModel("BlogRecord", "Delete"));
            return PartialView("..//Shared//CommonViews//DeleteDialog", model);
        }

        /// <summary>
        /// delete user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(long? id)
        {
            if (id.HasValue)
            {
                deleteRelationWithTags(id.Value);
                unitOfWork.BlogRecordRepository.Delete(id.Value);
                unitOfWork.Save();
                return Json(new { success = true, message = "success" });
            }
            else
                return Json(new { success = false, message = "problems" });
        }


        /*
         * Tag managment
         */
        private void saveRelationWithTags(long blogRecordID)
        {
            if (Session[TagController.SESSION_MANAGMENT_TAGS] != null)
            {
                var newTagIDs = (from tag in (List<Tag>)Session[TagController.SESSION_MANAGMENT_TAGS]
                                 select tag.ID).ToList();

                var previousTagIDs = unitOfWork.BlogRecordAndTagRepository.Get(filter:
                    relation => relation.BlogRecordID == blogRecordID).Select(obj => obj.TagID);

                var forDeleting = previousTagIDs.Except(newTagIDs);
                var forAdding = newTagIDs.Except(previousTagIDs);

                for (int i = 0; i < forDeleting.Count(); i++)
                {
                    long tagID = forDeleting.ElementAt(i);

                    var pa = unitOfWork.BlogRecordAndTagRepository.Get(filter:
                            relation => relation.BlogRecordID == blogRecordID &&
                                        relation.TagID == tagID).FirstOrDefault();

                    if (pa != null)
                        unitOfWork.BlogRecordAndTagRepository.Delete(pa);
                }

                for (int i = 0; i < forAdding.Count(); i++)
                {
                    var pa = new BlogRecordAndTag();
                    pa.BlogRecordID = blogRecordID;
                    pa.TagID = forAdding.ElementAt(i);
                    unitOfWork.BlogRecordAndTagRepository.Insert(pa);
                }
            }
        }

        private void deleteRelationWithTags(long blogRecordID)
        {
            var blogRecordAndTagRelations = unitOfWork.BlogRecordAndTagRepository.Get(filter:
                relation => relation.BlogRecordID == blogRecordID);
            for (int i = 0; i < blogRecordAndTagRelations.Count(); i++)
            {
                unitOfWork.BlogRecordAndTagRepository.Delete(blogRecordAndTagRelations.ElementAt(i));
            }
        }

        [HttpPost]
        public WrappedJsonResult UploadImage(HttpPostedFileWrapper imageFile)
        {
            if (imageFile == null || imageFile.ContentLength == 0)
            {
                return new WrappedJsonResult
                {
                    Data = new
                    {
                        IsValid = false,
                        Message = "No file was uploaded.",
                        ImagePath = string.Empty
                    }
                };
            }

            string absoluteUri = null;
            bool res = GeneralImageStorage.saveBlogImages(imageFile, DateTime.Now, out absoluteUri);
            if (res)
            {
                List<string> uriList = new List<string>();
                if (Session["BLOG_IMAGES"] != null)
                    uriList = (List<string>)Session["BLOG_IMAGES"];
                uriList.Add(absoluteUri);
                Session["BLOG_IMAGES"] = uriList;
            }

            return new WrappedJsonResult
            {
                Data = new
                {
                    IsValid = true,
                    Message = string.Empty,
                    ImagePath = Url.Content(absoluteUri)
                }
            };
        }


        void deleteUnnecessaryImages(string postText, List<string> uriList)
        {
            if (uriList != null && postText != null)
            {
                for (int i = 0; i < uriList.Count; i++)
                {
                    int index = postText.IndexOf(uriList[i]);
                    if (index < 0)
                    {
                        GeneralImageStorage.deleteFile(uriList[i]);
                    }
                }
            }
        }

        /// <summary>
        /// get n comments
        /// </summary>
        /// <param name="lastCommentID"></param>
        /// <param name="skipSize"></param>
        /// <param name="batchSize"></param>
        /// <returns></returns>
        private CommentBoxModel getBlogRecordComments(int? lastCommentID, int skipSize, int batchSize)
        {
            long? currentUserID = getCurrentUserID();

            string roleName = getRoleName();
            bool alwaysCanEdit = ((roleName == eUserRoles.admin.ToString()) || (roleName == eUserRoles.superadmin.ToString()) ||
                            (roleName == eUserRoles.moderator.ToString()) || (roleName == eUserRoles.editor.ToString()));

            var blogRecordID = (long)Session["BlogRecordID"];

            var blogRecordComments = unitOfWork.CommentBlogRecordRepository.Get(filter: obj => obj.BlogRecordID == blogRecordID && !obj.Deleted,
                includeProperties: "UserAuth").Skip(skipSize).Take(batchSize);
            List<CommonCommentViewModel> comments = new List<CommonCommentViewModel>();
            for (int i = 0; i < blogRecordComments.Count(); i++)
            {
                var comment = blogRecordComments.ElementAt(i);
                CommonCommentViewModel _comment = new CommonCommentViewModel(blogRecordComments.ElementAt(i).ID,
                                                                    comment.UserAuth,
                                                                    comment.CreationDate,
                                                                    comment.Comment,
                                                                    (alwaysCanEdit || (currentUserID == comment.UserAuth.ID)), 
                                                                    "BlogRecord");
                comments.Add(_comment);
            }

            //int wholeCommentNumber = unitOfWork.CommentBlogRecordRepository.Get(obj => obj.BlogRecordID == blogRecordID && !obj.Deleted).Count();
            CommentBoxModel model = new CommentBoxModel(comments, new int?(), new KeyValuePair<string, string>("GetMoreBlogRecordComments", "BlogRecord"), unitOfWork.UserManagment.getUserLogoByID(getCurrentUserID()));
            return model;
        }


        [HttpPost]
        public ActionResult GetMoreBlogRecordComments(int? pageNumber)
        {
            var page = pageNumber.HasValue ? pageNumber.Value : 0;

            int skipNumber = (page) * ConstantSettings.CommentBatchNumber;
            var model = getBlogRecordComments(null, skipNumber, ConstantSettings.CommentBatchNumber).CommentsList;
            return base.PartialView("..//ProgramEdition//_ViewCommentList", model);
        }


        /// <summary>
        /// Add comment
        /// </summary>
        /// <param name="commentText"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AddComment(string commentText)
        {
            var blogRecordID = (long)Session["BlogRecordID"];
            if (!string.IsNullOrWhiteSpace(commentText))
            {
                CommentBlogRecord comment = new CommentBlogRecord();
                comment.Comment = commentText;
                comment.CreationDate = DateTime.Now;
                comment.BlogRecordID = blogRecordID;
                comment.UserID = getCurrentUserID().Value;

                unitOfWork.CommentBlogRecordRepository.Insert(comment);
                unitOfWork.Save();
                addToBlogRecordCounters(true, false);
            }

            CommentBoxModel model = getBlogRecordComments(null, 0, ConstantSettings.CommentBatchNumber);

            return base.PartialView("..//ProgramEdition//_ViewCommentBox", model);
        }

        /// <summary>
        /// Delete comment
        /// </summary>
        /// <param name="commentID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DeleteComment(long commentID)
        {
            var comment = unitOfWork.CommentBlogRecordRepository.GetByID(commentID);
            unitOfWork.CommentBlogRecordRepository.Delete(comment);
            unitOfWork.Save();
            addToBlogRecordCounters(true, false);

            CommentBoxModel model = getBlogRecordComments(null, 0, ConstantSettings.CommentBatchNumber);
            return base.PartialView("..//ProgramEdition//_ViewCommentBox", model);
        }

        // ----------------------------------------------------------------------------------------------
        //  Counters
        // ----------------------------------------------------------------------------------------------
        private bool addToBlogRecordCounters(bool? updateCommentCounter, bool? addToViewedCounter)
        {
            try
            {
                var blogRecordID = (long)Session["BlogRecordID"];
                return addToBlogRecordCounters(unitOfWork, Request, blogRecordID, updateCommentCounter, addToViewedCounter);
            }
            catch (Exception)
            {
                // logging
                return false;
            }
        }

        public static bool addToBlogRecordCounters(UnitOfWork unitOfWork, HttpRequestBase request, long blogRecordID, bool? updateCommentCounter, bool? addToViewedCounter)
        {
            try
            {
                // check whether request was done not by real user
                if (LogManagment.isUserRequest(request))
                {

                    var blockRecord = unitOfWork.BlogRecordRepository.GetByID(blogRecordID);
                    if (updateCommentCounter.HasValue && updateCommentCounter.Value)
                    {
                        // get comment's number
                        var count = unitOfWork.CommentBlogRecordRepository.Get(obj => obj.BlogRecordID == blogRecordID && !obj.Deleted).Count();
                        blockRecord.CommentsCounter = count;
                    }
                    else if (addToViewedCounter.HasValue && addToViewedCounter.Value)
                    {
                        blockRecord.ViewedCounter += 1;
                    }

                    unitOfWork.BlogRecordRepository.Update(blockRecord);
                    unitOfWork.Save();
                }
            }
            catch (Exception)
            {
                // logging
                return false;
            }
            return true;
        }


        //// SUBSCRIPTION

        //bool? checkBlogSuscription(int blogID)
        //{
        //    long? userID = getCurrentUserID();
        //    if (!userID.HasValue)
        //        return null;

        //    // #TODO
        //    SubscriptionBlog subcription = unitOfWork.SubscriptionBlogRepository.Get(filter: obj => obj.BlogID == blogID && obj.UserID == userID).FirstOrDefault();
        //    if (subcription == null || subcription.Deleted)
        //    {
        //        return false;
        //    }
        //    else
        //        return true;
        //}


        protected override void Dispose(bool disposing)
        {
            unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}