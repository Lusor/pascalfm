﻿using System.Web;
using System.Web.Optimization;

namespace PascalWeb
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // /RoxieTheme/css  /js
      //      bundles.Add(new StyleBundle("~/Content/RoxieTheme/bootstrap").Include(
      //          "~/Content/RoxieTheme/bootstrap/bootstrap.min.css"));

            bundles.Add(new StyleBundle("~/Content/RoxieTheme/css/css").Include(
                "~/Content/RoxieTheme/css/bootstrap.min.css", 
                "~/Content/RoxieTheme/css/style-custom-main.css",
                "~/Content/RoxieTheme/css/style-custom-podcast.css",
                "~/Content/RoxieTheme/css/media.css",
                "~/Content/RoxieTheme/css/font-awesome.min.css",
                "~/Content/RoxieTheme/css/animate.css",
                "~/Content/RoxieTheme/css/style-custom.css"
            ));

            // good solution
            // http://stackoverflow.com/questions/14814730/mvc-website-images-path-not-valid-in-release-version

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                //  "~/Scripts/bootstrap.js",
                //  "~/Content/RoxieTheme/jsbootstrap.js",
                  "~/Scripts/respond.js",
                  "~/Scripts/bootstrap-hover-dropdown.js",
                  "~/Scripts/datetimepicker/bootstrap-datetimepicker.js"
                  ));

            bundles.Add(new StyleBundle("~/Content/datetimepicker/css").Include(
                        "~/Content/datetimepicker/bootstrap-datetimepicker.min.css"
                        ));

            //bundles.Add(new StyleBundle("~/Content/bootstrapcss").Include(
            //            "~/Content/RoxieTheme/css/bootstrap.min.css",
            //            "~/Content/bootstrap-responsive.min.css",
            //            "~/Content/datetimepicker/bootstrap-datetimepicker.min.css"
            //            ));


            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modalform").Include(
            "~/Scripts/modalform.js"));
        }
    }
}