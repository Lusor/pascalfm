﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Web.WebPages.OAuth;
using PascalWeb.Models;
using PascalWeb.Additional.Auth;

namespace PascalWeb
{
    public static class AuthConfig
    {
        public static void RegisterAuth()
        {
            // To let users of this site log in using their accounts from other sites such as Microsoft, Facebook, and Twitter,
            // you must update this site. For more information visit http://go.microsoft.com/fwlink/?LinkID=252166

            //OAuthWebSecurity.RegisterMicrosoftClient(
            //    clientId: "",
            //    clientSecret: "");

            //OAuthWebSecurity.RegisterTwitterClient(
            //    consumerKey: "",
            //    consumerSecret: "");

            //OAuthWebSecurity.RegisterFacebookClient(
            //    appId: "",
            //    appSecret: "");

            //OAuthWebSecurity.RegisterGoogleClient();

            //Dictionary<string, object> extraData = new Dictionary<string, object>();
            //extraData.Add();

            var facebooksocialData = new Dictionary<string, object>();
            facebooksocialData.Add("scope", "email, publish_stream, read_stream");


            OAuthWebSecurity.RegisterFacebookClient(
                appId: "417080805130325",
                appSecret: "3c058f538f9e046776acb1da40779612",
                displayName: "facebook",
                extraData: facebooksocialData);


            OAuthWebSecurity.RegisterClient(
                client: new VKontakteAuthenticationClient(
                        "4905689", "9GiTIsftQPvIiQfR5eyn"),
                displayName: "vkontatke", // надпись на кнопке
                extraData: null);

            //OAuthWebSecurity.RegisterClient(
            //   client: new VKontakteAuthenticationClient(
            //          "4905689", "9GiTIsftQPvIiQfR5eyn"),
            //   displayName: "ВКонтакте", // надпись на кнопке
            //   extraData: null);
        }
    }
}
