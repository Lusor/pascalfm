﻿using FluentScheduler;
using FluentScheduler.Model;
using PascalWeb.Additional;
using PascalWeb.Additional.Auth;
using PascalWeb.ExternalLibs.FluentScheduler;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace PascalWeb
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        //readonly static log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        //public static void RegisterRoutes(RouteCollection routes)
        //{
        //    routes.LowercaseUrls = true;
        //    routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

        //    routes.MapRoute(
        //        "Default", // Route name
        //        "{controller}/{action}/{id}", // URL with parameters
        //        new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
        //    );

        //}

        protected void Application_Start()
        {

            AreaRegistration.RegisterAllAreas();

            //this should be line #1 in this method
            GlobalConfiguration.Configure(WebApiConfig.Register);

           // WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);

            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();

            //ControllerBuilder.Current.SetControllerFactory(new DefaultControllerFactory(new CultureAwareControllerActivator()));

            // logging
            log4net.Config.XmlConfigurator.Configure(new FileInfo(Server.MapPath("~/Web.config")));

            // for schedule task
            TaskManager.UnobservedTaskException += SchedulerRegistry_UnobservedTaskException;
            TaskManager.Initialize(new SchedulerRegistry());

            // for messages on Russian
            DefaultModelBinder.ResourceClassKey = "Messages_ru";
        }



        static void SchedulerRegistry_UnobservedTaskException(TaskExceptionInformation sender, UnhandledExceptionEventArgs e)
        {
            PascalLogging.Logger.Info("Achtung! ITask Task: " + e.ExceptionObject);
            Console.WriteLine("Achtung! ITask Task: " + e.ExceptionObject);
	        //Log.Fatal("An error happened with a scheduled task: " + e.ExceptionObject);
        } 


        public class CultureAwareControllerActivator : IControllerActivator
        {
            public IController Create(RequestContext requestContext, Type controllerType)
            {
                //Get the {language} parameter in the RouteData
                string language = requestContext.RouteData.Values["language"] == null ?
                    "ru-RU" : requestContext.RouteData.Values["language"].ToString();

                //Get the culture info of the language code
                CultureInfo culture = CultureInfo.GetCultureInfo(language);
                Thread.CurrentThread.CurrentCulture = culture;
                Thread.CurrentThread.CurrentUICulture = culture;

                return DependencyResolver.Current.GetService(controllerType) as IController;
            }
        }


        /// <summary>
        /// http://peterbromberg.net/post/Implementing-a-Custom-IPrincipal-in-an-ASPNET-MVC-Application.aspx
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];

            if (authCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                CustomPrincipalSerializedModel serializeModel = serializer.Deserialize<CustomPrincipalSerializedModel>(authTicket.UserData);
                cPascalPrincipal newUser = new cPascalPrincipal(authTicket.Name);
                newUser.UserID = serializeModel.UserID;
                newUser.Login = serializeModel.Login;
                newUser.RoleName = serializeModel.RoleName;
                HttpContext.Current.User = newUser;

            }
        }
    }
       
}