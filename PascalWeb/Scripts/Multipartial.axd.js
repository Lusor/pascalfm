﻿

function MultipartialUpdate(views) {
    if (views.get_data) views = eval(views.get_data()); //for the MVC2 compatibility
    for (v in views)
        if (views[v].script) {
            eval(views[v].script);
        }
        else {
            $('#' + views[v].updateTargetId).html(views[v].html);
        }
    return false;
}