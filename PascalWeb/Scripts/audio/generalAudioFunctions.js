﻿

// general function
function showAndHideControl(hideLabel, showLabel, ID) {
    $('#' + hideLabel + '_' + ID).hide();
    $('#' + showLabel + '_' + ID).show();
}

function getAudioControl() {
    audio = $('audio');
    return audio;
}


exports.showAndHideControl = showAndHideControl;
exports.getAudioControl = getAudioControl;