﻿// Метод выполняется при загрузке документа
$(document).ready(function () {
    // Подписываемся на навигацию браузера по страницам
    //window.onpopstate = function (event) {
    //    if (event.state == "ajax")
    //        window.location.reload();
    //    else
    //        window.history.replaceState("ajax", document.title, window.location.href);
    //    event.preventDefault();
    //};
    //// Устанавливаем новый заголовок страницы
    //document.title = $("#pageTitle").html();
});

// Функция, вызываемая при старте загрузки страницы через Ajax
function changeLoadMesage(message) {
    //// Назначаем поясняющий текст окну загрузки
    //$("#loadMessage").empty();
    //if (message != null)
    //    $("#loadMessage").append(message);
}

// Функция, вызываемая при окончании загрузки страницы через Ajax
function onPageLoaded() {
    // Запоминаем новое состояниеonPageLoaded 
    //var url = $("#pageUrl").html().replace("&amp;", "&");
    //window.history.pushState("ajax", document.title, url);
    //// Устанавливаем новый заголовок страницы
    //document.title = $("#pageTitle").html();
    //// Пересчитываем клиентскую валидацию
    //$.validator.unobtrusive.parse($("#main"));
}

// Функция, при возврате на предыдущую страницу
function backToPreviousPage() {
    // Назначаем поясняющий текст окну загрузки
    window.history.back();
}