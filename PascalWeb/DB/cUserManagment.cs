﻿using PascalWeb.Additional;
using PascalWeb.Additional.Auth;
using PascalWeb.Additional.ImageProcessing;
using PascalWeb.Additional.Storage;
using PascalWeb.DB.Enumerators;
using PascalWeb.DB.PageModels;
using PascalWeb.DB.PageModels.Registration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;

namespace PascalWeb.DB
{
    public class cUserManagment
    {
        PascalDBEntities context = null;

        public cUserManagment(PascalDBEntities _context)
        {
            context = _context;
        }

        /// <summary>
        /// get roleID by role name
        /// </summary>
        /// <param name="context"></param>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public int? getUserRoleIDByName(string roleName)
        {
            int? id = null;
            if (string.IsNullOrWhiteSpace(roleName))
                return null;
            roleName = roleName.ToLower();
            id = (from role in context.UserRole
                  where role.Name.CompareTo(roleName) == 0
                  select role.ID).FirstOrDefault();
            return id;
        }

        /// <summary>
        /// get role by user ID
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public string getUserRoleNameByUserID(long userID)
        {
            string roleName = null;
            roleName = (from user in context.UserAuth
                        where user.ID == userID
                        select user.UserRole.Name).FirstOrDefault();
            return roleName;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public long? getUserIDByEmail(string email)
        {
            var user = getUserByEmail(email);
            if (user == null)
                return null;
            else
                return user.ID;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public UserAuth getUserByEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return null;
            var userAuth = (from user in context.UserAuth
                  where user.Email.ToLower() == email.ToLower()
                  select user).FirstOrDefault();
            return userAuth;
        }

        /// <summary>
        /// get user (auth) by login
        /// </summary>
        /// <param name="context"></param>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public UserAuth getUserByID(long? userID)
        {
            if (!userID.HasValue)
                return null;
            var userAuth = context.UserAuth.Include("UserRole").Where(obj => obj.ID == userID.Value).FirstOrDefault();
            return userAuth;
        }


        public bool createUser(UserAuth userAuth, UserInfo userInfo)
        {
            if(userAuth == null)
                return false;

            context.UserAuth.Add(userAuth);
            context.SaveChanges();

            if (userInfo != null)
            {
                context.UserInfoes.Add(userInfo);
                context.SaveChanges();
            }

            return true;
        }

        /// <summary>
        /// get user's (auth) list by login 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public IEnumerable<UserAuth> searchUsersByEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return null;
            var users = context.UserAuth.Include("UserRole").Where(user => user.Email.ToLower().Contains(email.ToLower()));
            return users;
        }

        /// <summary>
        /// get user's (auth) list by login 
        /// </summary>
        /// <param name="userData"></param>
        /// <returns></returns>
        public IEnumerable<UserAuth> searchUsersByNameAndEmail(string userData, int maxResultsNumber = 100)
        {
            if (string.IsNullOrWhiteSpace(userData))
                return null;

            var users = context.UserAuth.Include("UserRole").Where(user => user.Email.Contains(userData) ||
                                user.Surname.Contains(userData) ||
                                user.Name.Contains(userData)).Take(maxResultsNumber);
            return users;
        }


        public IEnumerable<UserAuth> searchUsersByDataAndRole(string userData, int? roleID, int maxResultsNumber = 100)
        {
            if (string.IsNullOrWhiteSpace(userData) && !roleID.HasValue)
                return null;

            // if only roleID is not null
            if (string.IsNullOrWhiteSpace(userData))
            {
                var users = context.UserAuth.Include("UserRole").Where(user => user.UserRoleID == roleID).Take(maxResultsNumber);
                return users;
            }
            else
            {
                var users = context.UserAuth.Include("UserRole").Where(user => (user.Email.Contains(userData) ||
                                    user.Surname.Contains(userData) || user.Name.Contains(userData)) && 
                                    (!roleID.HasValue || (roleID.HasValue && user.UserRoleID == roleID.Value))).Take(maxResultsNumber);
                return users;
            }
        }

        /// <summary>
        /// get user's (auth) list by login 
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public IEnumerable<UserAuth> searchUsersByRole(eUserRoles role, int maxResultsNumber = 100)
        {
            string roleName = role.ToString();
            //var users = (from user in context.UserAuth
            //             where user.UserRole.Name.CompareTo(roleName) == 0
            //             select user).Take(maxResultsNumber);

            var users = context.UserAuth.Include("UserRole").Where(user => user.UserRole.Name.CompareTo(roleName) == 0).Take(maxResultsNumber);
            return users;
        }

        /// <summary>
        /// get user's (auth) list by login 
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public IEnumerable<UserAuth> searchAllUsersExceptRole(eUserRoles role, int maxResultsNumber = 100)
        {
            string roleName = role.ToString();
            //var users = (from user in context.UserAuth
            //             where user.UserRole.Name.CompareTo(roleName) != 0
            //             select user).Take(maxResultsNumber);
            var users = context.UserAuth.Include("UserRole").Where(user => user.UserRole.Name.CompareTo(roleName) != 0).Take(maxResultsNumber);
            return users;
        }
        /// <summary>
        /// get permission for user #IMPORTANT
        /// </summary>
        /// <param name="controllerName"></param>
        /// <param name="actionName"></param>
        /// <param name="roleName"></param>
        /// <param name="toLoginPage"></param>
        /// <returns></returns>
        public bool getUserRight(string controllerName, string actionName, string roleName, out bool toLoginPage )
        {
            /*
             * Возможные варианты:
             * Контроллер   Метод    БезРоли   Роль    IsAvailable
             * Home         *        1         null    1
             * Home         *        0         4       1
             * Home         Index    1         null    1
             * Home         Index    0         4       1
             * 
             * Алгоритм:
             *  1. по имени контроллера выбираем все правила
             *  2. Ищем правило по имени метода:
             *      если найден - проверяем наличие конкретной роли
             *          если да, return IsAvailable
             *          если нет, то проверям флаг БезРоли нет - идем дальше;
             *      если не найден - идем дальше
             *  3. Ищем имя метода *, 
             *      если найден - проверяем наличие конкретной роли
             *          если да, return IsAvailable
             *          если нет, то проверям флаг БезРоли нет - return false;
             *      если не найден - return false
             */

            controllerName = controllerName != null ? controllerName.ToLower() : string.Empty;
            actionName = actionName != null ? actionName.ToLower()  : string.Empty;

            int? roleID = getUserRoleIDByName(roleName);

            // 1
            var query = (from right in context.UserRights
                         where right.ControllerName.ToLower().CompareTo(controllerName) == 0
                         select right).ToList();

            // 2 a
            var rule = query.Where(s => s.ActionName.ToLower().CompareTo(actionName) == 0 && s.UserRoleID == roleID && roleID.HasValue).FirstOrDefault();
            if (rule != null)
            {
                toLoginPage = rule != null ? rule.ToLoginPage : false;
                return rule.IsAvailable;
            }

            // 2 b
            rule = query.Where(s => s.ActionName.ToLower().CompareTo(actionName) == 0 && s.WithoutRole == true).FirstOrDefault();
            if (rule != null)
            {
                toLoginPage = rule != null ? rule.ToLoginPage : false;
                return rule.IsAvailable;
            }

            // 3 a
            rule = query.Where(s => s.ActionName.ToLower().CompareTo("*") == 0 && s.UserRoleID == roleID && roleID.HasValue).FirstOrDefault();
            if (rule != null)
            {
                toLoginPage = rule != null ? rule.ToLoginPage : false;
                return rule.IsAvailable;
            }

            // 3 b
            rule = query.Where(s => s.ActionName.ToLower().CompareTo("*") == 0 && s.WithoutRole == true).FirstOrDefault();
            if (rule != null)
            {
                toLoginPage = rule != null ? rule.ToLoginPage : false;
                return rule.IsAvailable;
            }

            toLoginPage = false;
            return false;
        }


        public bool updateUser(long id, UserUnitedModel model, HttpPostedFileBase file)
        {
            bool newRecord = (model.UserInfo.ID == 0);
            string previousSmallImageAbsoluteUri = null, previousMediumImageAbsoluteUri = null, previousSourceImageAbsoluteUri = null;
            if (!newRecord)
            {
                // save paths for possible deletion
                UserInfo tempUserInfo = context.UserInfoes.Find(id);
                previousSmallImageAbsoluteUri = tempUserInfo.SmallLogoPath;
                previousMediumImageAbsoluteUri = tempUserInfo.MediumLogoPath;
                previousSourceImageAbsoluteUri = tempUserInfo.SourceLogoPath;

                // it is not necessary I don't know why TODO investigate
                // detach record from context (because of presence of two entity with the same key)
               // context.Entry(tempUserInfo).State = EntityState.Detached;
            }


            UserAuth userAuth = context.UserAuth.Find(id);
            UserInfo userInfo = context.UserInfoes.Find(id);
            if (userAuth == null)
                return false;

            var attachedUserAuth = context.Entry(userAuth);

            userAuth.Surname = model.UserAuth.Surname;
            userAuth.Name = model.UserAuth.Name;
            userAuth.Email = model.UserAuth.Email;
            userAuth.IsActivated = model.UserAuth.IsActivated;
            userAuth.IsLocked = model.UserAuth.IsLocked;
            userAuth.UserRoleID = model.UserAuth.UserRoleID;


            if (file != null)
            {
                byte[] imageBytes = cFileWork.ConvertToBytes(file);

                Image smallImage = null, mediumImage = null;
                cFileWork.convertIconImage(imageBytes, out smallImage, out mediumImage);

                var smallImageBytes = ImageProcessing.ImageToByte(smallImage);
                model.UserInfo.LogoContentType = file.ContentType;

                string smallImageAbsoluteUri = null;
                bool res = GeneralImageStorage.saveUserImages(smallImage, DateTime.Now, out smallImageAbsoluteUri);
                string mediumImageAbsoluteUri = null;
                res = GeneralImageStorage.saveUserImages(mediumImage, DateTime.Now, out mediumImageAbsoluteUri);
                string sourceImageAbsoluteUri = null;
                res = GeneralImageStorage.saveUserImages(file, DateTime.Now, out sourceImageAbsoluteUri);

                model.UserInfo.SmallLogoPath = smallImageAbsoluteUri;
                model.UserInfo.MediumLogoPath = mediumImageAbsoluteUri;
                model.UserInfo.SourceLogoPath = sourceImageAbsoluteUri;
                // IMPORTANT
                userAuth.SmallLogoPath = smallImageAbsoluteUri;
            }
            else
            {
                model.UserInfo.SmallLogoPath = previousSmallImageAbsoluteUri;
                model.UserInfo.MediumLogoPath = previousMediumImageAbsoluteUri;
                model.UserInfo.SourceLogoPath = previousSourceImageAbsoluteUri;

                // IMPORTANT
                userAuth.SmallLogoPath = previousSmallImageAbsoluteUri;

                //// объект должен быть чтобы его менять
                //if (model.UserInfo.ID > 0)
                //{
                //    context.Entry(model.UserInfo).Property(obj => obj.LogoContentType).IsModified = false;

                //    // for two additional images
                //    context.Entry(model.UserInfo).Property(obj => obj.SmallLogoPath).IsModified = false;
                //    context.Entry(model.UserInfo).Property(obj => obj.MediumLogoPath).IsModified = false;
                //    context.Entry(model.UserInfo).Property(obj => obj.SourceLogoPath).IsModified = false;
                //}

                // it is unnecessary to delete previous images
                previousSmallImageAbsoluteUri = null;
                previousMediumImageAbsoluteUri = null;
                previousSourceImageAbsoluteUri = null;
            }

            attachedUserAuth.CurrentValues.SetValues(userAuth);

            model.UserInfo.ID = id;

            if (userInfo == null)
            {
                context.UserInfoes.Add(model.UserInfo);
            }
            else
            {
                context.Entry(userInfo).CurrentValues.SetValues(model.UserInfo);
                context.Entry(userInfo).State = EntityState.Modified;
            }


            context.SaveChanges();

            // delete previous images
            GeneralImageStorage.deleteFile(previousSmallImageAbsoluteUri);
            GeneralImageStorage.deleteFile(previousMediumImageAbsoluteUri);
            GeneralImageStorage.deleteFile(previousSourceImageAbsoluteUri);
            return false;

        }


        /// <summary>
        /// it means that userAuth is already created 100 %
        /// </summary>
        /// <param name="id"></param>
        /// <param name="wizard"></param>
        /// <returns></returns>
        public bool updateUser(long id, WizardRegistrationModel wizard)
        {
            string previousSmallImageAbsoluteUri = null, previousMediumImageAbsoluteUri = null, previousSourceImageAbsoluteUri = null;

            UserInfo userInfo = context.UserInfoes.Find(id);
            bool newRecord = userInfo == null;
            if (userInfo != null)
            {
                // save paths for possible deletion
                previousSmallImageAbsoluteUri = userInfo.SmallLogoPath;
                previousMediumImageAbsoluteUri = userInfo.MediumLogoPath;
                previousSourceImageAbsoluteUri = userInfo.SourceLogoPath;

                // it is not necessary I don't know why TODO investigate
                // detach record from context (because of presence of two entity with the same key)
               // context.Entry(tempUserInfo).State = EntityState.Detached;
            }
            if (userInfo == null)
                userInfo = new UserInfo();

            UserAuth userAuth = context.UserAuth.Find(id);
            //UserInfo userInfo = context.UserInfoes.Find(id);
            if (userAuth == null)
                return false;


            var attachedUserAuth = context.Entry(userAuth);

            userAuth.Surname = wizard.Step1.UserSurname;
            userAuth.Name = wizard.Step1.UserName;
            userAuth.Email = wizard.Step1.Email;
            userAuth.IsActivated = wizard.Step1.IsActivated;
            userAuth.IsLocked = wizard.Step1.IsLocked;
            userAuth.UserRoleID = wizard.Step1.UserRoleID;

            userInfo.Birthdate = DateTime.Now;
            userInfo.EducationStatusID = wizard.Step2.EducationStatusID;
            if (wizard.Step2.GenderIndex.HasValue)
                userInfo.GenderIsMale = wizard.Step2.GenderIndex == 1 ? false : true;
            else
                userInfo.GenderIsMale = null;
            userInfo.MaritalStatusID = wizard.Step2.MaritalStatusID;
            userInfo.Info = wizard.Step2.UserInterests;
            userInfo.UserOccuptaionID = wizard.Step2.UserOccupationID;

            if (wizard.Step1.Image != null && wizard.Step1.Image.Length > 0)
            {
                Image smallImage = null, mediumImage = null;
                cFileWork.convertIconImage(wizard.Step1.Image, out smallImage, out mediumImage);

                var smallImageBytes = ImageProcessing.ImageToByte(smallImage);
                userInfo.LogoContentType = wizard.Step1.ImageType;

                string smallImageAbsoluteUri = null;
                bool res = GeneralImageStorage.saveUserImages(smallImage, DateTime.Now, out smallImageAbsoluteUri);
                string mediumImageAbsoluteUri = null;
                res = GeneralImageStorage.saveUserImages(mediumImage, DateTime.Now, out mediumImageAbsoluteUri);
                string sourceImageAbsoluteUri = null;
                res = GeneralImageStorage.saveUserImages(wizard.Step1.Image, DateTime.Now, out sourceImageAbsoluteUri);

                userInfo.SmallLogoPath = smallImageAbsoluteUri;
                userInfo.MediumLogoPath = mediumImageAbsoluteUri;
                userInfo.SourceLogoPath = sourceImageAbsoluteUri;
                // IMPORTANT
                userAuth.SmallLogoPath = smallImageAbsoluteUri;
            }
            else
            {
                userInfo.SmallLogoPath = previousSmallImageAbsoluteUri;
                userInfo.MediumLogoPath = previousMediumImageAbsoluteUri;
                userInfo.SourceLogoPath = previousSourceImageAbsoluteUri;

                // IMPORTANT
                userAuth.SmallLogoPath = previousSmallImageAbsoluteUri;

                // it is unnecessary to delete previous images
                previousSmallImageAbsoluteUri = null;
                previousMediumImageAbsoluteUri = null;
                previousSourceImageAbsoluteUri = null;
            }

            attachedUserAuth.CurrentValues.SetValues(userAuth);

            userInfo.ID = id;

            if (newRecord)
            {
                context.UserInfoes.Add(userInfo);
            }
            else
            {
                context.Entry(userInfo).CurrentValues.SetValues(userInfo);
                context.Entry(userInfo).State = EntityState.Modified;
            }

            context.SaveChanges();

            // delete previous images
            GeneralImageStorage.deleteFile(previousSmallImageAbsoluteUri);
            GeneralImageStorage.deleteFile(previousMediumImageAbsoluteUri);
            GeneralImageStorage.deleteFile(previousSourceImageAbsoluteUri);
            return false;

        }


        public bool DeleteUser(long id)
        {
            try
            {
                UserAuth userAuth = context.UserAuth.Find(id);
                UserInfo userInfo = context.UserInfoes.Find(id);
                UserExternal userExternal = context.UserExternal.Find(id);

                if (userExternal != null)
                    context.UserExternal.Remove(userExternal);
                if (userInfo != null)
                    context.UserInfoes.Remove(userInfo);
                if (userAuth != null)
                    context.UserAuth.Remove(userAuth);
                context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //

        public string getUserLogoByID(long? id)
        {
            if (!id.HasValue)
                return null;

            var user = context.UserAuth.Find(id);
            return user != null ? user.SmallLogoPath : null;
        }


        /*
         * External users
         */

        public UserExternal checkUserExternal(string provider, string identificator)
        {
            try
            {
                UserExternal user = context.UserExternal.Where(obj => string.Compare(obj.WebsiteType.Name, provider) == 0 && string.Compare(obj.Identifier, identificator) == 0).FirstOrDefault();
                return user;
            }
            catch (Exception exc)
            {
                return null;
            }
        }


    }
}