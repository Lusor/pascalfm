﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.AuxiliaryModels
{
    public class PaginationModel
    {
        public PaginationModel(int pageCount, int currentPageNumber, ManageViewCommonModel manageViewCommonModel)
        {
            PageCount = pageCount;
            CurrentPageNumber = currentPageNumber;
            ManageViewCommonModel = manageViewCommonModel;
        }

        public int PageCount { get; set; }
        public int CurrentPageNumber { get; set; }
        public ManageViewCommonModel ManageViewCommonModel { get; set; }
        
    }
}