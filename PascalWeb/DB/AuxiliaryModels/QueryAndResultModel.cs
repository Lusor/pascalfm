﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.AuxiliaryModels
{
    public class QueryAndResultModel<T>
    {
        public QueryAndResultModel(T data, string query)
        {
            Query = query;
            Data = data;
        }

        public T Data { get; set; }
        public string Query { get; set; }
    }
}