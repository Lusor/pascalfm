﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.AuxiliaryModels
{
    public class PageWithPagination<T>
    {
        public PageWithPagination(T data, PaginationModel paginationModel)
        {
            PaginationModel = paginationModel;
            Data = data;
        }

        public T Data { get; set; }
        public PaginationModel PaginationModel { get; set; }
    }
}