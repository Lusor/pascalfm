﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.AuxiliaryModels
{
    public class ExpandingContainerModel<T>
    {
        public ExpandingContainerModel(T data, string controllerName, string actionName)
        {
            Data = data;
            ControllerName = controllerName;
            ActionName = actionName;
        }

        public T Data { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
    //    public Dictionary<string, string> Parameters { get; set; }
    }
}