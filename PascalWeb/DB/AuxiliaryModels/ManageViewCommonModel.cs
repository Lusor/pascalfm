﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace PascalWeb.DB.AuxiliaryModels
{
    /// <summary>
    /// модель для использования в однотипных view
    /// </summary>
    public class ManageViewCommonModel
    {
        public enum eActionType {Add, Delete, Edit, None};

        public ManageViewCommonModel(string controllerName, string actionName, string updateTargetID, string controlActionName, eActionType actionType)
        {
            ControllerName = controllerName;
            ActionName = actionName;
            RouteValueDictionary = null;

            UpdateTargetID = updateTargetID;
            ControlActionName = controlActionName;
            ActionType = actionType;
        }

        public ManageViewCommonModel(string controllerName, string actionName)
        {
            ControllerName = controllerName;
            ActionName = actionName;
            RouteValueDictionary = null;

            UpdateTargetID = "";
            ControlActionName = "";
            ActionType = eActionType.None;
        }

        public ManageViewCommonModel(string controllerName, string actionName, RouteValueDictionary rvD)
        {
            ControllerName = controllerName;
            ActionName = actionName;
            RouteValueDictionary = rvD;
            UpdateTargetID = "";
            ControlActionName = "";
            ActionType = eActionType.None;
        }

        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public RouteValueDictionary RouteValueDictionary { get; set; }
        public string UpdateTargetID { get; set; }

        // name for button or something like that
        public string ControlActionName { get; set; }
        public eActionType ActionType { get; set; }
    }
}