﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.AuxiliaryModels
{
    public class UserInfoModel
    {
        public UserInfoModel(long userID, string login, string surname, string name)
        {
            UserID = userID;
            Login = login;
            FullName = getFullName(surname, name);
            ProgramList = new List<Program>();
        }

        public UserInfoModel(long userID, string login, string surname, string name, string photoStr)
        {
            UserID = userID;
            Login = login;
            FullName = getFullName(surname, name);
            PhotoStr = photoStr;
            ProgramList = new List<Program>();
        }

        public long UserID { get; set; }
        public string Login { get; set; }
        public string FullName { get; set; }
        public string RoleName { get; set; }
        public string PhotoStr { get; set; }
        public List<Program> ProgramList { get; set; }
        public Blog Blog { get; set; }


        public void setRoleName(string roleName)
        {
            RoleName = roleName;
        }

        public static string getFullName(UserAuth user)
        {
            if (user == null)
                return "";
            return getFullName(user.Surname, user.Name);
        }

        public static string getFullName(string surname, string name)
        {
            string fullName = String.Concat(surname, " ", name);
            fullName = fullName != null ? fullName.Trim() : "";
            return fullName;
        }


        public static List<UserInfoModel> convertFromToUserInfoList(List<UserAuth> userAuths)
        {
            var userInfoList = new List<UserInfoModel>();
            for (int i = 0; i < userAuths.Count; i++)
            {
                var item = new UserInfoModel(userAuths.ElementAt(i).ID, userAuths.ElementAt(i).Email, userAuths.ElementAt(i).Surname, userAuths.ElementAt(i).Name, userAuths.ElementAt(i).SmallLogoPath);
                item.setRoleName(userAuths.ElementAt(i).UserRole.Alias);
                userInfoList.Add(item);
            }
            return userInfoList;
        }

        public static List<UserInfoModel> convertFromToUserInfoList(IEnumerable<UserAuth> userAuths)
        {
            return convertFromToUserInfoList(userAuths.ToList());
        }
    }
}