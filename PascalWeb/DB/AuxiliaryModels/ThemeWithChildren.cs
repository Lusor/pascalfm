﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.AuxiliaryModels
{
    public class ListElementWithChildren<T>
    {
        public ListElementWithChildren(string themeName, IEnumerable<T> childList)
        {
            ThemeName = themeName;
            ChildList = childList;
        }
        public string ThemeName { get; set; }
        public IEnumerable<T> ChildList { get; set; }
    }
}