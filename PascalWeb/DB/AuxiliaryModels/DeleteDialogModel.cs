﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.AuxiliaryModels
{
    public class DeleteDialogModel
    {
        public DeleteDialogModel(object id, string specificName, string whatDelete, ManageViewCommonModel manageViewCommonModel)
        {
            ID = id;
            SpecificName = specificName;
            WhatDelete = whatDelete;
            ManageViewCommonModel = manageViewCommonModel;
        }

        // genitive
        public string WhatDelete { get; set; }
        public object ID { get; set; }
        public string SpecificName { get; set; }
        public ManageViewCommonModel ManageViewCommonModel { get; set; }
    }
}