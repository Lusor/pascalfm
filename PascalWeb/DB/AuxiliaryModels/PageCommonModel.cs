﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PascalWeb.DB.PageModels
{
    public enum PageModelEnumeration { pagination_CurrentNumber, pagination_PageCount };

    public class PageCommonModel<T>
    {
        public PageCommonModel(Dictionary<PageModelEnumeration, object> valueList, T data)
        {
            ValueList = valueList;
            Data = data;
        }

        public Dictionary<PageModelEnumeration, object> ValueList { get; set; }
        public T Data { get; set; }

    }
}
