﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.AuxiliaryModels
{
    public class SiteActionModel
    {

        public SiteActionModel(string controllerName, SortedList<string, ActionRule> actionRules)
        {
            ControllerName = controllerName != null ? controllerName.ToLower() : "";
            ControllerName = ControllerName.Substring(0, ControllerName.IndexOf("controller"));
            ActionRules = actionRules;
        }

        public string ControllerName { get; set; }
        public SortedList<string, ActionRule> ActionRules { get; set; }

    }

    public class ActionRule
    {
        public ActionRule(string actionName, RightFeatures rights)
        {
            ActionName = actionName;
            Rights = rights;
        }

        public string ActionName { get; set; }
        public RightFeatures Rights { get; set; }
    }

    public class RightFeatures
    {
        //public bool ForWithoutRole { get; set; }
        public bool ForNotAuth { get; set; }
        public bool ForGeneralUser { get; set; }
        public bool ForJournalist { get; set; }
        public bool ForEditor { get; set; }
        public bool ForModerator { get; set; }
        public bool ForAdministrator { get; set; }
    }
 
}