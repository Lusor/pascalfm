//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PascalWeb.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProgramEdition
    {
        public ProgramEdition()
        {
            this.ProgramEditionAuthors = new HashSet<ProgramEditionAuthors>();
            this.CommentProgramEditions = new HashSet<CommentProgramEdition>();
            this.ProgramEditionAndTag = new HashSet<ProgramEditionAndTag>();
        }
    
        public long ID { get; set; }
        public System.DateTime CreationDate { get; set; }
        public int ProgramID { get; set; }
        public System.DateTime AirDate { get; set; }
        public string EditionTittle { get; set; }
        public string ShortDescription { get; set; }
        public Nullable<long> ContentID { get; set; }
        public Nullable<bool> Published { get; set; }
        public long ViewedCounter { get; set; }
        public long CommentsCounter { get; set; }
        public long ListenedCounter { get; set; }
        public string TagString { get; set; }
        public Nullable<int> Duration { get; set; }
        public long DownloadCounter { get; set; }
        public string Recommendations { get; set; }
    
        public virtual Content Content { get; set; }
        public virtual Program Program { get; set; }
        public virtual ICollection<ProgramEditionAuthors> ProgramEditionAuthors { get; set; }
        public virtual ICollection<CommentProgramEdition> CommentProgramEditions { get; set; }
        public virtual ICollection<ProgramEditionAndTag> ProgramEditionAndTag { get; set; }
        public virtual ProgramEditionText ProgramEditionText { get; set; }
    }
}
