﻿using PascalWeb.Controllers;
using PascalWeb.DB.PageModels;
using PascalWeb.DB.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB
{
    public class ProgramEditionManagment
    {
        /// <summary>
        /// update counters
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="programEditionID"></param>
        /// <param name="updateCommentCounter"></param>
        /// <param name="addToViewedCounter"></param>
        /// <param name="addToListenedCounter"></param>
        /// <returns></returns>
        public static bool addToProgramEditionCounters(UnitOfWork unitOfWork, HttpRequestBase request, long programEditionID, bool? updateCommentCounter, bool? addToViewedCounter, bool? addToListenedCounter, bool? addToDownloadCounter)
        {
            try
            {
                // check whether request was done not by real user
                if (LogManagment.isUserRequest(request))
                {

                    var programEdition = unitOfWork.ProgramEditionRepository.GetByID(programEditionID);
                    if (updateCommentCounter.HasValue && updateCommentCounter.Value)
                    {
                        // get comment's number
                        var count = unitOfWork.CommentProgramEditionRepository.Get(obj => obj.ProgramEditionID == programEditionID && !obj.Deleted).Count();
                        programEdition.CommentsCounter = count;
                    }
                    else if (addToViewedCounter.HasValue && addToViewedCounter.Value)
                    {
                        programEdition.ViewedCounter += 1;
                    }
                    else if (addToListenedCounter.HasValue && addToListenedCounter.Value)
                    {
                        programEdition.ListenedCounter += 1;
                    } if (addToDownloadCounter.HasValue && addToDownloadCounter.Value)
                    {
                        programEdition.DownloadCounter += 1;
                    }

                    unitOfWork.ProgramEditionRepository.Update(programEdition);
                    unitOfWork.Save();
                }
            }
            catch (Exception)
            {
                // logging
                return false;
            }
            return true;
        }



    }
}