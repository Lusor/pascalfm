﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace PascalWeb.DB.Repositories
{
    public class GenericRepository<TEntity> where TEntity : class
    {
        internal PascalDBEntities context;
        internal DbSet<TEntity> dbSet;

        public GenericRepository(PascalDBEntities context)
        {
            this.context = context;
            this.dbSet = context.Set<TEntity>();
        }

        public virtual IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "", 
            int? skip = null, int? take = null)
        {
            IQueryable<TEntity> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            if (take.HasValue && !skip.HasValue)
                skip = 0;
            if (skip.HasValue && take.HasValue)
            {
                query = query.Skip(skip.Value).Take(take.Value);
            }

            return query.ToList();
        }

        public virtual TEntity GetByID(object id)
        {
            return dbSet.Find(id);
        }

        public virtual TEntity GetOneItem(Expression<Func<TEntity, bool>> filter, string includeProperties)
        {
            //var item = dbSet.Find(id);
            var item = Get(filter: filter, includeProperties: includeProperties).SingleOrDefault();

            return item;
        }

        public virtual void Insert(TEntity entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Delete(object id)
        {
            TEntity entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual long Count(Expression<Func<TEntity, bool>> filter = null)
        {
            IQueryable<TEntity> query = dbSet;
            if (filter != null)
            {
                query = query.Where(filter);
            }
            return query.LongCount();
        }

        public virtual long Count()
        {
            return dbSet.LongCount();
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        // https://cmatskas.com/an-object-with-the-same-key-already-exists-in-the-objectstatemanager-entity-frawework/
        // http://stackoverflow.com/questions/12585664/an-object-with-the-same-key-already-exists-in-the-objectstatemanager-the-object
        public virtual void Update(TEntity entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;

            //var entry = this.context.Entry(entityToUpdate);
            //var key = this.GetPrimaryKey(entry);

            //if (entry.State == EntityState.Detached)
            //{
            //    var currentEntry = this.databaseSet.Find(key);
            //    if (currentEntry != null)
            //    {
            //        var attachedEntry = this.context.Entry(currentEntry);
            //        attachedEntry.CurrentValues.SetValues(entityToUpdate);
            //    }
            //    else
            //    {
            //        this.databaseSet.Attach(entityToUpdate);
            //        entry.State = EntityState.Modified;
            //    }
            //}
        }
    }
}
