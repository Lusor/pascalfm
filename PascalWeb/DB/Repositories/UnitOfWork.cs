﻿using System;
using PascalWeb.DB;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace PascalWeb.DB.Repositories
{
    public class UnitOfWork : IDisposable
    {
        public UnitOfWork()
        {
            context = new PascalDBEntities();
            context.Configuration.LazyLoadingEnabled = false;
        }

        private PascalDBEntities context = null;

        //private PascalDBEntities context = new PascalDBEntities();
        private GenericRepository<UserRole> userRoleRepository;
        private GenericRepository<UserRight> userRightRepository;
        private GenericRepository<UserAuth> userAuthRepository;
        private GenericRepository<UserInfo> userInfoRepository;
        private GenericRepository<UserExternal> userExternalRepository;

        private GenericRepository<Blog> blogRepository;
        private GenericRepository<BlogRecord> blogRecordRepository;
        private GenericRepository<BlogRecordAndTag> blogRecordAndTagRepository;
        private GenericRepository<CommentBlogRecord> commentBlogRecordRepository;
        private GenericRepository<CommentProgramEdition> commentProgramEditionRepository;

        // program
        private GenericRepository<Program> programRepository;
        private GenericRepository<ProgramAuthors> programAuthorsRepository;
        private GenericRepository<ProgramAndTag> programAndTagRepository;

        // programEdition
        private GenericRepository<ProgramEdition> programEditionRepository;
        private GenericRepository<ProgramEditionText> programEditionTextRepository;
        private GenericRepository<ProgramEditionAuthors> programEditionAuthorsRepository;
        private GenericRepository<ProgramEditionAndTag> programEditionAndTagRepository;


        private GenericRepository<Content> contentRepository;


        private GenericRepository<UserAuth> userAuth;
        private GenericRepository<UserInfo> userInfo;

        // additional
        private GenericRepository<Tag> tagRepository;

        // classifiers
        private GenericRepository<ContentType> contentTypeRepository;
        private GenericRepository<ProgramTheme> programThemeRepository;
        private GenericRepository<MaritalStatus> maritalStatusRepository;
        private GenericRepository<EducationStatus> educationStatusRepository;
        private GenericRepository<UserOccupation> userOccupationRepository;
        private GenericRepository<Country> countryRepository;
        private GenericRepository<EmailDispatchType> emailDispatchTypeRepository;
        private GenericRepository<WebsiteType> websiteTypeRepository;

        // email
        private GenericRepository<EmailDispatch> emailDispatchRepository;

        // logs
        private GenericRepository<LogRequest> logRequestRepository;

        // statistics
        private GenericRepository<StatisticsSlice> statisticsSliceRepository;

        // subscriptions
        private GenericRepository<SubscriptionProgram> subscriptionProgramRepository;
        private GenericRepository<SubscriptionBlog> subscriptionBlogRepository;

        // tables many-to-many
        private GenericRepository<RSSFeedAndProgram> rssFeedAndProgramRepository;


        private cUserManagment userManagment;
        //private cContentManager contentManager;

        public PascalDBEntities getContext()
        {
            return context;
        }

        public GenericRepository<UserRole> UserRoleRepository
        {
            get
            {

                if (this.userRoleRepository == null)
                {
                    this.userRoleRepository = new GenericRepository<UserRole>(context);
                }
                return userRoleRepository;
            }
        }

        public GenericRepository<UserRight> UserRightRepository
        {
            get
            {
                if (this.userRightRepository == null)
                {
                    this.userRightRepository = new GenericRepository<UserRight>(context);
                }
                return userRightRepository;
            }
        }

        public GenericRepository<Blog> BlogRepository
        {
            get
            {
                if (this.blogRepository == null)
                {
                    this.blogRepository = new GenericRepository<Blog>(context);
                }
                return blogRepository;
            }
        }

        public GenericRepository<BlogRecord> BlogRecordRepository
        {
            get
            {
                if (this.blogRecordRepository == null)
                {
                    this.blogRecordRepository = new GenericRepository<BlogRecord>(context);
                }
                return blogRecordRepository;
            }
        }

        public GenericRepository<BlogRecordAndTag> BlogRecordAndTagRepository
        {
            get
            {
                if (this.blogRecordAndTagRepository == null)
                {
                    this.blogRecordAndTagRepository = new GenericRepository<BlogRecordAndTag>(context);
                }
                return blogRecordAndTagRepository;
            }
        }

        public GenericRepository<CommentBlogRecord> CommentBlogRecordRepository
        {
            get
            {
                if (this.commentBlogRecordRepository == null)
                {
                    this.commentBlogRecordRepository = new GenericRepository<CommentBlogRecord>(context);
                }
                return commentBlogRecordRepository;
            }
        }

        public GenericRepository<CommentProgramEdition> CommentProgramEditionRepository
        {
            get
            {
                if (this.commentProgramEditionRepository == null)
                {
                    this.commentProgramEditionRepository = new GenericRepository<CommentProgramEdition>(context);
                }
                return commentProgramEditionRepository;
            }
        }

        public GenericRepository<Program> ProgramRepository
        {
            get
            {
                if (this.programRepository == null)
                {
                    this.programRepository = new GenericRepository<Program>(context);
                }
                return programRepository;
            }
        }

        public GenericRepository<ProgramEdition> ProgramEditionRepository
        {
            get
            {
                if (this.programEditionRepository == null)
                {
                    this.programEditionRepository = new GenericRepository<ProgramEdition>(context);
                }
                return programEditionRepository;
            }
        }

        public GenericRepository<ProgramEditionText> ProgramEditionTextRepository
        {
            get
            {
                if (this.programEditionTextRepository == null)
                {
                    this.programEditionTextRepository = new GenericRepository<ProgramEditionText>(context);
                }
                return programEditionTextRepository;
            }
        }

        public GenericRepository<ProgramEditionAuthors> ProgramEditionAuthorsRepository
        {
            get
            {
                if (this.programEditionAuthorsRepository == null)
                {
                    this.programEditionAuthorsRepository = new GenericRepository<ProgramEditionAuthors>(context);
                }
                return programEditionAuthorsRepository;
            }
        }

        public GenericRepository<ProgramEditionAndTag> ProgramEditionAndTagRepository
        {
            get
            {
                if (this.programEditionAndTagRepository == null)
                {
                    this.programEditionAndTagRepository = new GenericRepository<ProgramEditionAndTag>(context);
                }
                return programEditionAndTagRepository;
            }
        }


        public GenericRepository<Content> ContentRepository
        {
            get
            {
                if (this.contentRepository == null)
                {
                    this.contentRepository = new GenericRepository<Content>(context);
                }
                return contentRepository;
            }
        }

        public GenericRepository<ProgramAuthors> ProgramAuthorsRepository
        {
            get
            {
                if (this.programAuthorsRepository == null)
                {
                    this.programAuthorsRepository = new GenericRepository<ProgramAuthors>(context);
                }
                return programAuthorsRepository;
            }
        }

        public GenericRepository<UserAuth> UserAuthRepository
        {
            get
            {
                if (this.userAuth == null)
                {
                    this.userAuth = new GenericRepository<UserAuth>(context);
                }
                return userAuth;
            }
        }

        public GenericRepository<UserInfo> UserInfoRepository
        {
            get
            {
                if (this.userInfo == null)
                {
                    this.userInfo = new GenericRepository<UserInfo>(context);
                }
                return userInfo;
            }
        }

        public GenericRepository<UserExternal> UserExternalRepository
        {
            get
            {
                if (this.userExternalRepository == null)
                {
                    this.userExternalRepository = new GenericRepository<UserExternal>(context);
                }
                return userExternalRepository;
            }
        }

        /*
         additional
         */
        public GenericRepository<Tag> TagRepository
        {
            get
            {
                if (this.tagRepository == null)
                {
                    this.tagRepository = new GenericRepository<Tag>(context);
                }
                return tagRepository;
            }
        }

        /*
         classifiers
         */
        public GenericRepository<ContentType> ContentTypeRepository
        {
            get
            {
                if (this.contentTypeRepository == null)
                {
                    this.contentTypeRepository = new GenericRepository<ContentType>(context);
                }
                return contentTypeRepository;
            }
        }

        public GenericRepository<ProgramTheme> ProgramThemeRepository
        {
            get
            {
                if (this.programThemeRepository == null)
                {
                    this.programThemeRepository = new GenericRepository<ProgramTheme>(context);
                }
                return programThemeRepository;
            }
        }

        public GenericRepository<MaritalStatus> MaritalStatusRepository
        {
            get
            {
                if (this.maritalStatusRepository == null)
                {
                    this.maritalStatusRepository = new GenericRepository<MaritalStatus>(context);
                }
                return maritalStatusRepository;
            }
        }

        public GenericRepository<EducationStatus> EducationStatusRepository
        {
            get
            {
                if (this.educationStatusRepository == null)
                {
                    this.educationStatusRepository = new GenericRepository<EducationStatus>(context);
                }
                return educationStatusRepository;
            }
        }

        public GenericRepository<UserOccupation> UserOccupationRepository
        {
            get
            {
                if (this.userOccupationRepository == null)
                {
                    this.userOccupationRepository = new GenericRepository<UserOccupation>(context);
                }
                return userOccupationRepository;
            }
        }

        public GenericRepository<Country> CountryRepository
        {
            get
            {
                if (this.countryRepository == null)
                {
                    this.countryRepository = new GenericRepository<Country>(context);
                }
                return countryRepository;
            }
        }

        public GenericRepository<LogRequest> LogRequestRepository
        {
            get
            {
                if (this.logRequestRepository == null)
                {
                    this.logRequestRepository = new GenericRepository<LogRequest>(context);
                }
                return logRequestRepository;
            }
        }

        public GenericRepository<StatisticsSlice> StatisticsSliceRepository
        {
            get
            {
                if (this.statisticsSliceRepository == null)
                {
                    this.statisticsSliceRepository = new GenericRepository<StatisticsSlice>(context);
                }
                return statisticsSliceRepository;
            }
        }

        public GenericRepository<EmailDispatchType> EmailDispatchTypeRepository
        {
            get
            {
                if (this.emailDispatchTypeRepository == null)
                {
                    this.emailDispatchTypeRepository = new GenericRepository<EmailDispatchType>(context);
                }
                return emailDispatchTypeRepository;
            }
        }

        public GenericRepository<WebsiteType> WebsiteTypeRepository
        {
            get
            {
                if (this.websiteTypeRepository == null)
                {
                    this.websiteTypeRepository = new GenericRepository<WebsiteType>(context);
                }
                return websiteTypeRepository;
            }
        }


        // email
        public GenericRepository<EmailDispatch> EmailDispatchRepository
        {
            get
            {
                if (this.emailDispatchRepository == null)
                {
                    this.emailDispatchRepository = new GenericRepository<EmailDispatch>(context);
                }
                return emailDispatchRepository;
            }
        }


        // subscription
        public GenericRepository<SubscriptionProgram> SubscriptionProgramRepository
        {
            get
            {
                if (this.subscriptionProgramRepository == null)
                {
                    this.subscriptionProgramRepository = new GenericRepository<SubscriptionProgram>(context);
                }
                return subscriptionProgramRepository;
            }
        }

        public GenericRepository<SubscriptionBlog> SubscriptionBlogRepository
        {
            get
            {
                if (this.subscriptionBlogRepository == null)
                {
                    this.subscriptionBlogRepository = new GenericRepository<SubscriptionBlog>(context);
                }
                return subscriptionBlogRepository;
            }
        }

        // tables many-to-many
        public GenericRepository<ProgramAndTag> ProgramAndTagRepository
        {
            get
            {
                if (this.programAndTagRepository == null)
                {
                    this.programAndTagRepository = new GenericRepository<ProgramAndTag>(context);
                }
                return programAndTagRepository;
            }
        }

        public GenericRepository<RSSFeedAndProgram> RSSFeedAndProgramRepository
        {
            get
            {
                if (this.rssFeedAndProgramRepository == null)
                {
                    this.rssFeedAndProgramRepository = new GenericRepository<RSSFeedAndProgram>(context);
                }
                return rssFeedAndProgramRepository;
            }
        }
        

        public cUserManagment UserManagment
        {
            get
            {
                if (this.userManagment == null)
                {
                    this.userManagment = new cUserManagment(context);
                }
                return userManagment;
            }
        }


        // tabl
        //public cContentManager ContentManager
        //{
        //    get
        //    {
        //        if (this.contentManager == null)
        //        {
        //            this.contentManager = new cContentManager(context);
        //        }
        //        return contentManager;
        //    }
        //}


        public void Save()
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
        }

      private bool disposed = false;

      protected virtual void Dispose(bool disposing)
      {
         if (!this.disposed)
         {
            if (disposing)
            {
               context.Dispose();
            }
         }
         this.disposed = true;
      }

      public void Dispose()
      {
         Dispose(true);
         GC.SuppressFinalize(this);
      }
   }
}
