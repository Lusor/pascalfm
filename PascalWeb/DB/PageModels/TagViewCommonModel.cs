﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PascalWeb.DB.AuxiliaryModels;

namespace PascalWeb.DB.PageModels
{
    public class TagViewCommonModel
    {
        public TagViewCommonModel(IEnumerable<Tag> tags, ManageViewCommonModel manageViewCommonModel)
        {
            Tags = tags;
            ManageViewCommonModel = manageViewCommonModel;
        }

        public IEnumerable<Tag> Tags { get; set; }
        public ManageViewCommonModel ManageViewCommonModel { get; set; }
    }
}