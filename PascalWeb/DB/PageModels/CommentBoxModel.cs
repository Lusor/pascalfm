﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.PageModels
{
    public class CommentBoxModel
    {
        public CommentBoxModel()
        {
            //WholeCommentNumber = 0;
            CommentsList = new List<CommonCommentViewModel>();
            LastCommentID = null;
            ActionUpdateMethodInfo = new KeyValuePair<string, string>();
        }

        public CommentBoxModel(List<CommonCommentViewModel> commentsList, int? lastCommentID, KeyValuePair<string, string> actionUpdateMethodInfo, string userLogo)
        {
            //WholeCommentNumber = wholeCommentNumber;
            CommentsList = commentsList;
            LastCommentID = lastCommentID;
            ActionUpdateMethodInfo = actionUpdateMethodInfo;
            UserLogo = userLogo;
        }

        public KeyValuePair<string, string> ActionUpdateMethodInfo { get; set; }

        //public int WholeCommentNumber { get; set; }
        public List<CommonCommentViewModel> CommentsList { get; set; }

        // if comment was added - show it
        public int? LastCommentID { get; set; }

        public string UserLogo { get; set; }

    }
}