﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PascalWeb.DB.AuxiliaryModels;

namespace PascalWeb.DB.PageModels
{
    public class AuthorItemListCommonModel
    {
        public AuthorItemListCommonModel(IEnumerable<UserInfoModel> users, ManageViewCommonModel manageViewCommonModel)
        {
            Users = users;
            ManageViewCommonModel = manageViewCommonModel;
        }

        public IEnumerable<UserInfoModel> Users { get; set; }
        public ManageViewCommonModel ManageViewCommonModel { get; set; }
    }
}