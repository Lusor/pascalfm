﻿using PascalWeb.DB.AuxiliaryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.PageModels
{
    public class ProgramAuthorPageModel
    {
        public ProgramAuthorPageModel(List<UserInfoModel> authorList)
        {
            AuthorList = authorList;
        }
        // author list - left
        public List<UserInfoModel> AuthorList { get; set; }

        // either
        public List<UserAuth> PopularAuthors { get; set; }
        // or
        public AuthorInfoModel AuthorInfo { get; set; }

        public class AuthorInfoModel
        {
            public UserUnitedModel UserData { get; set; }
            //public List<Program> authorProgramList { get; set; }
            public KeyValuePair<int?, bool?> BlogSubscription { get; set; }

            public List<LastProgramEditionPageModel.LastProgramEdition> lastEditions { get; set; }
            public List<BlogRecord> lastBlogRecords { get; set; }
        }
    }
}