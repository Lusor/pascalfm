﻿using PascalWeb.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.PageModels
{
    public class LastProgramEditionPageModel
    {
        public LastProgramEditionPageModel()
        {
            LastProgramEditions = new List<LastProgramEdition>();
        }

        public List<LastProgramEdition> LastProgramEditions { get; set; }


        public class LastProgramEdition
        {
            public LastProgramEdition(Program program, ProgramEdition edition, List<string> programEditionTagList)
            {
                Program = program;
                Edition = edition;
                ProgramEditionTagList = programEditionTagList;
            }

            public Program Program { get; set; }
            public ProgramEdition Edition { get; set; }
            public List<string> ProgramEditionTagList { get; set; }
        }

        /// <summary>
        /// convert ProgramEdition to LastProgramEdition
        /// </summary>
        /// <param name="programEdition"></param>
        /// <returns></returns>
        public static LastProgramEdition getLastProgramEditionPageModel(ProgramEdition programEdition)
        {
            var programEditionTagList = TagController.stringToTagList(programEdition.TagString);
            var edition = new LastProgramEditionPageModel.LastProgramEdition(programEdition.Program, programEdition, programEditionTagList);
            return edition;
        }


        /// <summary>
        /// convert IEnumerable<ProgramEdition> to LastProgramEditionPageModel
        /// </summary>
        /// <param name="lastEditions"></param>
        /// <returns></returns>
        public static LastProgramEditionPageModel getLastProgramEditionModel(IEnumerable<ProgramEdition> lastEditions)
        {
            LastProgramEditionPageModel lastProgramEditionPageModel = new LastProgramEditionPageModel();
            var editions = new List<LastProgramEditionPageModel.LastProgramEdition>();
            for (int i = 0; i < lastEditions.Count(); i++)
            {
                var edition = getLastProgramEditionPageModel(lastEditions.ElementAt(i));
                lastProgramEditionPageModel.LastProgramEditions.Add(edition);
            }
            return lastProgramEditionPageModel;
        }
    }

}