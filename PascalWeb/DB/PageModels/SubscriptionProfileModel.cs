﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.PageModels
{
    public class SubscriptionProfileModel
    {
        public SubscriptionProfileModel(List<SubscriptionModel> subscriptionProgramList, List<SubscriptionModel> subscriptionBlogList)
        {
            SubscriptionProgramList = subscriptionProgramList;
            SubscriptionBlogList = subscriptionBlogList;
        }

        public List<SubscriptionModel> SubscriptionProgramList { get; set; }
        public List<SubscriptionModel> SubscriptionBlogList { get; set; }

        public SubscriptionProfileModel(IEnumerable<SubscriptionProgram> subscriptionProgramEnum, IEnumerable<SubscriptionBlog> subscriptionBlogEnum)
        {
            List<SubscriptionModel> subscriptionProgramList = new List<SubscriptionModel>();
            for (int i = 0; i < subscriptionProgramEnum.Count(); i++)
            {
                SubscriptionModel model = new SubscriptionModel(subscriptionProgramEnum.ElementAt(i));
                subscriptionProgramList.Add(model);
            }

            List<SubscriptionModel> subscriptionBlogList = new List<SubscriptionModel>();
            for (int i = 0; i < subscriptionBlogEnum.Count(); i++)
            {
                SubscriptionModel model = new SubscriptionModel(subscriptionBlogEnum.ElementAt(i));
                subscriptionBlogList.Add(model);
            }

            SubscriptionProgramList = subscriptionProgramList;
            SubscriptionBlogList = subscriptionBlogList;
        }

        public class SubscriptionModel
        {
            public SubscriptionModel(long? subscriptionID, bool existed, int subsciptionObjectID, string objectName, string imagePath)
            {
                SubscriptionID = subscriptionID;
                Existed = existed;
                SubsciptionObjectID = subsciptionObjectID;
                ObjectName = objectName;
                ImagePath = imagePath;
            }

            public SubscriptionModel(SubscriptionBlog subscriptionBlog)
            {
                SubscriptionID = subscriptionBlog.ID;
                Existed = subscriptionBlog.Existed;
                SubsciptionObjectID = subscriptionBlog.BlogID;
                ObjectName = subscriptionBlog.Blog.Name;
                ImagePath = subscriptionBlog.Blog.SmallLogoPath;
            }

            public SubscriptionModel(SubscriptionProgram subscriptionProgram)
            {
                SubscriptionID = subscriptionProgram.ID;
                Existed = subscriptionProgram.Existed;
                SubsciptionObjectID = subscriptionProgram.ProgramID;
                ObjectName = subscriptionProgram.Program.Name;
                ImagePath = subscriptionProgram.Program.SmallLogoPath;
            }

            public SubscriptionModel(Blog blog)
            {
                SubscriptionID = null;
                Existed = false;
                SubsciptionObjectID = blog.ID;
                ObjectName = blog.Name;
                ImagePath = blog.SmallLogoPath;
            }

            public SubscriptionModel(Program program)
            {
                SubscriptionID = null;
                Existed = false;
                SubsciptionObjectID = program.ID;
                ObjectName = program.Name;
                ImagePath = program.SmallLogoPath;
            }

            // general info for subscription
            public long? SubscriptionID { get; set; }
            public bool Existed { get; set; }

            // information for program or blog
            public int SubsciptionObjectID { get; set; }
            public string ObjectName { get; set; }
            public string ImagePath { get; set; }
        }
    }
}