﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.PageModels
{
    public class TagShowModel
    {
        public TagShowModel(int _id, int _ordinalNumber, string _name, int _level, string _parentTag, List<string> _subTags)
        {
            ID = _id;
            OrdinalNumber = _ordinalNumber;
            Name = _name;
            Level = _level;
            ParentTag = _parentTag;
            SubTags = _subTags;
        }

        public int ID { get; set; }
        public int OrdinalNumber { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public string ParentTag { get; set; }
        public List<string> SubTags { get; set; }
    }
}