﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PascalWeb.DB.AuxiliaryModels;
using PascalWeb.Models;

namespace PascalWeb.DB.PageModels
{
    public class UserUnitedModel
    {
        public UserUnitedModel()
        {
            UserPrograms = new List<KeyValuePair<Program, bool?>>();
            UserProgramEditions = null;
            UserBlogRecords = null;
        }

        public UserUnitedModel(UserAuth userAuth)
            : this()
        {

            UserAuth = userAuth;
        }

        public UserUnitedModel(UserAuth userAuth, UserInfo userInfo)
            : this()
        {
            UserAuth = userAuth;
            UserInfo = userInfo;
        }

        //public UserUnitedModel(UserAuth userAuth, UserInfo userInfo, RegisterModel registerModel)
        //    : this()
        //{
        //    UserAuth = userAuth;
        //    UserInfo = userInfo;
        //    RegisterModel = registerModel;
        //}

        public void setUserProgramEditions(PageWithPagination<LastProgramEditionPageModel> userProgramEditions)
        {
            UserProgramEditions = userProgramEditions;
        }

        public void setUserBlogRecords(List<BlogRecord> userBlogRecords)
        {
            UserBlogRecords = userBlogRecords;
        }

        public UserAuth UserAuth { get; set; }
        public UserInfo UserInfo { get; set; }


        //public RegisterModel RegisterModel { get; set; }

        // programs with check of subscription
        public List<KeyValuePair<Program, bool?>> UserPrograms { get; set; }
        public PageWithPagination<LastProgramEditionPageModel> UserProgramEditions { get; set; }
        public List<BlogRecord> UserBlogRecords { get; set; }
    }
}