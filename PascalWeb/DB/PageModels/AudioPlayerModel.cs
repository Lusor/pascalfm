﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.PageModels
{

    public class AudioPlayerModel
    {
        public AudioPlayerModel()
        {
//            StartImmediatelyPlaying = true;
            Elements = new List<AudioElement>();
        }
        public AudioElement FirstElement { get; set; }
        public List<AudioElement> Elements { get; set; }

//        public bool StartImmediatelyPlaying { get; set; }

        public class AudioElement
        {
            public long ProgramEditionID;
            public string ProgramName { get; set; }
            public string ProgramEditionName {get; set;}
            public string ResourceLink {get; set;}
        }
    }
}