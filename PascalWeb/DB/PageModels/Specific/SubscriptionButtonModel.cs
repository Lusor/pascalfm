﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.PageModels.Specific
{
    public class SubscriptionButtonModel
    {
        public SubscriptionButtonModel(int objectID, bool? existed, bool changeToSubscibtionIndication)
        {
            ObjectID = objectID;
            Existed = existed;
            ChangeToSubscibtionIndication = changeToSubscibtionIndication;
        }

        public int ObjectID;
        public bool? Existed;
        public bool ChangeToSubscibtionIndication;
    }
}