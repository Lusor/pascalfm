﻿using PascalWeb.DB.AuxiliaryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.PageModels.Specific
{
    public class UserSearchModel
    {
        public UserSearchModel(string userData, int? roleID)
        {
            RoleID = roleID;
            UserData = userData;
        }

        // role (optional)
        public int? RoleID { get; set; }
        // usually name, surname, email
        public string UserData { get; set; }

        // result
        public IEnumerable<UserInfoModel> UserList { get; set; }
    }
}