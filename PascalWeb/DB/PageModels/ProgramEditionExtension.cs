﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.PageModels
{
    public class ProgramEditionExtension
    {
        public ProgramEditionExtension(ProgramEdition _edition)
        {
            Edition = _edition;
            FullFileLink = "";
            if (_edition != null && _edition.Content != null)
            {
                FullFileLink = _edition.Content.AbsoluteFileUri;
            }
        }

        public ProgramEdition Edition { get; set; }
        public string FullFileLink { get; set; }

        // authors
    }
}