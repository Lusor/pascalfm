﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.PageModels
{
    public class CommonCommentViewModel
    {
        public CommonCommentViewModel(long _commentID, UserAuth _userAuth, DateTime _commentDatetime, string _commentText, bool canEdit, string controllerName)
        {
            UserAuth =_userAuth;

            if (_userAuth != null)
            {
                UserName = _userAuth.Name;

            }
            else
            {
                UserName = null;
            }
            CanEdit = canEdit;
            CommentID = _commentID;
            CommentText = _commentText;
            CommentDateTime = GeneralMethods.getFormatedDateAndTime(_commentDatetime);
            ControllerName = controllerName;

        }

        public string UserName { get; set; }
        public string FullUserName { get; set; }
        public UserAuth UserAuth { get; set; }
        
        public long CommentID { get; set; }
        public string CommentText { get; set; }
        public string CommentDateTime  { get; set; }

        //
        public bool CanEdit  { get; set; }
        public string ControllerName { get; set; }

    }
}