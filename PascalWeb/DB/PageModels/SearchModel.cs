﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.PageModels
{
    public class SearchModel
    {
        public string Query { get; set; }
        public bool IsHashTag { get; set; }
        public bool IsSearchInPrograms { get; set; }
        public bool IsSearchInBlogs{ get; set; }

        public SearchResult.SearchResultType SearchType { get; set; }
        public string InfoResultString { get; set; }
        public int NumberOfResults { get; set; }
        public List<SearchResult> SearchResultList { get; set; }
    }



    public class SearchResult
    {
        public enum SearchResultType { All, ProgramEdition, Blog }

        public SearchResultType ResultType { get; set; }


        public DateTime PublishTime { get; set; }
        public string ImagePath { get; set; }
        public string Description { get; set; }


        // special
        public string RecordName { get; set; }
        public long RecordID { get; set; }
        // can be BolgID or ProgramID depending on the ResultType
        public string ParentName { get; set; }
        public int ParentID { get; set; }
    }
}