﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PascalWeb.DB.AuxiliaryModels;

namespace PascalWeb.DB.PageModels
{
    public class ProgramEditPageModel
    {
        public ProgramEditPageModel()
        {
            Program = new Program();
            AuthorList = new List<UserAuth>();
            TagList = new List<Tag>();
        }

        public ProgramEditPageModel(Program _program, List<UserAuth> authorList, List<Tag> tagNames)
        {
            Program = _program;
            AuthorList = authorList;
            TagList = tagNames;
         }

        public Program Program { get; set; }
        public List<UserAuth> AuthorList { get; set; }
        public List<Tag> TagList { get; set; }
    }
}