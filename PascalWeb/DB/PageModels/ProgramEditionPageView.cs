﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.PageModels
{
    public class ProgramEditionPageView
    {
        public ProgramEditionPageView()
        {
            ProgramEdition = new ProgramEdition();
           // ProgramEditionText = new DB.ProgramEditionText();
            Comments = new CommentBoxModel();
            Recommendation = new LastProgramEditionPageModel();
        }

        public ProgramEdition ProgramEdition { get; set; }
    //    public ProgramEditionText ProgramEditionText { get; set; }

        public CommentBoxModel Comments { get; set; }
        public LastProgramEditionPageModel Recommendation { get; set; }
    }
}