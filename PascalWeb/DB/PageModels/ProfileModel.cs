﻿using PascalWeb.DB.AuxiliaryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.PageModels
{
    public class ProfileModel
    {
        public ProfileModel()
        {

        }

        public int? BlogID { get; set; }

        public long UserID { get; set; }
        public string RoleName { get; set; }
        public bool IsExternalAccount { get; set; }

        public ExpandingContainerModel<List<LastProgramEditionPageModel.LastProgramEdition>> SubscribedProgramEditions{ get; set; }
        public ExpandingContainerModel<List<BlogRecord>> SubscribedBlogRecords { get; set; }
    }
}