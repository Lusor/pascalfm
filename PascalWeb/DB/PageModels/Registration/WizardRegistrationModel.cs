﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PascalWeb.DB.PageModels.Registration
{
    public class WizardRegistrationModel
    {
        public WizardRegistrationModel()
        {
            Step1 = new RegistrationStepOneModel();
            Step2 = new RegistrationStepTwoModel();
        }

        [HiddenInput]
        public RegistrationStepOneModel Step1 { get; set; }

        public RegistrationStepTwoModel Step2 { get; set; }


    }
}