﻿using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.PageModels.Specific
{
    public class RestorePasswordModel
    {
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(ValidationMessagesRu))]
        [EmailAddress(ErrorMessage = "Поле '{0}' заполненно некорректно")]
        [StringLength(50)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        //[EmailAddress(ErrorMessage = "Поле '{0}' заполненно некорректно")]
        //[StringLength(50)]
        //[Display(Name = "Email")]
        //public string Email { get; set; }
    }
}