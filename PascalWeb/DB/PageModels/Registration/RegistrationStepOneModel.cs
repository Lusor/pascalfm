﻿using PascalWeb.Additional.Validation;
using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PascalWeb.DB.PageModels.Registration
{
    [Serializable]
    public class RegistrationStepOneModel
    {
        public RegistrationStepOneModel()
        {
            PasswordObject = new PasswordInput();
            CaptchaObject = new CaptchaInput();
        }

        // regex is provided by https://regex101.com/r/yR0bA3/2

        //[Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(ValidationMessagesRu))]
        //[StringLength(30, ErrorMessage = "Поле '{0}' должен содержать не менее {2} символов", MinimumLength = 6)]
        //[RegularExpression(@"^(?!.*(-[^-]*-|_[^_]*_))[A-Za-z0-9][\w-]*[A-Za-z0-9]$", ErrorMessage = "Недопустимые символы при заполнении поля '{0}'")]   // @"^[a-zA-Z0-9][a-zA-Z0-9_-]*$"
        //[Display(Name = "Логин")]
        //public string Login { get; set; }

        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(ValidationMessagesRu))]
        [EmailAddress(ErrorMessage = "Поле '{0}' заполненно некорректно")]
        [StringLength(50)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        public PasswordInput PasswordObject { get; set; }

        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(ValidationMessagesRu))]
        [RegularExpression(@"^[а-яА-ЯёЁa-zA-Z0-9]+$", ErrorMessage = "Недопустимые символы при заполнении поля '{0}'")]   // @"^[a-zA-Z0-9][a-zA-Z0-9_-]*$"
        [StringLength(50, ErrorMessage = "Поле '{0}' должно содержать не менее {2} символов", MinimumLength = 3)]
        [Display(Name = "Имя")]
        public string UserName { get; set; }

        [StringLength(50)]
        [Display(Name = "Фамилия")]
        public string UserSurname { get; set; }

        [Display(Name = "Дата рождения")]
        public System.DateTime? Birthdate { get; set; }

        public CaptchaInput CaptchaObject { get; set; }

        public byte[] Image  { get; set; }
        public string ImageType { get; set; }


        public bool IsActivated  { get; set; }
        public bool IsLocked  { get; set; }
        public int UserRoleID  { get; set; }

        // hidden
        [HiddenInput]
        public long? ID { get; set; }
        public string SmallLogoPath { get; set; }


        public class PasswordInput
        {
            [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(ValidationMessagesRu))]
            [StringLength(100, ErrorMessage = "Поле '{0}' должно содержать не менее {2} символов", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Пароль")]
            public string Password { get; set; }

            [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(ValidationMessagesRu))]
            [DataType(DataType.Password)]
            [Display(Name = "Подтверждение пароля")]
            [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Введенные пароли не совпадают")]
            public string PasswordConfirmation { get; set; }
        }

        public class CaptchaInput
        {
            [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(ValidationMessagesRu))]
            [Display(Name = "Код верификации")]
            public string CaptchaEnterCode { get; set; }

            public string CaptchaImageString { get; set; }

            [CheckboxValidation(ErrorMessage = "Необходимо дать согласие на обработку персональных данных")]
            [Display(Name = "Согласие на обработку данных")]
            public bool ConfirmationOfDataProcession { get; set; }
        }
    }
}