﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.PageModels.Registration
{
    public class ChangePasswordModel
    {
        public ChangePasswordModel()
        {
            PasswordObject = new RegistrationStepOneModel.PasswordInput();
        }

        public RegistrationStepOneModel.PasswordInput PasswordObject { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        public long? UserID { get; set; }

    }
}