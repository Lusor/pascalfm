﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.PageModels.Registration
{
    [Serializable]
    public class RegistrationStepTwoModel
    {
        [Display(Name = "Семейное положение")]
        public int? MaritalStatusID { get; set; }

        [Display(Name = "Образование")]
        public int? EducationStatusID { get; set; }

        [Display(Name = "Род занятий")]
        public int? UserOccupationID { get; set; }

        [Display(Name = "Пол")]
        public int? GenderIndex { get; set; }

        [Display(Name = "Интересы (перечислите через запятую)")]
        public string UserInterests { get; set; }


    }
}