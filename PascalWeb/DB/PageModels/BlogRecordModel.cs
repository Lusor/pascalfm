﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.PageModels
{
    public class BlogRecordModel
    {
        public BlogRecordModel(BlogRecord _blogRecord, CommentBoxModel _comments)
        {
            BlogRecord = _blogRecord;
            Comments = _comments;
         }

        public BlogRecord BlogRecord { get; set; }
        public CommentBoxModel Comments { get; set; }
    }
}