﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.PageModels
{
    public class AudioTransferData
    {
        public AudioTransferData(long editionID, string programName, string programEditionName, string resourceLink)
        {
            EditionID = editionID;
            ProgramName = programName;
            ProgramEditionName = programEditionName;
            ResourceLink = resourceLink;
        }

        public AudioTransferData(ProgramEdition edition)
        {
            EditionID = edition.ID;
            ProgramName = edition.Program.Name;
            ProgramEditionName = edition.EditionTittle;
            ResourceLink = edition.Content.AbsoluteFileUri;
        }

        public long EditionID { set; get; }
        public string ProgramName { set; get; }
        public string ProgramEditionName { set; get; }
        public string ResourceLink { set; get; }
    }
}