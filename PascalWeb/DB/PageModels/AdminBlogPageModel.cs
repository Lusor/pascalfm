﻿using PascalWeb.DB.AuxiliaryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.PageModels
{
    public class AdminBlogPageModel
    {
        public AdminBlogPageModel()
        {
            Blog = null;
            TableData = null;
            ThemeList = new List<ListElementWithChildren<Blog>>();
            PopularAuthorList = new List<UserInfoModel>();
        }

        //public AdminBlogPageModel(Blog blog, PageWithPagination<IEnumerable<BlogRecord>> tableData, List<ListElementWithChildren<Blog>> themeList, IEnumerable<Blog> blogRankingList)
        //{
        //    Blog = blog;
        //    TableData = tableData;

        //    ThemeList = themeList;
        //    BlogRankingList = blogRankingList;
        //}

        public Blog Blog;
        public bool? UserIsSubscribed { get; set; }
        public PageWithPagination<IEnumerable<BlogRecord>> TableData;
        public IEnumerable<Blog> BlogRankingList;

        public List<ListElementWithChildren<Blog>> ThemeList;
        public List<UserInfoModel> PopularAuthorList;


    }
}