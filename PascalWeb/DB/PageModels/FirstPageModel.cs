﻿using PascalWeb.DB.AuxiliaryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.PageModels
{
    public class FirstPageModel
    {
        public FirstPageModel()
        {
//            TopProgramEditions = new List<LastProgramEditionPageModel.LastProgramEdition>();
//            LastProgramEditions = new List<LastProgramEditionPageModel.LastProgramEdition>();
        }

        public FirstPageModel(ExpandingContainerModel<List<LastProgramEditionPageModel.LastProgramEdition>> lastProgramEditions = null,
            ExpandingContainerModel<List<BlogRecord>> lastBlogRecords = null)
        {
            //TopProgramEditions = topProgramEditions;
            //LastProgramEditions = lastProgramEditions;

            LastProgramEditions = lastProgramEditions;
            LastBlogRecords = lastBlogRecords;
        }


//        public List<LastProgramEditionPageModel.LastProgramEdition> TopProgramEditions { get; set; }
//        public List<LastProgramEditionPageModel.LastProgramEdition> LastProgramEditions { get; set; }

        public ExpandingContainerModel<List<LastProgramEditionPageModel.LastProgramEdition>> LastProgramEditions { get; set; }
        public ExpandingContainerModel<List<BlogRecord>> LastBlogRecords { get; set; }

        // themes
        public List<PascalWeb.DB.ProgramTheme> ThemeList { get; set; }

        // popular authors
        public List<UserInfoModel> PopularAuthorList { get; set; }

        public int? ChosenThemeID { get; set; }
        public long? ChosenAuthorID { get; set; }
    }
}