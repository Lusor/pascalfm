﻿using PascalWeb.DB.AuxiliaryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.PageModels
{

    public class ProgramThemePageModel
    {
        public ProgramThemePageModel(List<ListElementWithChildren<Program>> themeWithProgramsList, List<UserInfoModel> popularAuthorList, IEnumerable<Program> popularProgramList)
        {
            ThemeWithProgramsList = themeWithProgramsList;
            PopularProgramList = popularProgramList;
            PopularAuthorList = popularAuthorList;
        }

        public ProgramThemePageModel(List<ListElementWithChildren<Program>> themeWithProgramsList, List<UserInfoModel> popularAuthorList, ProgramPageModel chosenProgram)
        {
            ThemeWithProgramsList = themeWithProgramsList;
            PopularAuthorList = popularAuthorList;
            ChosenProgram = chosenProgram;
        }

        public List<ListElementWithChildren<Program>> ThemeWithProgramsList { get; set; }

        // chose either PopularProgramList or ChosenProgram
        public IEnumerable<Program> PopularProgramList { get; set; }
        // popular authors
        public List<UserInfoModel> PopularAuthorList { get; set; }

        public ProgramPageModel ChosenProgram { get; set; }

    }

}