﻿using PascalWeb.DB.AuxiliaryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.PageModels
{
    public class ProgramPageModel
    {
        public ProgramPageModel()
        {
            Program = null;
            LastProgramEditions = null;
        }

        public ProgramPageModel(Program program, PageWithPagination<LastProgramEditionPageModel> lastProgramEditions)
        {
            Program = program;
            LastProgramEditions = lastProgramEditions;
        }

        public Program Program { get; set; }

        public PageWithPagination<LastProgramEditionPageModel> LastProgramEditions { get; set; }
                                  
        public LastProgramEditionPageModel RecommendedProgramEditions { get; set; }

        public bool? UserIsSubscribed { get; set; }
    }
}