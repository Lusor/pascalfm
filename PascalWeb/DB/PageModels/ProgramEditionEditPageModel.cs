﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.PageModels
{
    public class ProgramEditionEditPageModel
    {
        public ProgramEditionEditPageModel()
        {
            ProgramEdition = new ProgramEdition();
            ProgramEditionText = new DB.ProgramEditionText();
            AuthorList = new List<UserAuth>();
            TagList = new List<Tag>();
        }

        public ProgramEditionEditPageModel(ProgramEdition programEdition, ProgramEditionText programEditionText, List<UserAuth> authorList, List<Tag> tagNames)
        {
            ProgramEdition = programEdition;
            ProgramEditionText = programEditionText;
            AuthorList = authorList;
            TagList = tagNames;
        }

        public ProgramEdition ProgramEdition { get; set; }
        public ProgramEditionText ProgramEditionText { get; set; }
        public List<UserAuth> AuthorList { get; set; }
        public List<Tag> TagList { get; set; }
    }
}