﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Storage.Blob;
using PascalWeb.Additional.Storage;
using PascalWeb.DB.Repositories;

namespace PascalWeb.DB
{
    public class cContentManager
    {
        //PascalDBEntities context = null;

        public static int? getContenTypeIDByName(UnitOfWork uow, string contentTypeName)
        {
            if (contentTypeName == null)
                return null;

            contentTypeName = contentTypeName.ToLower();
            var contentType = uow.ContentTypeRepository.Get(filter: obj => obj.Name.CompareTo(contentTypeName) == 0).FirstOrDefault();
            //id = (from contentType in context.ContentType
            //      where contentType.Name.CompareTo(contentTypeName) == 0
            //      select contentType.ID).First();
            return contentType != null ? contentType.ID : new int?();
        }

        public static bool saveAudioContent(UnitOfWork uow, HttpPostedFileBase file, long userID, DateTime creationDate, ref DB.Content content)
        {
            cStorageManager.eStorageContainerName containerName = cStorageManager.eStorageContainerName.audio;
            return saveContent(uow, file, containerName.ToString(), userID, creationDate, ref content);
        }

        private static bool saveContent(UnitOfWork uow, HttpPostedFileBase file, string containerName, long userID, DateTime creationDate, ref DB.Content content)
        {
            Guid fileGuid = Guid.NewGuid();
            string uniqueName = null;
            string hostName = null;
            string localPath = null;
            string absoluteUri = null;

            bool storageConnected = cStorageManager.getConnected();
            var container = cStorageManager.createContainer(containerName);
            uniqueName = cStorageManager.generateBlobName(container, fileGuid, cStorageManager.eEntityStorageName.programEdition);
            var uniqueNameWithExtension = GeneralMethods.concatenateStrings(uniqueName, Path.GetExtension(file.FileName));
            CloudBlockBlob blob = null;
            bool saved = cStorageManager.addBlob(file.InputStream, file.ContentType, container, uniqueNameWithExtension, out blob);
          
            if (!saved)
                return saved;

            hostName = blob.StorageUri.PrimaryUri.Host;
            localPath = blob.StorageUri.PrimaryUri.LocalPath;
            absoluteUri = blob.StorageUri.PrimaryUri.AbsoluteUri;


            content = new DB.Content();
            content.AuthorID = userID;
            content.CreationDate = creationDate;
            content.ContentTypeID = cContentManager.getContenTypeIDByName(uow, containerName).Value;
            content.GeneratedName = fileGuid.ToString();

            content.OriginalFileName = Path.GetFileName(file.FileName);
            if (content.OriginalFileName != null && content.OriginalFileName.Length > 50)
                content.OriginalFileName = content.OriginalFileName.Substring(0, 50);
            content.Extension = Path.GetExtension(file.FileName);
            content.FileSize = file.ContentLength;
            content.Storage = hostName;
            content.FileName = uniqueName;
            content.AbsoluteFileUri = absoluteUri;

            return true;
        }

        public static bool deleteAudioContent(UnitOfWork uow, long contentID)
        {
            return deleteContent(uow, "audio", contentID);
        }

        private static bool deleteContent(UnitOfWork uow, string contanerName, long contentID)
        {
            try
            {
                Content content = uow.ContentRepository.GetByID(contentID);
                if (content != null)
                {
                    cStorageManager.deleteBlob(contanerName, content.FileName);
                    uow.ContentRepository.Delete(content);
                    uow.Save();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}