﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.Enumerators
{
    public enum eEmailDispatchType { email_confirmation, restore_password, other };
}