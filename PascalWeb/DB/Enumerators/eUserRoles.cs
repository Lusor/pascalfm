﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.Enumerators
{
    public enum eUserRoles { superadmin, admin, journalist, generaluser, withoutauth, moderator, editor };
}