//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PascalWeb.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserRight
    {
        public int ID { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public Nullable<int> UserRoleID { get; set; }
        public bool IsAvailable { get; set; }
        public bool ToLoginPage { get; set; }
        public bool WithoutRole { get; set; }
    
        public virtual UserRole UserRole { get; set; }
    }
}
