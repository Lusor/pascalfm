//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PascalWeb.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class StatisticsSlice
    {
        public long ID { get; set; }
        public System.DateTime DateStamp { get; set; }
        public int EntityType { get; set; }
        public Nullable<long> EntityID { get; set; }
        public Nullable<long> ViewedCounter { get; set; }
        public Nullable<long> ListenedCounter { get; set; }
        public Nullable<long> DownloadCounter { get; set; }
        public Nullable<long> CommentsCounter { get; set; }
    }
}
