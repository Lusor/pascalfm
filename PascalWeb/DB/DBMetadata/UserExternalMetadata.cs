﻿using PascalWeb.DB.DBMetadata;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace PascalWeb.DB.DBMetadata
{
    internal sealed class UserExternalMetadata
    {
        [StringLength(50)]
        public string Birthday { get; set; }
    }
}

namespace PascalWeb.DB
{
    [MetadataType(typeof(UserExternalMetadata))]
    public partial class UserExternal
    {

    }
}

