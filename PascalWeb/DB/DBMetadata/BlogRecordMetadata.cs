﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PascalWeb.DB.DBMetadata;

namespace PascalWeb.DB.DBMetadata
{
    internal sealed class BlogRecordMetadata
    {
        [Required]
        [DataType(DataType.DateTime)]
        [Editable(false)]
        [Display(Name = "Дата создания")]
        public System.DateTime CreationDate { get; set; }

        [Required]
        [Display(Name = "Название")]
        [StringLength(200)]
        public string Title { get; set; }

        [Display(Name = "Фрагмент (короткое описание)")]
        //[StringLength(200)]
        public string NoteFragment { get; set; }

        [Display(Name = "Содержимое записи")]
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string Note { get; set; }

    }
}

namespace PascalWeb.DB
{
    [MetadataType(typeof(BlogRecordMetadata))]
    public partial class BlogRecord
    {

    }
}
