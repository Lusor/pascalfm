﻿using PascalWeb.DB.DBMetadata;
using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.DBMetadata
{
    internal sealed class UserInfoMetadata
    {
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(ValidationMessagesRu))]
        [Display(Name = "Дата рождения")]
        public DateTime? Birthdate { get; set; }

        [Display(Name = "Образование")]
        public int EducationStatusID { get; set; }

        [Display(Name = "Семейный статус")]
        public int MaritalStatusID { get; set; }

        [Display(Name = "Пол")]
        public bool GenderIsMale { get; set; }

    }
}

namespace PascalWeb.DB
{
    [MetadataType(typeof(UserInfoMetadata))]
    public partial class UserInfo
    {

    }
}

