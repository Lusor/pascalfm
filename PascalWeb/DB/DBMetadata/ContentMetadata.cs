﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.DBMetadata
{
    internal sealed class ContentMetadata
    {
        [Required]
        [Display(Name = "Название")]
        [StringLength(50)]
        public string OriginalFileName { get; set; }
    }

    namespace PascalWeb.DB
    {
        [MetadataType(typeof(ProgramEditionMetadata))]
        public partial class Content
        {

        }
    }
}