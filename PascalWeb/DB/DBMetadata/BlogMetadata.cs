﻿using PascalWeb.DB.DBMetadata;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PascalWeb.DB.DBMetadata
{
    internal sealed class BlogMetadata
    {
        [Required]
        [DataType(DataType.DateTime)]
        [Editable(false)]
        [Display(Name = "Дата создания")]
        public System.DateTime CreationDate { get; set; }

        [Required]
        [Display(Name = "Название")]
        [StringLength(30)]
        public string Name { get; set; }

        [Display(Name = "Описание")]
        [StringLength(1000)]
        public string Description { get; set; }
    }
}

namespace PascalWeb.DB
{
    [MetadataType(typeof(BlogMetadata))]
    public partial class Blog
    {

    }
}
