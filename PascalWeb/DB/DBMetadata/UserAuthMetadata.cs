﻿using PascalWeb.DB.DBMetadata;
using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PascalWeb.DB.DBMetadata
{
    internal sealed class UserAuthMetadata
    {
        //[Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(ValidationMessagesRu))]
        //[StringLength(30, MinimumLength = 6)]
        //[Display(Name = "Логин")]
        //public string Login { get; set; }

        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(ValidationMessagesRu))]
        [EmailAddress]
        [StringLength(50)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [StringLength(50)]
        [Display(Name = "Фамилия")]
        public string Surname { get; set; }

        [StringLength(50)]
        [Display(Name = "Имя")]
        public string Name { get; set; }


        [Display(Name = "Роль")]
        public int UserRoleID { get; set; }

        [Display(Name = "Активация аккаунта")]
        public bool IsActivated { get; set; }

        [Display(Name = "Блокировка аккаунта")]
        public bool IsLocked { get; set; }
    }
}

namespace PascalWeb.DB
{
    [MetadataType(typeof(UserAuthMetadata))]
    public partial class UserAuth
    {

    }
}

