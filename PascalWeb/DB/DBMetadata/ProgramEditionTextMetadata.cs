﻿using PascalWeb.DB.DBMetadata;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PascalWeb.DB.DBMetadata
{
    internal sealed class ProgramEditionTextMetadata
    {
        [Display(Name = "Полный текст")]
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string Note { get; set; }

    }
}

namespace PascalWeb.DB
{
    [MetadataType(typeof(ProgramEditionTextMetadata))]
    public partial class ProgramEditionText
    {

    }
}