﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using PascalWeb.DB.DBMetadata;

namespace PascalWeb.DB.DBMetadata
{
    internal sealed class ProgramMetadata
    {
        [Required]
        [DataType(DataType.DateTime)]
        [Editable(false)]
        [Display(Name = "Дата создания")]
        public System.DateTime CreationDate { get; set; }

        [Required]
        [Display(Name = "Название")]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Тема подкаста")]
        public string ThemeID { get; set; }

        [Display(Name = "Описание")]
        [StringLength(2000)]
        public string Description { get; set; }

        [Editable(false)]
        public byte[] SmallLogo { get; set; }
        [Editable(false)]
        public string LogoContentType { get; set; }
    }
}

namespace PascalWeb.DB
{
    [MetadataType(typeof(ProgramMetadata))]
    public partial class Program
    {

    }
}