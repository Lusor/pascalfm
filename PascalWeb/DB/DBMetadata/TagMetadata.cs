﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using PascalWeb.DB.DBMetadata;

namespace PascalWeb.DB.DBMetadata
{
    internal sealed class TagMetadata
    {
        [Required]
        [StringLength(20)]
        [Display(Name = "Название")]
        public string Name { get; set; }

        [Required]
        //[Range(1, 4)]
        [Display(Name = "Уровень")]
        public int Level { get; set; }

        [Display(Name = "Тэг предыдущего уровня")]
        public Nullable<int> ParentTagID { get; set; }

    }
}

namespace PascalWeb.DB
{
    [MetadataType(typeof(TagMetadata))]
    public partial class Tag
    {

    }
}
