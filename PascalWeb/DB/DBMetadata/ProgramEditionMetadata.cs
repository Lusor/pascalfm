﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using PascalWeb.DB.DBMetadata;
using PascalWeb.Additional;

namespace PascalWeb.DB.DBMetadata
{
    internal sealed class ProgramEditionMetadata
    {
        [Required]
        [Display(Name = "Дата создания")]
        [DataType(DataType.DateTime)]
        public System.DateTime CreationDate { get; set; }

        [Required]
        [Display(Name = "Подкаст")]
        public int ProgramID { get; set; }

        [Required]
        [DatetimeAttribute]
        [Display(Name = "Дата выпуска в эфир")]
        public DateTime AirDate { get; set; }

        [Required]        
        [StringLength(30)]
        [Display(Name = "Название выпуска")]
        public string EditionTittle { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Краткое описание выпуска")]
        public string ShortDescription { get; set; }

        [Required]
        [Range(0,36000)]
        [Display(Name = "Длительность (в секундах)")]
        public int? Duration { get; set; }

    }
}


namespace PascalWeb.DB
{
    [MetadataType(typeof(ProgramEditionMetadata))]
    public partial class ProgramEdition
    {

    }
}