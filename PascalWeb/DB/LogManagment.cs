﻿using PascalWeb.DB.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.DB
{
    public class LogManagment
    {
        public static void addRequestToLog(UnitOfWork unitOfWork, string ip, string url, string urlReferrer, long? userID)
        {
            ip = checkAndShrinkString(ip, 50);
            url = checkAndShrinkString(url, 100);
            urlReferrer = checkAndShrinkString(urlReferrer, 100);

            LogRequest request = new LogRequest();
            request.CreationDate = DateTime.Now;
            request.IP = ip;
            request.URL = url;
            request.UserID = userID;
            request.UrlReferrer = urlReferrer;
            unitOfWork.LogRequestRepository.Insert(request);
            unitOfWork.Save();
        }

        public static void addRequestToLog(UnitOfWork unitOfWork, string ip, string url, string urlReferrer, string browser, string additionalInfo, long? userID, bool notUser)
        {
            ip = checkAndShrinkString(ip, 50);
            url = checkAndShrinkString(url, 100);
            urlReferrer = checkAndShrinkString(urlReferrer, 100);

            LogRequest request = new LogRequest();
            request.CreationDate = DateTime.Now;
            request.IP = ip;
            request.URL = url;
            request.UserID = userID;
            request.UrlReferrer = urlReferrer;
            request.BrowserType = browser;
            request.AdditionalInfo = additionalInfo;
            request.NotUser = notUser;
            unitOfWork.LogRequestRepository.Insert(request);
            unitOfWork.Save();
        }

        /// <summary>
        /// check and shrink string
        /// </summary>
        /// <param name="str"></param>
        /// <param name="characterNumber"></param>
        /// <returns></returns>
        public static string checkAndShrinkString(string str, int characterNumber)
        {
            if (str == null)
                return null;

            if (str.Length > characterNumber)
            {
                return str.Substring(0, characterNumber);
            }
            else
                return str;
        }

        /// <summary>
        /// get host name with port from url
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public static string extractHostNameWithPort(HttpRequestBase req)
        {
            var uri = req.Url;
            string cleanURL = uri.Scheme + "://" + uri.GetComponents(UriComponents.HostAndPort, UriFormat.UriEscaped);

            return cleanURL;
        }

        /// <summary>
        /// get host name from url
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public static string extractHostName(HttpRequestBase req)
        {
            var uri = req.Url;
            string cleanURL = uri.Scheme + "://" + uri.GetComponents(UriComponents.Host, UriFormat.UriEscaped);

            return cleanURL;
        }

        public static bool isUserRequest(HttpRequestBase request)
        {
            // check whether request was done not by real user
            if (request.Browser != null)
            {
                string browserVersion = request.Browser.Version == null ? "" : request.Browser.Version.Trim();
                if (GeneralMethods.compareStrings(browserVersion, "0.0"))
                    return false;

            }
            return true;
        }
    }
}