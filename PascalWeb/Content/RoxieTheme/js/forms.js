var Forms = function () {
    var confiq = {
        regexp: /^[A-z0-9._-]+@[A-z0-9.-]+\.[A-z]{2,4}$/,
        field: $('.js-require'),
        parent: $('.js-step-active'),
        btn: $('.js-btn'),
        error_class: 'js-err-field'
    }
    
    var self = this,
        valid = false;
    
    //валидируем обязательные поля
    var checkVal = function() {
        var vals = [],
            elems = confiq.parent.find(confiq.field);
        $.each(elems, function(){
            var type = $(this).data('type');
            
            if ( type == 'email' ) {
                var pattern = confiq.regexp;
                if ( pattern.test($(this).val()) ) {
                    vals.push($(this).val());
                } else {
                    $(this).addClass(confiq.error_class);
                }
            } else {
                if ( $(this).val() != '' ) {
                    vals.push($(this).val());
                } else {
                    $(this).addClass(confiq.error_class);
                }
            }
            
        });
        if ( vals.length == confiq.field.length ) {
            valid = true;
        }
        
        return valid;
    }
    
    //сохраняем промежуточный результат в sessionStorage
    var saveData = function() {
        $.each(confiq.field, function(){
            sessionStorage.setItem($(this).attr('name'), $(this).val());
        });
    }
    
    //активируем следущий шаг
    var next = function() {
        if ( checkVal() ) {
            saveData();
            confiq.parent.addClass('b-reg-form_unactive').removeClass('b-reg-form_active').next().addClass('b-reg-form_active animated bounceInLeft').removeClass('b-reg-form_unactive');
        }
    }
    
    //переходим к предыдущему
    var prev = function() {
    
    }
    
    var init = function() {
        next();
        console.log(sessionStorage.getItem('name'));
    }
    
    return {
        init: init
    }
}();

$(document).on('click', '.js-btn', function(){
    Forms.init();
});
