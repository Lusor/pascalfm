﻿using FluentScheduler;
using PascalWeb.Additional;
using PascalWeb.Additional.ScheduleTasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace PascalWeb.ExternalLibs.FluentScheduler
{
    public class SchedulerRegistry : Registry
    {
        public SchedulerRegistry()
        {
            // Schedule an ITask to run at an interval

            // update cache for first page
            Schedule<FirstPageUpdateTask>().ToRunNow().AndEvery(180).Seconds();

            // sending emails
            Schedule<EmailSendingTask>().ToRunNow().AndEvery(30).Seconds();

            // recompute recomendation (each 30 min)
            Schedule<PerformRecommendationTask>().ToRunNow().AndEvery(1800).Seconds();


            // recompute recomendation (each 30 min)
            Schedule<StatisticsSliceTask>().ToRunNow().AndEvery(600).Seconds();
        }

    } 
}