﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace PascalWeb.Additional.RSS
{

    // http://weblogs.asp.net/britchie/serving-up-rss-feed-in-mvc-using-wcf-syndication
    // http://castfeedvalidator.com/

    public class RSSFeedResult : ContentResult
    {
        public RSSFeedResult(SyndicationFeed feed)
            : base()
        {
            using (var memstream = new MemoryStream())
            using (var writer = new XmlTextWriter(memstream, System.Text.UTF8Encoding.UTF8))
            {
                feed.SaveAsRss20(writer);
                writer.Flush();
                memstream.Position = 0;
                Content = new StreamReader(memstream).ReadToEnd();
                ContentType = "application/rss+xml";
            }
        }
    }
}