﻿using PascalWeb.Controllers;
using PascalWeb.DB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace PascalWeb.Additional.RSS
{
    public class RSSFeed
    {
        // http://weblogs.asp.net/britchie/creating-a-podcast-feed-for-itunes-amp-blackberry-users-using-wcf-syndication
        // validator - http://feedvalidator.org/

        public static SyndicationFeed createRSSDoc(List<ProgramEdition> programEditionList)
        {
            string title = "Радио Pascal FM";
            string description = "Мы собрали для вас все самое интересное, полезное и познавательное. Наши авторы - лучшие профессионалы в разных областях, которые готовы поделиться своими знаниями и навыками. А ещё, это творческие личности, которые публикуют у нас свои произведения.";
            string webSiteLink = "http://pascal.fm";
            string subTitle = "Pascal FM. Твой эфир. Вовпремя";
            string copyright = "&#x2117; &amp; &#xA9; PascalFM. 2014 - " + DateTime.Now.Year.ToString();
            string ownerName = "Pascal.FM";
            string imageUrl = (new Uri(new Uri("http://pscmaintest.azurewebsites.net"), "Content/RoxieTheme/img/logo/logo_big.jpg")).AbsoluteUri;
            string ownerEmail = "support@pascal.fm";

            string category = "";
            string subCategory = "";

            string mediaType = "audio/mpeg";
            string language = "ru-RU"; // en-us 

            XNamespace itunesNS = "http://www.itunes.com/dtds/podcast-1.0.dtd";

            string prefix = "itunes";

            var feed = new SyndicationFeed(title, description, new Uri(webSiteLink));
            feed.Categories.Add(new SyndicationCategory(category));
            feed.AttributeExtensions.Add(new XmlQualifiedName(prefix, "http://www.w3.org/2000/xmlns/"), itunesNS.NamespaceName);
            feed.Copyright = new TextSyndicationContent(copyright);
            feed.Language =language;

            feed.Copyright = new TextSyndicationContent(DateTime.Now.Year + " " + ownerName);
            feed.ImageUrl = new Uri(imageUrl);
             feed.LastUpdatedTime = DateTime.Now;
            feed.Authors.Add(new SyndicationPerson() {Name=ownerName, Email=ownerEmail });
            var extensions = feed.ElementExtensions;

       //     XElement imageElement = new XElement(itunesNS + "image");
      //      imageElement.SetAttributeValue("href", imageUrl);
           // extensions.Add(imageElement.CreateReader());

            extensions.Add(new XElement(itunesNS + "subtitle", subTitle).CreateReader());
            extensions.Add(new XElement(itunesNS + "image", new XAttribute("href", imageUrl)).CreateReader());
            extensions.Add(new XElement(itunesNS + "author", ownerName).CreateReader());
            extensions.Add(new XElement(itunesNS + "summary", description).CreateReader());

            //extensions.Add(new XElement(itunesNS + "category", new XAttribute("text", "Science & Medicine"), new XElement(itunesNS + "category",
            //    new XAttribute("text", "Natural Sciences"))).CreateReader());
            //extensions.Add(new XElement(itunesNS + "category", new XAttribute("text", "Business"), new XElement(itunesNS + "category",
            //    new XAttribute("text", "Careers"))).CreateReader());
            //extensions.Add(new XElement(itunesNS + "category", new XAttribute("text", "Arts"), new XElement(itunesNS + "category",
            //    new XAttribute("text", "Literature"))).CreateReader());
            //extensions.Add(new XElement(itunesNS + "category", new XAttribute("text", "Education"), new XElement(itunesNS + "category",
            //    new XAttribute("text", "Training"))).CreateReader());

            extensions.Add(new XElement(itunesNS + "category", new XAttribute("text", "Education"), new XElement(itunesNS + "category")).CreateReader());

            extensions.Add(new XElement(itunesNS + "explicit", "no").CreateReader());
            extensions.Add(new XDocument(
                new XElement(itunesNS + "owner",
                new XElement(itunesNS + "name", ownerName),
                new XElement(itunesNS + "email", ownerEmail))).CreateReader());
  
            var feedItems = new List<SyndicationItem>();
            foreach (var edition in programEditionList)
            {
                string editionName = GeneralMethods.concatenateStrings(edition.Program.Name, " - ", edition.EditionTittle);
                string editionLink = (new Uri(new Uri(webSiteLink), "programedition/details/" + edition.ID.ToString())).AbsoluteUri;
                var item = new SyndicationItem(editionName, null, new Uri(editionLink));
                item.Summary = new TextSyndicationContent(edition.ShortDescription);
                item.Id = edition.ID.ToString();
                if (edition.AirDate != null)
                    item.PublishDate = (DateTimeOffset)edition.AirDate;
                item.Links.Add(new SyndicationLink() {
                    Title = editionName,
                    Uri = new Uri(editionLink), 
                    Length = edition.Content.FileSize,
                    MediaType = mediaType
                });
                var itemExt = item.ElementExtensions;
                //itemExt.Add(new XElement(itunesNS + "subtitle", i.subTitle).CreateReader());
                //itemExt.Add(new XElement(itunesNS + "summary", i.summary).CreateReader());

                int hours = edition.Duration.Value / 3600;
                int minutes = edition.Duration.Value / 60;
                int seconds = edition.Duration.Value % 60;
                itemExt.Add(new XElement(itunesNS + "duration", 
                    string.Format("{0}:{1:00}:{2:00}", hours, minutes, seconds)
                    ).CreateReader());

                var tags = TagController.stringToTagList(edition.TagString);
                string keywordString = "";
                for (int n = 0; n < tags.Count; n++)
                {
                    keywordString += tags[n];
                    keywordString += (n != tags.Count - 1) ? "," : "";
                }

                itemExt.Add(new XElement(itunesNS + "summary", edition.ShortDescription).CreateReader());
                itemExt.Add(new XElement(itunesNS + "keywords", keywordString).CreateReader());
                itemExt.Add(new XElement(itunesNS + "explicit", "no").CreateReader());
                itemExt.Add(new XElement(itunesNS + "author", ownerName).CreateReader());

                string absFileString = edition.Content.AbsoluteFileUri;
                absFileString = absFileString.Replace("https", "http");
                itemExt.Add(new XElement("enclosure", new XAttribute("url", absFileString),
                    new XAttribute("length", edition.Content.FileSize), new XAttribute("type", mediaType)));
                    feedItems.Add(item);
                
                feed.Items = feedItems;
            }
            return feed;
        }








        public static SyndicationFeed createRSSDocTEST(List<ProgramEdition> programEditionList)
        {
            string title = "Радио Pascal FM";
            string description = "Мы собрали для вас все самое интересное, полезное и познавательное. Наши авторы - лучшие профессионалы в разных областях, которые готовы поделиться своими знаниями и навыками. А ещё, это творческие личности, которые публикуют у нас свои произведения.";
            string webSiteLink = "http://pascal.fm";
            string subTitle = "Pascal FM. Твой эфир. Вовпремя";
            string copyright = "&#x2117; &amp; &#xA9; PascalFM. 2014 - " + DateTime.Now.Year.ToString();
            string ownerName = "Pascal.FM";
            string imageUrl = (new Uri(new Uri("http://pscmaintest.azurewebsites.net"), "Content/RoxieTheme/img/logo/logo_big.jpg")).AbsoluteUri;
            string ownerEmail = "support@pascal.fm";

            string category = "";
            string subCategory = "";

            string mediaType = "audio/mpeg";
            string language = "ru-RU"; // en-us 

            XNamespace itunesNS = "http://www.itunes.com/dtds/podcast-1.0.dtd";

            string prefix = "itunes";

            var feed = new SyndicationFeed(title, description, new Uri(webSiteLink));
            feed.Categories.Add(new SyndicationCategory(category));
            feed.AttributeExtensions.Add(new XmlQualifiedName(prefix, "http://www.w3.org/2000/xmlns/"), itunesNS.NamespaceName);
            feed.Copyright = new TextSyndicationContent(copyright);
            feed.Language = language;

            feed.Copyright = new TextSyndicationContent(DateTime.Now.Year + " " + ownerName);
            feed.ImageUrl = new Uri(imageUrl);
            feed.LastUpdatedTime = DateTime.Now;
            feed.Authors.Add(new SyndicationPerson() { Name = ownerName, Email = ownerEmail });
            var extensions = feed.ElementExtensions;

            //     XElement imageElement = new XElement(itunesNS + "image");
            //      imageElement.SetAttributeValue("href", imageUrl);
            // extensions.Add(imageElement.CreateReader());

            extensions.Add(new XElement(itunesNS + "subtitle", subTitle).CreateReader());
            extensions.Add(new XElement(itunesNS + "image", new XAttribute("href", imageUrl)).CreateReader());
            extensions.Add(new XElement(itunesNS + "author", ownerName).CreateReader());
            extensions.Add(new XElement(itunesNS + "summary", description).CreateReader());

            //extensions.Add(new XElement(itunesNS + "category", new XAttribute("text", "Science & Medicine"), new XElement(itunesNS + "category",
            //    new XAttribute("text", "Natural Sciences"))).CreateReader());
            //extensions.Add(new XElement(itunesNS + "category", new XAttribute("text", "Business"), new XElement(itunesNS + "category",
            //    new XAttribute("text", "Careers"))).CreateReader());
            //extensions.Add(new XElement(itunesNS + "category", new XAttribute("text", "Arts"), new XElement(itunesNS + "category",
            //    new XAttribute("text", "Literature"))).CreateReader());
//            extensions.Add(new XElement(itunesNS + "category", new XAttribute("text", "Education"), new XElement(itunesNS + "category",
//                new XAttribute("text", "Training"))).CreateReader());

            extensions.Add(new XElement(itunesNS + "category", new XAttribute("text", "Education"), new XElement(itunesNS + "category")).CreateReader());

            extensions.Add(new XElement(itunesNS + "explicit", "no").CreateReader());
            extensions.Add(new XDocument(
                new XElement(itunesNS + "owner",
                new XElement(itunesNS + "name", ownerName),
                new XElement(itunesNS + "email", ownerEmail))).CreateReader());

            var feedItems = new List<SyndicationItem>();
            foreach (var edition in programEditionList)
            {
                string editionName = GeneralMethods.concatenateStrings(edition.Program.Name, " - ", edition.EditionTittle);
                string editionLink = (new Uri(new Uri(webSiteLink), "programedition/details/" + edition.ID.ToString())).AbsoluteUri;
                var item = new SyndicationItem(editionName, null, new Uri(editionLink));
                item.Summary = new TextSyndicationContent(edition.ShortDescription);
                item.Id = edition.ID.ToString();
                if (edition.AirDate != null)
                    item.PublishDate = (DateTimeOffset)edition.AirDate;
                item.Links.Add(new SyndicationLink()
                {
                    Title = editionName,
                    Uri = new Uri(editionLink),
                    Length = edition.Content.FileSize,
                    MediaType = mediaType
                });
                var itemExt = item.ElementExtensions;
                //itemExt.Add(new XElement(itunesNS + "subtitle", i.subTitle).CreateReader());
                //itemExt.Add(new XElement(itunesNS + "summary", i.summary).CreateReader());

                int hours = edition.Duration.Value / 3600;
                int minutes = edition.Duration.Value / 60;
                int seconds = edition.Duration.Value % 60;
                itemExt.Add(new XElement(itunesNS + "duration",
                    string.Format("{0}:{1:00}:{2:00}", hours, minutes, seconds)
                    ).CreateReader());

                var tags = TagController.stringToTagList(edition.TagString);
                string keywordString = "";
                for (int n = 0; n < tags.Count; n++)
                {
                    keywordString += tags[n];
                    keywordString += (n != tags.Count - 1) ? "," : "";
                }

                itemExt.Add(new XElement(itunesNS + "summary", edition.ShortDescription).CreateReader());
                itemExt.Add(new XElement(itunesNS + "keywords", keywordString).CreateReader());
                itemExt.Add(new XElement(itunesNS + "explicit", "no").CreateReader());
                itemExt.Add(new XElement(itunesNS + "author", ownerName).CreateReader());

                //string absFileString = edition.Content.AbsoluteFileUri;

                string absFileString = (new Uri(new Uri(webSiteLink), "audioplayer/Listen/" + edition.ID.ToString())).AbsoluteUri;
                absFileString = absFileString.Replace("https", "http");
                itemExt.Add(new XElement("enclosure", new XAttribute("url", absFileString),
                    new XAttribute("length", edition.Content.FileSize), new XAttribute("type", mediaType)));
                feedItems.Add(item);

                feed.Items = feedItems;
            }
            return feed;
        }
    }
}