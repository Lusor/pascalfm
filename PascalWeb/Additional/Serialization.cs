﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.Additional
{
    public class Serialization
    {
        private static string itemSplitter = "||";

        /// <summary>
        /// write numbers in a string with splitter
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="numberList"></param>
        /// <returns></returns>
        public static string numberListToString<T>(List<T> numberList)
        {
            string numberString = "";
            if (numberList == null)
                return numberString;

            for (int i = 0; i < numberList.Count; i++)
            {
                numberString = string.Concat(numberString, numberList[i].ToString());
                numberString = string.Concat(numberString, itemSplitter);
            }
            return numberString;
        }

        /// <summary>
        /// parse string with numbers to list of numbers
        /// </summary>
        /// <param name="numberString"></param>
        /// <returns></returns>
        public static List<long> stringToLongList(string numberString)
        {
            var numberList = new List<long>();
            if (string.IsNullOrWhiteSpace(numberString))
                return numberList;

            var numberStrList = numberString.Split(new string[] { itemSplitter }, StringSplitOptions.RemoveEmptyEntries).ToList();

            for (int i = 0; i < numberStrList.Count; i++)
            {
                long value;
                if (long.TryParse(numberStrList[i], out value))
                    numberList.Add(value);
            }
            return numberList;
        }
    }
}