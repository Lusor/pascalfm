﻿using PascalWeb.Controllers;
using PascalWeb.DB;
using PascalWeb.DB.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.Additional.Algorithms
{
    public class ProgramEditionCorrelation
    {
        public static float devider = 2.0f;
        public static float threshold = 0.1f;

        private static bool updateProgramEditionRecommendation(UnitOfWork unitOfWork, Dictionary<long, string> programEditionList)
        {
            try
            {
                for (int i = 0; i < programEditionList.Count; i++)
                {
                    ProgramEdition edition = unitOfWork.ProgramEditionRepository.GetByID(programEditionList.Keys.ElementAt(i));
                    string newValue = programEditionList.Values.ElementAt(i);

                    // check whether it is necessary to update pe field
                    if (!GeneralMethods.compareStrings(edition.Recommendations, newValue))
                    {
                        edition.Recommendations = newValue;
                    }
                }

                // TODO modify - it is bad to update many records at one time
                unitOfWork.Save();
                return true;
            }
            catch (Exception exc)
            {
                PascalLogging.Logger.Error("ProgramEditionCorrelation:updateProgramEditionRecommendation", exc);
                return false;
            }
        }

        /// <summary>
        /// cross-comparision of published program editions
        /// </summary>
        /// <param name="unitOfWork"></param>
        public static void correlateAllProgramEditions(UnitOfWork unitOfWork)
        {
            // list of pe for update
            var programEditionListForUpdate = new Dictionary<long, string>();

            // get all editions
            List<long> programEditionIDList = unitOfWork.ProgramEditionRepository.Get(filter: obj => obj.Published == true).Select(s => s.ID).ToList();
            var programEditionsWithVector = new SortedList<long, SortedList<int, float>>();
            // calculate vector for each element
            for (int i = 0; i < programEditionIDList.Count; i++)
            {
                long programEditionID = programEditionIDList[i];
                SortedList<int, float> vector = getVector(unitOfWork, programEditionID);
                programEditionsWithVector.Add(programEditionID, vector);
            }

            // check each element with each other, build ranking for each element
            for (int i = 0; i < programEditionIDList.Count; i++)
            {
                var etalonProgramEditionID = programEditionIDList[i];
                var etalonVector = programEditionsWithVector[etalonProgramEditionID];

                // add special comparer
                var ranking = new SortedList<long, float>();
                for (int j = 0; j < programEditionIDList.Count; j++)
                {
                    if (i == j)
                        continue;
                    var comparingProgramEditionID = programEditionIDList[j];
                    var comparingVector = programEditionsWithVector[comparingProgramEditionID];
                    float value = computeCorrelation(etalonVector, comparingVector);
                    if (value >= threshold)
                    {
                        ranking.Add(comparingProgramEditionID, value);
                    }
                }
          
                // sort result by value

                // it is necessary to write results into programEdition record
                var dataToWrite = ranking.OrderByDescending(o => o.Value).Select(s => s.Key).Take(5).ToList();
                var recommendationStr = Serialization.numberListToString<long>(dataToWrite);
                programEditionListForUpdate.Add(etalonProgramEditionID, recommendationStr);

            }
            // write somewhere
            updateProgramEditionRecommendation(unitOfWork, programEditionListForUpdate);
        }

        /// <summary>
        /// build tag vector with weights from program edition (id)
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="programEditionID"></param>
        /// <returns></returns>
        public static SortedList<int, float> getVector(UnitOfWork unitOfWork, long programEditionID)
        {

            var tagList = unitOfWork.ProgramEditionAndTagRepository.Get(filter: obj => obj.ProgramEditionID == programEditionID, includeProperties: "Tag").Select(s => s.Tag).ToList();
            var tagPairList = getTagIDWithWeights(unitOfWork, tagList);

            return tagPairList;
        }

        /// <summary>
        /// build tag vector with weights (icluding information about parent's tags) from just tag list
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="tagList"></param>
        /// <returns></returns>
        private static SortedList<int, float> getTagIDWithWeights(UnitOfWork unitOfWork, List<Tag> tagList)
        {
            try
            {

                var tagPairList = new SortedList<int, float>();

                // explore each tag of tag list
                for (int i = 0; i < tagList.Count; i++)
                {
                    // let check tag
                    Tag tag = tagList[i];
                    int tagLevel = tag.Level;
                    int tagID = tag.ID;

                    if (!tagPairList.ContainsKey(tagID))
                        tagPairList.Add(tagID, 1f);
                    else
                    {
                        tagPairList[tagID] = 1f;
                    }
                    getParentTagInfo(unitOfWork, tagID, tagPairList, 1f);
                }
                return tagPairList;
            }
            catch (Exception exc)
            {
                return new SortedList<int, float>();
            }

        }

        /// <summary>
        /// check parent tag and add info about it to tagPairList
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="tagID"></param>
        /// <param name="tagPairList"></param>
        /// <param name="previousValue"></param>
        static void getParentTagInfo(UnitOfWork unitOfWork, int tagID, SortedList<int, float> tagPairList, float previousValue)
        {
            Tag tagInfo = unitOfWork.TagRepository.GetByID(tagID);
            if (tagInfo == null || tagInfo.Level == 1 || tagInfo.ParentTag == null)
                return;

            // explore parent tag
            Tag parentTag = tagInfo.ParentTag;
            int currentTagID = parentTag.ID;
            float currentValue = previousValue / devider;

            // check whether tag was already in tagPairList
            // leave pair with higher value
            if (tagPairList.ContainsKey(currentTagID))
            {
                float oldValue = tagPairList[currentTagID];
                if (currentValue > oldValue)
                    tagPairList[currentTagID] = currentValue;
            }
            else
            {
                tagPairList.Add(currentTagID, currentValue);
            }

            if (parentTag.Level != 1)
                getParentTagInfo(unitOfWork, currentTagID, tagPairList, currentValue);
            else
                return;
        }



        // можно в принципе ограничиться пространством тегов только двух сравниваемых выпусков
        // будет правда посложнее выстроить вектор тегов
        /// <summary>
        /// 
        /// </summary>
        /// <param name="originalTagListA"></param>
        /// <param name="originalTagListB"></param>
        /// <returns></returns>
        private static float computeCorrelation(SortedList<int, float> originalTagListA, SortedList<int, float> originalTagListB)
        {
            // get the length of the real vector (keys without repetition)
            var allTagIds = originalTagListA.Keys.Union(originalTagListB.Keys).Distinct();
            var vectorLength = allTagIds.Count();

            float[] vectorA = new float[vectorLength];
            float[] vectorB = new float[vectorLength];

            // fill vectors with values
            for (int i = 0; i < vectorA.Length; i++)
            {
                int tagID = allTagIds.ElementAt(i);
                vectorA[i] = originalTagListA.ContainsKey(tagID) ? originalTagListA[tagID] : 0.0f;
                vectorB[i] = originalTagListB.ContainsKey(tagID) ? originalTagListB[tagID] : 0.0f;
            }

            float? result = CosCorrelation.calculateCos(vectorA, vectorB);
            return result.HasValue ? result.Value : 0.0f;
        }




    }
}