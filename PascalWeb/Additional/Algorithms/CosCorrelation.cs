﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.Additional.Algorithms
{
    public class CosCorrelation
    {
        /// <summary>
        /// calculate cos distance between two vectors
        /// </summary>
        /// <param name="vectorA"></param>
        /// <param name="vectorB"></param>
        /// <returns></returns>
        public static float? calculateCos(float[] vectorA, float[] vectorB)
        {
            try
            {
                if (vectorA == null || vectorB == null || vectorA.Length != vectorB.Length)
                    return 0.0f;
                float result = 0.0f;
                for (int i = 0; i < vectorA.Length; i++)
                    result += vectorA[i] * vectorB[i];
                var absVectorA = calculateVectorAbs(vectorA);
                var absVectorB = calculateVectorAbs(vectorB);
                if (absVectorA == 0.0f || absVectorB == 0.0f)
                    result = 0.0f;
                else
                    result = result / (absVectorA * absVectorB);
                return result;
            }
            catch (Exception exc)
            {
                return null;
            }
        }


        /// <summary>
        /// calculate abs of vector
        /// </summary>
        /// <param name="vector"></param>
        /// <returns></returns>
        private static float calculateVectorAbs(float[] vector)
        {
            float result = 0.0f;
            for (int i = 0; i < vector.Length; i++)
                result += vector[i] * vector[i];
            result = (float)Math.Sqrt(result);
            return result;
        }
    }
}