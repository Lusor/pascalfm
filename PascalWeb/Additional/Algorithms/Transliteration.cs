﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.Additional.Algorithms
{
    public class Transliteration
    {
        public static Dictionary<string, string> initDictionaryWithFMS()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();

            dict.Add("а", "a");
            dict.Add("б", "b");
            dict.Add("в", "v");
            dict.Add("г", "g");
            dict.Add("д", "d");
            dict.Add("е", "e");
            dict.Add("ё", "e");
            dict.Add("ж", "zh");
            dict.Add("з", "z");
            dict.Add("и", "i");
            dict.Add("й", "i");
            dict.Add("к", "k");
            dict.Add("л", "l");
            dict.Add("м", "m");
            dict.Add("н", "n");
            dict.Add("о", "o");
            dict.Add("п", "p");
            dict.Add("р", "r");
            dict.Add("с", "s");
            dict.Add("т", "t");
            dict.Add("у", "u");
            dict.Add("ф", "f");
            dict.Add("х", "kh");
            dict.Add("ц", "ts");
            dict.Add("ч", "ch");
            dict.Add("ш", "sh");
            dict.Add("щ", "tsch");
            dict.Add("ъ", "");
            dict.Add("ы", "y");
            dict.Add("ь", "");
            dict.Add("э", "e");
            dict.Add("ю", "yu");
            dict.Add("я", "ya");

            return dict;
        }


        public static string translite(string russianStr)
        {
            if (string.IsNullOrEmpty(russianStr))
                return string.Empty;
            else
            {
                var dict = initDictionaryWithFMS();

                russianStr = russianStr.ToLower();
                string result = russianStr;
                for (int i = 0; i < dict.Count(); i++)
                {
                    result = result.Replace(dict.ElementAt(i).Key, dict.ElementAt(i).Value);
                }
                return result;
            }
        }
    }
}