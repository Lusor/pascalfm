﻿using PascalWeb.Additional.FileProcessing;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace PascalWeb.Additional
{
    public class cFileWork
    {
        public static byte[] ConvertToBytes(HttpPostedFileBase image)
        {
            byte[] dataBytes;

            MemoryStream memoryStream = image.InputStream as MemoryStream;
            if (memoryStream == null)
            {
                memoryStream = new MemoryStream();
                image.InputStream.CopyTo(memoryStream);
            }
            dataBytes = memoryStream.ToArray();

            // return position for reading
            image.InputStream.Position = 0;
            return dataBytes;
        }

        public static bool convertIconImage(byte[] imageBytes, out Image smallSizeImage, out Image mediumSizeImage)
        {
            Size smallSize = new Size(50, 50);
            Size mediumSize = new Size(200, 200);

            MemoryStream bitmapDataStream = new MemoryStream(imageBytes);
            Bitmap bitmap = new Bitmap(bitmapDataStream);

            smallSizeImage = ImageProcessing.ImageProcessing.Resize(bitmap, smallSize);
            mediumSizeImage = ImageProcessing.ImageProcessing.Resize(bitmap, mediumSize);
            return true;
        }

        public static int getMp3FileDuration(HttpPostedFileBase file)
        {
            try
            {
                int duration = 0;
                var property = AudioFilesTagAnalyzor.FileProperty(file.InputStream, file.FileName);
                if (property != null)
                    duration =  (int)property.Duration.TotalSeconds;
                return duration;
            }
            catch (Exception exp)
            {
                // TODO LOG
                return 0;
            }
        }
    }
}