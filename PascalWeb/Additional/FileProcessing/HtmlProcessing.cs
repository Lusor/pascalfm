﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace PascalWeb.Additional
{
    public class HtmlProcessing
    {
        public static string GetParagraphsBeforeKeyWord(string htmlCode, string keyWord, out int separationPosition)
        {
            separationPosition = 0;
            if (string.IsNullOrEmpty(htmlCode) || string.IsNullOrEmpty(keyWord))
                return htmlCode;

            // if key word was not found then return original html code
            if (!htmlCode.Contains(keyWord))
                return htmlCode;

            htmlCode = string.Copy(htmlCode);
            string result = "";
            // search keyword in each paragraph

            using (TextReader sr = new StringReader(htmlCode))
            {
                HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
                document.Load(sr);
                var array = document.DocumentNode.SelectNodes("//p");
                if (array != null)
                {
                    foreach (HtmlAgilityPack.HtmlNode paragraph in array)
                    {
                        string paragraphHtmlCode = paragraph.OuterHtml; // or something similar
                        result += paragraphHtmlCode;
                        separationPosition += paragraphHtmlCode.Length;
                        if (paragraphHtmlCode.Contains(keyWord))
                        {
                            break;
                        }
                    }
                }
            }

            separationPosition = result.Length;
            return result;
        }


    }
}