﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace PascalWeb.Additional.FileProcessing
{
    public class AudioFilesTagAnalyzor
    {
        //public void SampleTaglib(Stream song, string songFileName)
        //{
        //    //Get the tags
        //    TagLib.Tag tags = FileTagReader(song, songFileName);

        //    //We can read and display info from it
        //    //TextBlock1.Text = tags.Title;
        //    //TextBlock2.Text = tags.Album;

        //    //Sample 2
        //    tags.Comment = "Read and edited using SampleTaglib app by Zumicts";

        //    //Reading picture
        //    MemoryStream ms = new MemoryStream(tags.Pictures[0].Data.Data);
        //    BitmapImage bi = new BitmapImage();
        //    bi.SetSource(ms);

        //    //Writing picture
        //    //albmStream would be a stream containing picture data
        //    //create the picture for the album cover
        //    TagLib.Picture picture = new TagLib.Picture(new TagLib.ByteVector(albmStream.ToArray()));

        //    //create Id3v2 Picture Frame
        //    TagLib.Id3v2.AttachedPictureFrame albumCoverPictFrame = new TagLib.Id3v2.AttachedPictureFrame(picture);
        //    //set the type of picture (front cover)
        //    albumCoverPictFrame.Type = TagLib.PictureType.FrontCover;

        //    //Id3v2 allows more than one type of image, just one needed
        //    TagLib.IPicture[] pictFrames = { albumCoverPictFrame };

        //    tags.Pictures = pictFrames;
        //}

        public static TagLib.Tag FileTagReader(Stream stream, string fileName)
        {
            //Create a simple file and simple file abstraction
            var simpleFile = new SimpleFile(fileName, stream);
            var simpleFileAbstraction = new SimpleFileAbstraction(simpleFile);
            /////////////////////

            //Create a taglib file from the simple file abstraction
            var mp3File = TagLib.File.Create(simpleFileAbstraction);

            //Get all the tags
            TagLib.Tag tags = mp3File.Tag;

            //Save and close
            //mp3File.Save();
            mp3File.Dispose();

            //Return the tags
            return tags;
        }

        public static TagLib.Properties FileProperty(Stream stream, string fileName)
        {
            //Create a simple file and simple file abstraction
            var simpleFile = new SimpleFile(fileName, stream);
            var simpleFileAbstraction = new SimpleFileAbstraction(simpleFile);
            /////////////////////

            //Create a taglib file from the simple file abstraction
            var mp3File = TagLib.File.Create(simpleFileAbstraction);

            //Get all the tags
            var tags = mp3File.Properties;

            //Save and close
            //mp3File.Save();
            mp3File.Dispose();

            //Return the tags
            return tags;
        }

        public static Stream FileTagEditor(Stream stream, string fileName, TagLib.Tag newTag)
        {
            //Create a simple file and simple file abstraction
            var simpleFile = new SimpleFile(fileName, stream);
            var simpleFileAbstraction = new SimpleFileAbstraction(simpleFile);
            /////////////////////

            //Create a taglib file from the simple file abstraction
            var mp3File = TagLib.File.Create(simpleFileAbstraction);

            //Copy the all the tags to the file (overwrite if exist)
            newTag.CopyTo(mp3File.Tag, true);
            //Pictures tag had to be done seperately
            //During testing sometimes it didn't copy
            mp3File.Tag.Pictures = newTag.Pictures;

            //save it and close it
            mp3File.Save();
            mp3File.Dispose();

            //Return the stream back (now edited with the new tags)
            return stream;
        }
    }
}