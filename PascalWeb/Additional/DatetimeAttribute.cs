﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace PascalWeb.Additional
{
    public class DatetimeAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            string format = "yyyy-MM-dd HH:mm";
            try
            {
                DateTime? str = value as DateTime?;
                //CultureInfo provider = CultureInfo.InvariantCulture;
                //DateTime result = DateTime.ParseExact(str, format, provider);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }

            //DateTime dt;
            //return true;
            //bool parsed = DateTime.TryParse((string)value, out dt);
            //if (!parsed)
            //    return false;

            // eliminate other invalid values, etc
            // if contains valid hour for your business logic, etc

            return true;
        }
    }
}