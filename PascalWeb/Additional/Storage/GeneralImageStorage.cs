﻿using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace PascalWeb.Additional.Storage
{
    public class GeneralImageStorage
    {

        public static bool saveBlogImages(HttpPostedFileBase file, DateTime creationDate, out string absoluteUri)
        {
            cStorageManager.eStorageContainerName containerName = cStorageManager.eStorageContainerName.blogcontent;
            return saveContent(file.InputStream, file.ContentType, containerName.ToString(), cStorageManager.eEntityStorageName.blogRecord, creationDate, out absoluteUri);
        }


        public static bool saveProgramImages(HttpPostedFileBase file, DateTime creationDate, out string absoluteUri)
        {
            cStorageManager.eStorageContainerName containerName = cStorageManager.eStorageContainerName.programlogos;
            return saveContent(file.InputStream, file.ContentType, containerName.ToString(), cStorageManager.eEntityStorageName.program, creationDate, out absoluteUri);
        }

        public static bool saveProgramImages(Image image, DateTime creationDate, out string absoluteUri)
        {
            cStorageManager.eStorageContainerName containerName = cStorageManager.eStorageContainerName.programlogos;
            string mimeType = null;
            var stream = ImageProcessing.ImageProcessing.ImageToStream(image);
            return saveContent(stream, mimeType, containerName.ToString(), cStorageManager.eEntityStorageName.program, creationDate, out absoluteUri);
        }

        public static bool saveBlogLogos(Image image, DateTime creationDate, out string absoluteUri)
        {
            cStorageManager.eStorageContainerName containerName = cStorageManager.eStorageContainerName.bloglogos;
            string mimeType = null;
            var stream = ImageProcessing.ImageProcessing.ImageToStream(image);
            return saveContent(stream, mimeType, containerName.ToString(), cStorageManager.eEntityStorageName.blog, creationDate, out absoluteUri);
        }

        public static bool saveUserImages(HttpPostedFileBase file, DateTime creationDate, out string absoluteUri)
        {
            cStorageManager.eStorageContainerName containerName = cStorageManager.eStorageContainerName.userlogos;
            return saveContent(file.InputStream, file.ContentType, containerName.ToString(), cStorageManager.eEntityStorageName.user, creationDate, out absoluteUri);
        }

        public static bool saveUserImages(Image image, DateTime creationDate, out string absoluteUri)
        {
            if (image == null)
            {
                absoluteUri = null;
                return false;
            }
            cStorageManager.eStorageContainerName containerName = cStorageManager.eStorageContainerName.userlogos;
            string mimeType = null;
            var stream = ImageProcessing.ImageProcessing.ImageToStream(image);
            return saveContent(stream, mimeType, containerName.ToString(), cStorageManager.eEntityStorageName.user, creationDate, out absoluteUri);
        }

        public static bool saveUserImages(byte[] imageBytes, DateTime creationDate, out string absoluteUri)
        {
            if (imageBytes == null)
            {
                absoluteUri = null;
                return false;
            }
            cStorageManager.eStorageContainerName containerName = cStorageManager.eStorageContainerName.userlogos;
            string mimeType = null;
            var stream = new MemoryStream(imageBytes);
            return saveContent(stream, mimeType, containerName.ToString(), cStorageManager.eEntityStorageName.user, creationDate, out absoluteUri);
        }

        /// <summary>
        /// save file to container
        /// </summary>
        /// <param name="fileStream"></param>
        /// <param name="contentType"></param>
        /// <param name="containerName"></param>
        /// <param name="entityName"></param>
        /// <param name="creationDate"></param>
        /// <param name="absoluteUri"></param>
        /// <returns></returns>
        private static bool saveContent(Stream fileStream, string contentType, string containerName, cStorageManager.eEntityStorageName entityName, DateTime creationDate, out string absoluteUri)
        {
            Guid fileGuid = Guid.NewGuid();
            string uniqueName = null;
            absoluteUri = null;

            bool storageConnected = cStorageManager.getConnected();
            var container = cStorageManager.createContainer(containerName);
            uniqueName = cStorageManager.generateBlobName(container, fileGuid, entityName);
            CloudBlockBlob blob = null;
            bool saved = cStorageManager.addBlob(fileStream, contentType, container, uniqueName, out blob);

            if (!saved)
                return saved;

            absoluteUri = blob.StorageUri.PrimaryUri.AbsoluteUri;

            return true;
        }


        /// <summary>
        /// delete blob from storage
        /// </summary>
        /// <param name="absoluteUri"></param>
        /// <returns></returns>
        public static bool deleteFile(string absoluteUri)
        {
            try
            {
                cStorageManager.deleteBlob(absoluteUri);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}