﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace PascalWeb.Additional.Storage
{
    public static class cStorageManager
    {
        public enum eStorageContainerName { audio, blogcontent, bloglogos, programlogos, userlogos };
        public enum eEntityStorageName { programEdition, blogRecord, blog, program, user };
        //private static volatile cStorageManager instance;
       // private static object syncRoot = new Object();

        static bool connected = false;
        static CloudStorageAccount storageAccount = null;

        static cStorageManager() 
        {
            cStorageManager.connect();
        }

        public static bool connect()
        {
            try{
                storageAccount = CloudStorageAccount.Parse(
                    CloudConfigurationManager.GetSetting("StorageConnectionString"));
                connected = true;

            } catch(Exception ex)
            {
                connected = false;
            }
          

            return connected;
        }

        public static bool getConnected()
        {
            return connected;
        }

        public static string getStorageBlobEndpoint()
        {
            return storageAccount.BlobEndpoint.AbsoluteUri;
        }

        public static CloudBlobContainer createContainer(string name = "audio")
        {
            try
            {
                var blobStorage = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobStorage.GetContainerReference(name);
                if (container.CreateIfNotExists())
                {
                    // configure container for public access
                    var permissions = container.GetPermissions();
                    permissions.PublicAccess = BlobContainerPublicAccessType.Blob;
                    container.SetPermissions(permissions);
                }
                return container;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static string generateBlobName(CloudBlobContainer container, Guid guid, eEntityStorageName entityName)
        {
            string uniqueBlobName = null;
            string containerName = container.Name;
            DateTime date = DateTime.Now;
            uniqueBlobName = string.Format("{0}/{1}/{2}/{3}", 
                date.Year.ToString(),
                date.Month.ToString(),
                entityName,
                guid.ToString());

            return uniqueBlobName;
        }

        //public static bool addBlob(HttpPostedFileBase file, CloudBlobContainer container, string uniqueBlobName,
        //    out CloudBlockBlob blob)
        //{
        //    return addBlob(file.InputStream, file.ContentType, container, uniqueBlobName, out blob);
        //}

        //public static bool addBlob(byte[] byteArray, string contentType, CloudBlobContainer container, string uniqueBlobName,
        //    out CloudBlockBlob blob)
        //{
        //    MemoryStream stream = new MemoryStream(byteArray);
        //    return addBlob(stream, contentType, container, uniqueBlobName, out blob);
        //}

        public static bool addBlob(Stream fileStream, string contentType, CloudBlobContainer container, string uniqueBlobName,
            out CloudBlockBlob blob)
        {
            try
            {
                string containerName = container.Name;
                blob = container.GetBlockBlobReference(uniqueBlobName);
                blob.Properties.ContentType = contentType;
                blob.UploadFromStream(fileStream);
                return true;
            }
            catch (Exception ex)
            {
                blob = null;
                return false;
            }
        }

        public static bool deleteBlob(string containerName, string blobName)
        {
            try
            {
                // Create the blob client.
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve reference to a previously created container.
                CloudBlobContainer container = blobClient.GetContainerReference(containerName);

                // Retrieve reference to a blob named "myblob.txt".
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(blobName);

                // Delete the blob.
                blockBlob.Delete();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool deleteBlob(string absoluteUri)
        {
            try
            {
                // Create the blob client.
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve reference to a blob named "myblob.txt".
                var blockBlob = blobClient.GetBlobReferenceFromServer(new Uri(absoluteUri));

                // Delete the blob.
                blockBlob.DeleteIfExists();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="absoluteUri"></param>
        /// <returns></returns>
        public static string getDownloadLink(string absoluteUri, string fileName)
        {
            try
            {
                // Create the blob client.
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve reference to a blob named "myblob.txt".
                var blob = blobClient.GetBlobReferenceFromServer(new Uri(absoluteUri));

                string extension = Path.GetExtension(absoluteUri);

                var sasToken = blob.GetSharedAccessSignature(new SharedAccessBlobPolicy()
                {
                    Permissions = SharedAccessBlobPermissions.Read,
                    SharedAccessExpiryTime = DateTime.UtcNow.AddMinutes(60),
                }, new SharedAccessBlobHeaders()
                {
                    ContentDisposition = "attachment; filename=" + fileName + extension
                    //ContentType = "audio/mpeg3"
                });
                var blobUrl = string.Format("{0}{1}", blob.Uri, sasToken);
                return blobUrl;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        /// <summary>
        /// #IMPORTANT method for setting property for byte range support
        /// http://programmerpayback.com/2013/01/30/hosting-progressive-download-videos-on-azure-blobs/
        /// </summary>
        /// <returns></returns>
        public static bool setSpecialSettings()
        {
            try
            {
                var client = storageAccount.CreateCloudBlobClient();
                var properties = client.GetServiceProperties();
                properties.DefaultServiceVersion = "2011-01-18"; //"2014-02-14";// "2012-02-12";
                client.SetServiceProperties(properties); 
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }




        //public static void renameFiles(string oldBlobName, string newBlobName)
        //{
        //    bool storageConnected = cStorageManager.getConnected();
        //    var container = cStorageManager.createContainer(eEntityStorageName.programEdition.ToString());
        //    CloudBlockBlob blobObj = container.GetBlockBlobReference(oldBlobName);
        //    blobObj.ren


        //}


    }
}