﻿using FluentScheduler;
using PascalWeb.Additional.Algorithms;
using PascalWeb.DB.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.Additional.ScheduleTasks
{
    public class PerformRecommendationTask : ITask
    {
        public void Execute()
        {
            try
            {
                PascalLogging.Logger.Info("PerformRecommendationTask begins");
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    ProgramEditionCorrelation.correlateAllProgramEditions(unitOfWork);

                    //HttpRuntime.Cache.Insert(eCasheEnumeration.FP_PROGRAM_TOP_AUTHOR_LIST.ToString(), programTopAuthorList);
                }
                // end
                PascalLogging.Logger.Info("PerformRecommendationTask finished");
            }
            catch (Exception exc)
            {
                PascalLogging.Logger.Error("ERROR PerformRecommendationTask", exc);
            }
        }
    }
}