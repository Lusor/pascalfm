﻿using FluentScheduler;
using PascalWeb.Additional.Email;
using PascalWeb.DB.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.Additional.ScheduleTasks
{
    public class EmailSendingTask : ITask
    {
        //readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void Execute()
        {
            try
            {
                //PascalLogging.WriteInfoMessage("EmailSendingTask", "Execute", "beginning of email sending");

                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    // get several emails and send them
                    var emailList = unitOfWork.EmailDispatchRepository.Get(includeProperties: "UserAuth, EmailDispatchType").Take(25);
                    for (int i = 0; i < emailList.Count(); i++)
                    {
                        var emailItem = emailList.ElementAt(i);
                        bool sendingResult = EmailSending.sendMessageFromDB(emailItem);
                        if (sendingResult)
                        {
                            unitOfWork.EmailDispatchRepository.Delete(emailItem);
                        }
                    }
                    // delete sent emails
                    unitOfWork.Save();
                }

                //PascalLogging.WriteInfoMessage("EmailSendingTask", "Execute", "end of email sending");
            }
            catch (Exception exp)
            {
                PascalLogging.WriteErrorMessage("EmailSendingTask", "Execute", exp.Message);
            }
        }
    }
}