﻿using FluentScheduler;
using PascalWeb.Additional.Cache;
using PascalWeb.DB;
using PascalWeb.DB.PageModels;
using PascalWeb.DB.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.Additional.ScheduleTasks
{
    public class FirstPageUpdateTask : ITask
    {
        public void Execute()
        {
            try
            {
                UnitOfWork unitOfWork = new UnitOfWork();
                {
                    // for first page
                    //  var topProgramEditionsByProgram = CasheManager.execQuery<LastProgramEditionPageModel>(eCasheEnumeration.FP_TOP_PROGRAM_EDITIONS_BY_PROGRAM, unitOfWork);
                    //  HttpRuntime.Cache.Insert(eCasheEnumeration.FP_TOP_PROGRAM_EDITIONS_BY_PROGRAM.ToString(), topProgramEditionsByProgram);

                    var lastProgramEditionsByProgram = CasheManager.execQuery<LastProgramEditionPageModel>(eCasheEnumeration.FP_LAST_PROGRAM_EDITIONS, unitOfWork);
                    HttpRuntime.Cache.Insert(eCasheEnumeration.FP_LAST_PROGRAM_EDITIONS.ToString(), lastProgramEditionsByProgram);

                    var lastBlogRecordsForFP = CasheManager.execQuery<List<BlogRecord>>(eCasheEnumeration.FP_BLOG_RECORDS_LAST, unitOfWork);
                    HttpRuntime.Cache.Insert(eCasheEnumeration.FP_BLOG_RECORDS_LAST.ToString(), lastBlogRecordsForFP);

                    // program top author list
                    var programTopAuthorList = CasheManager.execQuery<List<UserAuth>>(eCasheEnumeration.FP_PROGRAM_TOP_AUTHOR_LIST, unitOfWork);
                    HttpRuntime.Cache.Insert(eCasheEnumeration.FP_PROGRAM_TOP_AUTHOR_LIST.ToString(), programTopAuthorList);
                }
                // end
                PascalLogging.Logger.Info("FirstPage Cache End");
            }
            catch (Exception)
            {
                PascalLogging.Logger.Info("ERROR FirstPageUpdateTask");
            }
        }
    }
}