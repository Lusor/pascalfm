﻿using FluentScheduler;
using PascalWeb.DB;
using PascalWeb.DB.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PascalWeb.Additional.ScheduleTasks
{ 
    public class StatisticsSliceTask : ITask
    {
        public void Execute()
        {
            try
            {
                // the only restriction is that it should be performed once in a day near midnight
                DateTime currentTime = DateTime.Now;
                
    

                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    DateTime lastSliceDatetime = unitOfWork.StatisticsSliceRepository.Get(orderBy: q => q.OrderByDescending(x => x.ID)).Select(obj => obj.DateStamp).FirstOrDefault();
                    if (lastSliceDatetime.Day == currentTime.Day &&
                       lastSliceDatetime.Month == currentTime.Month &&
                        lastSliceDatetime.Year == currentTime.Year)
                        return;


                    /*
                     SELECT pe.ID, pe.AirDate, p.Name, pe.EditionTittle, pe.ShortDescription, pe.ViewedCounter, pe.ListenedCounter, pe.CommentsCounter, pe.TagString, pe.Published  FROM ProgramEdition pe
                      INNER JOIN Program p ON pe.ProgramID = p.ID
                      ORDER by pe.ViewedCounter+pe.ListenedCounter DESC
                     */

                    var programEditionList = unitOfWork.ProgramEditionRepository.Get();
                    for (int i = 0; i < programEditionList.Count(); i++)
                    {
                        ProgramEdition pe = programEditionList.ElementAt(i);
                        StatisticsSlice slice = new StatisticsSlice();
                        slice.EntityType = 1;
                        slice.DateStamp = DateTime.Now;
                        slice.EntityID = pe.ID;
                        slice.ListenedCounter = pe.ListenedCounter;
                        slice.ViewedCounter = pe.ViewedCounter;
                        slice.DownloadCounter = pe.DownloadCounter;
                        slice.CommentsCounter = pe.CommentsCounter;
                        unitOfWork.StatisticsSliceRepository.Insert(slice);
                        unitOfWork.Save();
                    }
            
                    PascalLogging.Logger.Info("PerformRecommendationTask performed");
                }
                // end

            }
            catch (Exception exc)
            {
                PascalLogging.Logger.Error("ERROR PerformRecommendationTask", exc);
            }
        }
    }
}
