﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

public static class GeneralMethods
{
    /// <summary>
    /// Date to string
    /// </summary>
    /// <param name="date"></param>
    /// <returns></returns>
    public static string getFormatedDate(DateTime? date)
    {
        return date.HasValue ? date.Value.ToString("dd.MM.yyyy") : "";
    }

    /// <summary>
    /// Datetime to string
    /// </summary>
    /// <param name="date"></param>
    /// <returns></returns>
    public static string getFormatedDateAndTime(DateTime? date)
    {
        return date.HasValue ? date.Value.ToString("dd.MM.yyyy HH:mm") : "";
    }

    /// <summary>
    /// bytes of immage to string
    /// </summary>
    /// <param name="byteArray"></param>
    /// <param name="contentType"></param>
    /// <returns></returns>
    public static string getImageSourceFromBytes(byte[] byteArray, string contentType)
    {
        if (byteArray == null)
            return null;
        string imageBase64 = Convert.ToBase64String(byteArray);
        string imageSrc = string.Format("data:" + contentType + ";base64,{0}", imageBase64);
        return imageSrc;
    }

    /// <summary>
    /// comparision of string
    /// </summary>
    /// <param name="firstStr"></param>
    /// <param name="secondStr"></param>
    /// <param name="caseSensitive"></param>
    /// <returns></returns>
    public static bool compareStrings(string firstStr, string secondStr, bool caseSensitive = false)
    {
        string str1 = firstStr == null ? "" : firstStr;
        string str2 = secondStr == null ? "" : secondStr;
        if (!caseSensitive)
        {
            str1 = str1.ToLower();
            str2 = str2.ToLower();
        }
        return (str1.CompareTo(str2) == 0);

    }

    /// <summary>
    /// concatenation of 2 strings
    /// </summary>
    /// <param name="str1"></param>
    /// <param name="str2"></param>
    /// <returns></returns>
    public static string concatenateStrings(string str1, string str2)
    {
        string temp_str1 = str1 == null ? "" : str1;
        string temp_str2 = str2 == null ? "" : str2;

        return temp_str1 + temp_str2;
    }

    /// <summary>
    /// concatenation of 3 strings
    /// </summary>
    /// <param name="str1"></param>
    /// <param name="str2"></param>
    /// <param name="str3"></param>
    /// <returns></returns>
    public static string concatenateStrings(string str1, string str2, string str3)
    {
        string temp_str1 = str1 == null ? "" : str1;
        string temp_str2 = str2 == null ? "" : str2;
        string temp_str3 = str3 == null ? "" : str3;

        return temp_str1 + temp_str2 + temp_str3;
    }

    public static string getTrackDurationString(int? timeInSeconds)
    {
        if (!timeInSeconds.HasValue)
            return "";
        int minutes = timeInSeconds.Value / 60;
        int seconds = timeInSeconds.Value % 60;

        string result = minutes < 10 ? "0" + minutes.ToString() : minutes.ToString();
        result += ":";
        result += seconds < 10 ? "0" + seconds.ToString() : seconds.ToString();
        return result;
    }

    public static string limitStringAndAddDotsLength(string initialString, int limitNumber)
    {
        string result = initialString != null ? initialString : "";
        int positionToCut = result.Length;
        if (result.Length > limitNumber)
        {
            for (int i = limitNumber; i < result.Length; i++)
            {
                if (result[i] == ' ' || result[i] == '.' || result[i] == ',')
                {
                    positionToCut = i;
                    break;
                }
            }
            result = result.Substring(0, positionToCut);

            // add dots in the end
            result = result + " ...";
        }

        return result;
    }

    public static T ParseEnum<T>(string value)
    {
        return (T)Enum.Parse(typeof(T), value, true);
    }





    public static string ImageToBase64(Image image, System.Drawing.Imaging.ImageFormat format)
    {
        using (MemoryStream ms = new MemoryStream())
        {
            // Convert Image to byte[]
            image.Save(ms, format);
            byte[] imageBytes = ms.ToArray();

            // Convert byte[] to Base64 String
            string base64String = Convert.ToBase64String(imageBytes);
            return base64String;
        }
    }

    public static Image Base64ToImage(string base64String)
    {
        // Convert Base64 String to byte[]
        byte[] imageBytes = Convert.FromBase64String(base64String);
        MemoryStream ms = new MemoryStream(imageBytes, 0,
          imageBytes.Length);

        // Convert byte[] to Image
        ms.Write(imageBytes, 0, imageBytes.Length);
        Image image = Image.FromStream(ms, true);
        return image;
    }
}
