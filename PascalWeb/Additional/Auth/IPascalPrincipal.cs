﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace PascalWeb.Additional.Auth
{
    public interface IPascalPrincipal : IPrincipal
    {
        long UserID { get; set; }
        string Login { get; set; }
        string RoleName { get; set; }
    }

    public class cPascalPrincipal : IPascalPrincipal
    {
        public IIdentity Identity { get; private set; }
        public bool IsInRole(string role)
        {
            if (!string.IsNullOrWhiteSpace(role) && !string.IsNullOrWhiteSpace(RoleName))
            {
                return GeneralMethods.compareStrings(role, RoleName);
            }
            return false;
        }

        public cPascalPrincipal(string login)
        {
            this.Identity = new GenericIdentity(login);
        }

        public long UserID { get; set; }
        public string Login { get; set; }
        public string RoleName { get; set; }
    }


    public class CustomPrincipalSerializedModel
    {
        public long UserID { get; set; }
        public string Login { get; set; }
        public string RoleName { get; set; }
    }
}