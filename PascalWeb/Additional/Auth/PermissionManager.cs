﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PascalWeb.DB.Enumerators;
using PascalWeb.DB.Repositories;

namespace PascalWeb.Additional.Auth
{
    public class PermissionManager
    {
        public enum eRedirectResults {eToLoginPage, eToHomePage, OK};

        public UnitOfWork unitOfWork = new UnitOfWork();

        public eRedirectResults ValidatePermissions(string controller, string action, string role, bool isAuthenticated)
        {
            eRedirectResults isUserAccess = eRedirectResults.eToHomePage;

            if (!isAuthenticated)
                role = eUserRoles.withoutauth.ToString();

            controller = controller != null ? controller.ToLower() : "";
            action = action != null ? action.ToLower() : "";
            role = role != null ? role.ToLower() : "";

            if (controller.CompareTo("home") == 0)
            {
                isUserAccess = eRedirectResults.OK;
                return isUserAccess;
            }

            if (isAuthenticated && (role == eUserRoles.superadmin.ToString()))
                return eRedirectResults.OK;

            bool toLoginPage = false;
            var res = unitOfWork.UserManagment.getUserRight(controller, action, role, out toLoginPage);

            switch (res)
            {
                case true:  isUserAccess = eRedirectResults.OK;
                            break;
                case false: if (toLoginPage)
                                isUserAccess = eRedirectResults.eToLoginPage;
                            else
                                isUserAccess = eRedirectResults.eToHomePage;
                            break;
            }

            return isUserAccess;
        }
    }

    // http://habrahabr.ru/post/137581/
    // http://stackoverflow.com/questions/1279643/asp-net-mvc-redirect-to-an-access-denied-page-using-a-custom-role-provider
    // http://stackoverflow.com/questions/1490401/redirecting-to-specified-controller-and-action-in-asp-net-mvc-action-filter

}