﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using PascalWeb.DB;
using PascalWeb.DB.Repositories;
using PascalWeb.DB.Enumerators;

namespace PascalWeb.Additional.Auth
{
    public class DynamicAuthorizeAttribute : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            PermissionManager permissionManager = new PermissionManager();
            string action = filterContext.ActionDescriptor.ActionName;
            string controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            bool isAuthenticated = filterContext.HttpContext.User.Identity.IsAuthenticated;

            //if (filterContext.HttpContext.Request.QueryString != null && filterContext.HttpContext.Request.QueryString.AllKeys != null &&
            //    filterContext.HttpContext.Request.QueryString.AllKeys.Contains("_"))
            //    return;

            long? userID = null;
            string userRoleName = eUserRoles.withoutauth.ToString();
            {
                var user = filterContext.HttpContext.User as cPascalPrincipal;
                if (user != null)
                {
                    userRoleName = user.RoleName;
                    userID = user.UserID; 
                }
            }

            PermissionManager.eRedirectResults res = permissionManager.ValidatePermissions(controller, action, userRoleName, isAuthenticated);
            switch (res)
            {
                case PermissionManager.eRedirectResults.OK:
                    break;
                case PermissionManager.eRedirectResults.eToHomePage:
                    filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary {{ "Controller", "Home" },
                                      { "Action", "PageNotFound" } });
                    break;
                case PermissionManager.eRedirectResults.eToLoginPage:
                    filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary {{ "Controller", "Account" },
                                      { "Action", "Login" } });
                    break;
            }

            writeRequestIntoLog(filterContext.RequestContext.HttpContext.Request, permissionManager.unitOfWork, userID);

        }

        private static void writeRequestIntoLog(HttpRequestBase request, UnitOfWork unitOfWork, long? userID)
        {
            try
            {
                // get url without host name (to save memory)
                var host = LogManagment.extractHostName(request);
                var requestedURL = request.Url.ToString();
                requestedURL = requestedURL.Replace(host, "");

                // get refferer url (if it is existed)
                string urlRefferer = "";
                if (request.UrlReferrer != null)
                {
                    urlRefferer = request.UrlReferrer.ToString();
                    urlRefferer = urlRefferer.Replace(host, "");
                }

                string browserVersion = "";
                if (request.Browser != null)
                {
                    browserVersion = GeneralMethods.concatenateStrings(request.Browser.Browser, " ", request.Browser.Version);
                    if (browserVersion.Length > 30)
                        browserVersion = browserVersion.Substring(0, 30);
                }

                string userAgent = "";
                if (request.UserAgent != null)
                {
                    userAgent = request.UserAgent;
                    if (userAgent.Length > 200)
                        userAgent = browserVersion.Substring(0, 200);
                }

                bool notUser = !LogManagment.isUserRequest(request);
                string ip = request.UserHostAddress;
     

                // write to DB
                LogManagment.addRequestToLog(unitOfWork, ip, requestedURL, urlRefferer, browserVersion, userAgent, userID, notUser);
            }
            catch (Exception exc)
            {
                PascalLogging.Logger.Info("DynamicAuthorizeAttribute: writeRequestIntoLog" + exc.Message);
            }
        }
    }
}