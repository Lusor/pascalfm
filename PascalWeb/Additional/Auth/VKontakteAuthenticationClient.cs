﻿using DotNetOpenAuth.AspNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace PascalWeb.Additional.Auth
{
    // http://codehint.ru/articles/2013-06-03_oauth_vkontakte_asp.net_mvc_4

    public class VKontakteAuthenticationClient : IAuthenticationClient
    {
        public string appId;
        public string appSecret;

        public VKontakteAuthenticationClient(string appId, string appSecret)
        {
            this.appId = appId;
            this.appSecret = appSecret;
        }

        string IAuthenticationClient.ProviderName
        {
            get { return "vkontakte"; }
        }

        private string redirectUri;

        void IAuthenticationClient.RequestAuthentication(HttpContextBase context, Uri returnUrl)
        {
            var APP_ID = this.appId;
            this.redirectUri = context.Server.UrlEncode(returnUrl.ToString());
            var address = String.Format(
                    "https://oauth.vk.com/authorize?client_id={0}&redirect_uri={1}&response_type=code",
                    APP_ID, this.redirectUri
                );

            HttpContext.Current.Response.Redirect(address, false);
        }

        class AccessToken
        {
            public string access_token = null;
            public string user_id = null;
        }

        class UserData
        {
            public string id = null;
            public string first_name = null;
            public string last_name = null;
            public string photo_50 = null;

            [DataMember(Name = "bdate")]
            public string bdate = null;
            public string domain = null;

            public string sex = null;
            public JsonKeyValueEntity city;
            public JsonKeyValueEntity country;
        }


        class UsersData
        {
            public UserData[] response = null;
        }

        class JsonKeyValueEntity
        {
            public string id = null;
            public string title = null;
        }

        AuthenticationResult IAuthenticationClient.VerifyAuthentication(HttpContextBase context)
        {
            try
            {
                string code = context.Request["code"];

                var address = String.Format(
                        "https://oauth.vk.com/access_token?client_id={0}&client_secret={1}&code={2}&redirect_uri={3}",
                        this.appId, this.appSecret, code, this.redirectUri);

                var response = VKontakteAuthenticationClient.Load(address);
                var accessToken = VKontakteAuthenticationClient.DeserializeJson<AccessToken>(response);

                address = String.Format(
                        "https://api.vk.com/method/users.get?user_ids={0}&fields=country,home_town,city,sex,photo_50,bdate,domain&format=JSON&v=5.8",  //&v=5.8
                        accessToken.user_id);
                //https://api.vk.com/method/users.get?uids={0}&fields=country,home_town,city_title,sex,photo_50,bdate,domain

                response = VKontakteAuthenticationClient.Load(address);
                var usersData = VKontakteAuthenticationClient.DeserializeJson<UsersData>(response);
                var userData = usersData.response.First();

                var dataAsDictionary = new Dictionary<string, string>();

                dataAsDictionary.Add("first_name", userData.first_name);
                dataAsDictionary.Add("last_name", userData.last_name);
                dataAsDictionary.Add("picture", userData.photo_50);
                dataAsDictionary.Add("birthday", userData.bdate);

                if (userData.country != null)
                    dataAsDictionary.Add("country", userData.country.title);
                if (userData.city != null)
                    dataAsDictionary.Add("city", userData.city.title);

                dataAsDictionary.Add("id", userData.id);
                // gender as text
                string gender = userData.sex.Equals("1") ? "female" : userData.sex.Equals("2") ? "male" : null;
                dataAsDictionary.Add("gender", gender);
                dataAsDictionary.Add("link", userData.domain);
                dataAsDictionary.Add("accesstoken", accessToken.access_token);

                return new AuthenticationResult(
                    true, (this as IAuthenticationClient).ProviderName, accessToken.user_id,
                    userData.first_name + " " + userData.last_name,
                    dataAsDictionary);
            }
            catch (Exception ex)
            {
                return new AuthenticationResult(ex);
            }
        }

        public static string Load(string address)
        {
            var request = WebRequest.Create(address) as HttpWebRequest;
            using (var response = request.GetResponse() as HttpWebResponse)
            {
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    return reader.ReadToEnd();
                }
            }
        }

        public static T DeserializeJson<T>(string input)
        {
            var serializer = new JavaScriptSerializer();
            return serializer.Deserialize<T>(input);
        }
    }
}
