﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Security;
using PascalWeb;
using PascalWeb.Additional;
using PascalWeb.DB;
using PascalWeb.DB.Enumerators;
using PascalWeb.DB.Repositories;
using PascalWeb.Models;
using WebMatrix.WebData;


public class PascalMembershipProvider : ExtendedMembershipProvider
    {
        // // Свойства из web.config
        private string _ApplicationName;
        private bool _EnablePasswordReset;
        private bool _EnablePasswordRetrieval = false;
        private bool _RequiresQuestionAndAnswer = false;
        private bool _RequiresUniqueEmail = true;
        private int _MaxInvalidPasswordAttempts;
        private int _PasswordAttemptWindow;
        private int _MinRequiredPasswordLength;
        private int _MinRequiredNonalphanumericCharacters;
        private string _PasswordStrengthRegularExpression;
        private MembershipPasswordFormat _PasswordFormat = MembershipPasswordFormat.Hashed;

        private UnitOfWork uow = new UnitOfWork();

        public override void Initialize(string name, NameValueCollection config)
        {
            if (config == null)
                throw new ArgumentNullException("config");
            if (name == null || name.Length == 0)
                name = "CustomMembershipProvider";
            if (String.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Custom Membership Provider");
            }
            base.Initialize(name, config);
            _ApplicationName = GetConfigValue(config["applicationName"],
                            System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
            _MaxInvalidPasswordAttempts = Convert.ToInt32(
                            GetConfigValue(config["maxInvalidPasswordAttempts"], "5"));
            _PasswordAttemptWindow = Convert.ToInt32(
                            GetConfigValue(config["passwordAttemptWindow"], "10"));
            _MinRequiredNonalphanumericCharacters = Convert.ToInt32(
                            GetConfigValue(config["minRequiredNonalphanumericCharacters"], "1"));
            _MinRequiredPasswordLength = Convert.ToInt32(
                            GetConfigValue(config["minRequiredPasswordLength"], "6"));
            _EnablePasswordReset = Convert.ToBoolean(
                            GetConfigValue(config["enablePasswordReset"], "true"));
            _PasswordStrengthRegularExpression = Convert.ToString(
                            GetConfigValue(config["passwordStrengthRegularExpression"], ""));
        }

        ////  Хелпер для получения значений из конфигурационного файла.
        private string GetConfigValue(string configValue, string defaultValue)
        {
            if (string.IsNullOrEmpty(configValue))
                return defaultValue;
            return configValue;
        }

        public long? CreateUser(string email, string password)
        {
            try
            {
                UserAuth user = new UserAuth();
                user.Email = email;
                user.PasswordSalt = cPassword.CreateSalt();
                user.PasswordHash = cPassword.CreatePasswordHash(password, user.PasswordSalt);
                user.CreationDate = DateTime.Now;
                user.IsActivated = true;
                user.IsLocked = false;

                user.UserRoleID = uow.UserManagment.getUserRoleIDByName(eUserRoles.generaluser.ToString()).Value;
                user.EmailConfirmationKey = cPassword.GenerateKey();

                uow.UserManagment.createUser(user, null);

                return user.ID;
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                return null;
            }
        }

        public MembershipUser GetUser(string email)
        {
            var dbuser = uow.UserManagment.getUserByEmail(email);
            if (dbuser != null)
            {
                //var dbuser = result.FirstOrDefault();
                int _providerUserKey = Convert.ToInt32(dbuser.ID);
                string _email = dbuser.Email;
                string _passwordQuestion = "";
                string _comment = ""; //dbuser.Comments;
                bool _isApproved = dbuser.IsActivated;
                bool _isLockedOut = dbuser.IsLocked;
                DateTime _creationDate = dbuser.CreationDate;
                DateTime _lastLoginDate = DateTime.Now;//dbuser.LastLoginDate;
                DateTime _lastActivityDate = DateTime.Now;
                DateTime _lastPasswordChangedDate = DateTime.Now;
                DateTime _lastLockedOutDate = DateTime.Now; //dbuser.LastLockedOutDate;
                MembershipUser user = new MembershipUser("CustomMembershipProvider",
                                                        dbuser.Name,
                                                        _providerUserKey,
                                                        _email,
                                                        _passwordQuestion,
                                                        _comment,
                                                        _isApproved,
                                                        _isLockedOut,
                                                        _creationDate,
                                                        _lastLoginDate,
                                                        _lastActivityDate,
                                                        _lastPasswordChangedDate,
                                                        _lastLockedOutDate);
                return user;
            }
            else
            {
                return null;
            }
        }

        //public bool Login(UnitOfWork unitOfWork,string provider, string userIdentifier, bool createPersistentCookie)
        //{
        //    if (string.IsNullOrWhiteSpace(provider) || string.IsNullOrWhiteSpace(userIdentifier))
        //    {
        //        return false;
        //    }

        //    // check DB
        //    var userExternal = unitOfWork.UserManagment.checkUserExternal(provider, userIdentifier);

        //    if (userExternal == null)
        //    {
        //        // create
        //    }
        //    else
        //    {

        //    }

        //    var uniqueID = provider + "/" + userIdentifier;

        //    // we then log the user into our application
        //    // we could have done a database lookup for a 
        //    // more user-friendly username for our app
        //    FormsAuthentication.SetAuthCookie(uniqueID, createPersistentCookie);

        //    // dictionary of values from identity provider
        //    //var userDataFromProvider = result.ExtraData;
        //    //var email = userDataFromProvider["email"];
        //    //var gender = userDataFromProvider["gender"];
        //}

        public override bool ConfirmAccount(string accountConfirmationToken)
        {
            throw new NotImplementedException();
        }

        public override bool ConfirmAccount(string userName, string accountConfirmationToken)
        {
            throw new NotImplementedException();
        }

        public override string CreateAccount(string email, string password, bool requireConfirmationToken)
        {
            ValidatePasswordEventArgs args = new ValidatePasswordEventArgs(email, password, true);
            OnValidatingPassword(args);
            if (args.Cancel)
            {
                return null;
            }
            /*        if (RequiresUniqueEmail && GetUserNameByEmail(email) != "")
                    {
                        status = MembershipCreateStatus.DuplicateEmail;
                        return null;
                    }*/
            MembershipUser u = GetUser(email);
            if (u == null)
            {
                long? userID = CreateUser(email, password);
                //status = MembershipCreateStatus.Success;

                return userID == null ? null : userID.Value.ToString();
            }
            else
            {
                return null;
            }

            return null;
        }

        public override string CreateUserAndAccount(string id, string password, bool requireConfirmation, IDictionary<string, object> values)
        {
            return CreateAccount(id, password);
        }

        public override bool DeleteAccount(string userName)
        {
            throw new NotImplementedException();
        }

        public override string GeneratePasswordResetToken(string userName, int tokenExpirationInMinutesFromNow)
        {
            throw new NotImplementedException();
        }

        public override ICollection<OAuthAccountData> GetAccountsForUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override DateTime GetCreateDate(string userName)
        {
            throw new NotImplementedException();
        }

        public override DateTime GetLastPasswordFailureDate(string userName)
        {
            throw new NotImplementedException();
        }

        public override DateTime GetPasswordChangedDate(string userName)
        {
            throw new NotImplementedException();
        }

        public override int GetPasswordFailuresSinceLastSuccess(string userName)
        {
            throw new NotImplementedException();
        }

        public override int GetUserIdFromPasswordResetToken(string token)
        {
            return 1;
        }

        public override bool IsConfirmed(string userName)
        {
            throw new NotImplementedException();
        }

        public override bool ResetPasswordWithToken(string token, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                return _ApplicationName;
            }
            set
            {
                _ApplicationName = value;
            }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            return GetUser(username);
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public override bool ValidateUser(string email, string password)
        {
            using (UnitOfWork _uow = new UnitOfWork())
            {
                var dbuser = _uow.UserManagment.getUserByEmail(email);

                if (dbuser != null)
                {
                    if (dbuser.PasswordHash == cPassword.CreatePasswordHash(password, dbuser.PasswordSalt)
                        && dbuser.IsActivated == true)
                        return true;
                    else
                        return false;
                }
                else
                {
                    return false;
                }
            }
        }
    }
