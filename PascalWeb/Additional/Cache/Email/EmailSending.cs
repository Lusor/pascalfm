﻿using PascalWeb.DB;
using PascalWeb.DB.Enumerators;
using PascalWeb.DB.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PascalWeb.Additional.Email
{
    public class EmailSending
    {
        /// <summary>
        /// Postal send http://aboutcode.net/postal/
        /// </summary>
        /// <param name="userAuth"></param>
        private static void sendConfirmationEmail(UserAuth userAuth, string baseURL)
        {

            dynamic email = new Postal.Email("EmailConfirmation");
            email.To = userAuth.Email;
            email.UserName = userAuth.Name;

            string confirmationLink = baseURL + "/account/verify?key=" + userAuth.EmailConfirmationKey;
            email.ConfirmationLink = confirmationLink;

            email.Send();
        }

        /// <summary>
        /// send email about password recovery
        /// </summary>
        /// <param name="userAuth"></param>
        private static void sendRestorePassword(UserAuth userAuth, string baseURL)
        {
            dynamic email = new Postal.Email("EmailRestorePassword");
            email.To = userAuth.Email;
            email.UserName = userAuth.Name;


            string confirmationLink = baseURL + "/account/restorePassword?key=" + userAuth.RestorePasswordKey;
            email.ConfirmationLink = confirmationLink;

            email.Send();
        }

        /// <summary>
        /// put email data to DB for dispatch (later)
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="userID"></param>
        /// <param name="emailType"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static bool putMessageToDB(UnitOfWork unitOfWork, long userID, eEmailDispatchType emailType, string message = null)
        {
            try
            {
                string emailTypeString = emailType.ToString();
                var emailDispatchTypeID = unitOfWork.EmailDispatchTypeRepository.Get(obj => obj.Name == emailTypeString).Select(s => s.ID).SingleOrDefault();
                if (emailDispatchTypeID == 0)
                    return false;

                // form entity
                EmailDispatch newEmail = new EmailDispatch();
                newEmail.EmailDispatchTypeID = emailDispatchTypeID;
                newEmail.UserID = userID;
                newEmail.Message = message;
                unitOfWork.EmailDispatchRepository.Insert(newEmail);
                unitOfWork.Save();
                return true;
            }
            catch (Exception exp)
            {
                PascalLogging.WriteErrorMessage("EmailSending", "putMessageToDB", exp.Message);
                return false;
            }
        }

        /// <summary>
        /// send email using data from DB
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool sendMessageFromDB(EmailDispatch email)
        {
            try
            {
                eEmailDispatchType emailType = GeneralMethods.ParseEnum<eEmailDispatchType>(email.EmailDispatchType.Name);
                switch (emailType)
                {
                    case eEmailDispatchType.email_confirmation:
                        sendConfirmationEmail(email.UserAuth, email.Message);
                        break;
                    case eEmailDispatchType.restore_password:
                        sendRestorePassword(email.UserAuth, email.Message);
                        break;
                    //case eEmailDispatchType.other:
                    //    break;
                    default: return false;
                }

                return true;
            }
            catch (Exception exp)
            {
                PascalLogging.WriteErrorMessage("EmailSending", "sendMessageFromDB", exp);
                return false;
            }
        }
    
    
    
    }
}