﻿using PascalWeb.DB.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.Additional.Cache
{
    public class CasheManager
    {
        /// <summary>
        /// check presence of data in cashe - otherwise get data from database
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataType"></param>
        /// <param name="unitOfWork"></param>
        /// <returns></returns>
        public static  T getData<T>(eCasheEnumeration dataType, UnitOfWork unitOfWork)
        {
            var data = (T)HttpRuntime.Cache.Get(dataType.ToString());
            if (data == null)
            {
                data = execQuery<T>(dataType, unitOfWork);
            }

            return data;
        }

        /// <summary>
        /// exec query to database according to dataType and return data with exact type
        /// </summary>
        /// <param name="dataType"></param>
        /// <param name="unitOfWork"></param>
        /// <returns></returns>
        public static T execQuery<T>(eCasheEnumeration dataType, UnitOfWork unitOfWork)
        {
            var data = execQuery(dataType, unitOfWork);
            var dataWithType = (T)data;
            return dataWithType;
        }

        /// <summary>
        /// exec query to database according to dataType
        /// </summary>
        /// <param name="dataType"></param>
        /// <param name="unitOfWork"></param>
        /// <returns></returns>
        private static object execQuery(eCasheEnumeration dataType, UnitOfWork unitOfWork)
        {
            switch (dataType)
            {
                //case eCasheEnumeration.FP_LAST_PROGRAM_EDITIONS_BY_PROGRAM:
                //    return CacheQueries.lastProgramEditionsByProgramQuery(unitOfWork);

                case eCasheEnumeration.FP_LAST_PROGRAM_EDITIONS:
                    return CacheQueries.lastProgramEditionsForFPQuery(unitOfWork);
            //    case eCasheEnumeration.FP_TOP_PROGRAM_EDITIONS_BY_PROGRAM:
            //        return CacheQueries.topProgramEditionsByProgramQuery(unitOfWork);


                case eCasheEnumeration.FP_BLOG_RECORDS_LAST:
                    return CacheQueries.lastBlogRecordsForFPQuery(unitOfWork);
                case eCasheEnumeration.FP_PROGRAM_TOP_AUTHOR_LIST:
                    return CacheQueries.getProgramTopAuthorList(unitOfWork);

            }

            return null;
        }
    }
}