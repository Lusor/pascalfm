﻿using PascalWeb.Controllers;
using PascalWeb.DB;
using PascalWeb.DB.AuxiliaryModels;
using PascalWeb.DB.PageModels;
using PascalWeb.DB.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.Additional.Cache
{
    public class CacheQueries
    {

        /// <summary>
        /// take just N last created program editions (IS USED)
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <returns></returns>
        public static LastProgramEditionPageModel lastProgramEditionsForFPQuery(UnitOfWork unitOfWork, int skip, int batch, int? themeID, long? authorID)
        {
            // get list of programIDs
            IEnumerable<int> programs = getProgramIDList(unitOfWork, themeID, authorID);

            LastProgramEditionPageModel model = new LastProgramEditionPageModel();
            var lastEditions = unitOfWork.ProgramEditionRepository.Get(filter: obj => obj.Published == true && programs.Contains(obj.ProgramID),
                orderBy: q => q.OrderByDescending(x => x.AirDate),
                includeProperties: "Content, Program",
                skip: skip,
                take: batch);
            return LastProgramEditionPageModel.getLastProgramEditionModel(lastEditions);
        }

        public static LastProgramEditionPageModel lastProgramEditionsForFPQuery(UnitOfWork unitOfWork, int? themeID = null, long? authorID = null)
        {
            return lastProgramEditionsForFPQuery(unitOfWork, 0, ConstantSettings.FP_MaxNumberOfLastProgramEditions, themeID, authorID);
        }


        /// <summary>
        /// take last program edition of each program
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <returns></returns>
        //public static LastProgramEditionPageModel lastProgramEditionsByProgramQuery(UnitOfWork unitOfWork, int? themeID = null, long? authorID = null)
        //{
        //    LastProgramEditionPageModel model = new LastProgramEditionPageModel();
        //    // get list of programIDs
        //    IEnumerable<int> programs = getProgramIDList(unitOfWork, themeID, authorID);

        //    // get editions
        //    for (int i = 0; i < programs.Count(); i++)
        //    {
        //        int programID = programs.ElementAt(i);
        //        var lastEdition = unitOfWork.ProgramEditionRepository.Get(filter: obj => obj.ProgramID == programID && obj.Published == true,
        //            orderBy: q => q.OrderByDescending(x => x.AirDate),
        //            includeProperties: "Content, Program",
        //            take: 1).FirstOrDefault();
        //        if (lastEdition != null)
        //            model.LastProgramEditions.Add(LastProgramEditionPageModel.getLastProgramEditionPageModel(lastEdition));
        //    }
        //    model.LastProgramEditions = model.LastProgramEditions.OrderByDescending(obj => obj.Edition.AirDate).Take(ConstantSettings.FP_MaxNumberOfLastProgramEditions).ToList();
        //    return model;
        //}


        /// <summary>
        /// take top rated edition of each program
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <returns></returns>
        public static LastProgramEditionPageModel topProgramEditionsByProgramQuery(UnitOfWork unitOfWork, int? themeID = null, long? authorID = null)
        {
            LastProgramEditionPageModel model = new LastProgramEditionPageModel();
            
            // get list of programIDs
            IEnumerable<int> programs = getProgramIDList(unitOfWork, themeID, authorID);

            // get editions
            for (int i = 0; i < programs.Count(); i++)
            {
                int programID = programs.ElementAt(i);
                var topEdition = unitOfWork.ProgramEditionRepository.Get(filter: obj => obj.ProgramID == programID && obj.Published == true,
                    orderBy: q => q.OrderByDescending(x => (x.ListenedCounter + x.ViewedCounter)),
                    includeProperties: "Content, Program",
                    take: 1).FirstOrDefault();
                if (topEdition != null)
                    model.LastProgramEditions.Add(LastProgramEditionPageModel.getLastProgramEditionPageModel(topEdition));
            }
            model.LastProgramEditions = model.LastProgramEditions.OrderByDescending(obj => obj.Edition.ViewedCounter + obj.Edition.ListenedCounter).Take(ConstantSettings.FP_MaxNumberOfTopProgramEditions).ToList();
            return model;
        }


        public static List<BlogRecord> lastBlogRecordsForFPQuery(UnitOfWork unitOfWork, int skip, int batch, int? themeID, long? authorID)
        {
            // get list of programIDs
            IEnumerable<int> blogs = getBlogIDList(unitOfWork, themeID, authorID);

            var blogRecordList = unitOfWork.BlogRecordRepository.Get(filter: obj => obj.Published == true && blogs.Contains(obj.BlogID),
                orderBy: q => q.OrderByDescending(x => x.CreationDate),
                includeProperties: "Blog",
                skip: skip,
                take: batch).ToList();
            return blogRecordList;
        }

        public static List<BlogRecord> lastBlogRecordsForFPQuery(UnitOfWork unitOfWork, int? themeID = null, long? authorID = null)
        {
            return lastBlogRecordsForFPQuery(unitOfWork, 0, ConstantSettings.FP_MaxNumberOfLastBlogRecords, themeID, authorID);
        }


        public static List<UserAuth> getProgramTopAuthorList(UnitOfWork unitOfWork)
        {
            var authorQuery = unitOfWork.ProgramAuthorsRepository.Get(includeProperties: "UserAuth, UserAuth.UserInfo, Program").Select(obj => obj.UserAuth).Distinct().ToList();
            return authorQuery;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="themeID"></param>
        /// <param name="authorID"></param>
        /// <returns></returns>
        private static IEnumerable<int> getProgramIDList(UnitOfWork unitOfWork, int? themeID, long? authorID)
        {
            IEnumerable<int> programs = null;
            if (themeID.HasValue)
                programs = unitOfWork.ProgramRepository.Get(filter: obj => obj.ThemeID == themeID).Select(s => s.ID);
            else if (authorID.HasValue)
                programs = unitOfWork.ProgramAuthorsRepository.Get(filter: obj => obj.UserID == authorID).Select(s => s.ProgramID);
            else
                programs = unitOfWork.ProgramRepository.Get().Select(s => s.ID);
            return programs;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="themeID"></param>
        /// <param name="authorID"></param>
        /// <returns></returns>
        private static IEnumerable<int> getBlogIDList(UnitOfWork unitOfWork, int? themeID, long? authorID)
        {
            IEnumerable<int> blogIDList = null;
            if (themeID.HasValue)
                blogIDList = unitOfWork.BlogRepository.Get(filter: obj => obj.ThemeID == themeID).Select(s => s.ID);
            else if (authorID.HasValue)
                blogIDList = unitOfWork.BlogRepository.Get(filter: obj => obj.AuthorID == authorID).Select(s => s.ID);
            else
                blogIDList = unitOfWork.BlogRepository.Get().Select(s => s.ID);
            return blogIDList;
        }
    }
}