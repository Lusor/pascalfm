﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.Additional
{
    public class ConstantSettings
    {
        // program edition
        public const int FP_MaxNumberOfTopProgramEditions = 9;
        public const int FP_MaxNumberOfLastProgramEditions = 9;

        public const int FP_MaxNumberOfLastBlogRecords = 4;

        public const int TitleNumberLastOnProgramEditionPage = 8;

        public const int ProgramEditionNumberOnProgramPage = 6;

        // blog
        public const int BlogRecordsOnPage = 6;


        public const int CommentBatchNumber = 10;
    }
}