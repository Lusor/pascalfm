﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PascalWeb.DB;
using PascalWeb.DB.PageModels;

namespace PascalWeb.Additional.Audio
{
    public class AudioManager
    {
        public static string  PLAYLIST_SESSION_KEY = "LAST_EDITIONS";

        public static List<AudioTransferData> setPlayList(List<ProgramEdition> editions)
        {
            List<AudioTransferData> audioData = new List<AudioTransferData>();
            for (int i = 0; i < editions.Count(); i++)
            {
                createAudioTransferData(audioData, editions.ElementAt(i));
            }
            return audioData;
        }

        public static List<AudioTransferData> setPlayList(List<LastProgramEditionPageModel.LastProgramEdition> editions)
        {
            List<AudioTransferData> audioData = new List<AudioTransferData>();
            for (int i = 0; i < editions.Count(); i++)
            {
                createAudioTransferData(audioData, editions[i].Edition);
            }
            return audioData;
        }

        private static void createAudioTransferData(List<AudioTransferData> audioData, ProgramEdition edition)
        {
            if (edition != null && edition.Content != null)
            {
                AudioTransferData data = new AudioTransferData(edition.ID,
                    edition.Program.Name,
                    edition.EditionTittle,
                    edition.Content.AbsoluteFileUri);
                audioData.Add(data);
            }
        }
    }
}