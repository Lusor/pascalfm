﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.Additional
{
    public class PascalLogging
    {
        public readonly static log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void WriteErrorMessage(string className, string methodName, string message)
        {
            string fullMessage = String.Format("Class '{0}' - method '{1}' : {2}", className, methodName, message);
            Logger.Error(fullMessage);
        }

        public static void WriteErrorMessage(string className, string methodName, Exception exc)
        {
            string errorMessage = exc != null ? exc.Message : "";
            string errorInnerMessage = (exc != null && exc.InnerException != null)? exc.InnerException.Message : "";
            string fullMessage = String.Format("Class '{0}' - method '{1}' : {2} ({3})", className, methodName, exc.Message, errorInnerMessage);
            Logger.Error(fullMessage);
        }

        public static void WriteInfoMessage(string className, string methodName, string message)
        {
            string fullMessage = String.Format("Class '{0}' - method '{1}' : {2}", className, methodName, message);
            Logger.Info(fullMessage);
        }
    }
}