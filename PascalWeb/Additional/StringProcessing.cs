﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PascalWeb.Additional
{
    public class StringProcessing
    {
        public static bool getUrlParts(string originalUrl, out string controllerName, out string actionName)
        {
            controllerName = null;
            actionName = null;
            if (string.IsNullOrWhiteSpace(originalUrl))
            {
                return false;
            }

            var questionMarkIndex = originalUrl.IndexOf('?');
            if (questionMarkIndex > 0)
            {
                originalUrl = originalUrl.Remove(questionMarkIndex, originalUrl.Length - questionMarkIndex);
            }
            var parts = originalUrl.Split(new char[] { '/' }).ToList();

            for (int i = parts.Count - 1; i > -1; i--)
            {
                if (string.IsNullOrWhiteSpace(parts[i]))
                    parts.Remove(parts[i]);
            }

            if (parts.Count() > 0)
                controllerName = parts[0];
            if (parts.Count() > 1)
                actionName = parts[1];
            return true;      
        }
    }
}