# Project description

This is a repository for PascalFM web application. The web application is a podcast service. It allows user searching and listening podcasts they are interested in;
Each podcast has a description and a number of tags associated with them. So, users can search using this information. Tags are used also to provide recommendations to the user what to listen that are based on tag's system and the previous history what user listened before.
Registered authors of podcasts can create a new podcast, edit their textual and audio information.

Editors and admins have an access to the list of users, all information about podcasts. Admins can also assign rights to certain roles of users.

The application is based on Microsoft Azure and uses the following technologies:
- ASP MVC
- SQL Database (with Entity Framework)
- Azure Storage

The service is integrated with the Apple ITunes Podcast and several other services.